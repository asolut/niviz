'use strict';

const webpack = require('webpack'), path = require('path');

let config = {
  entry: {
    niviz: [
      './lib/niviz.js'
    ]
  },

  plugins: [
    // Minify assets.
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false // https://github.com/webpack/webpack/issues/1496
      }
    })
  ],

  output: {
    path: path.resolve(__dirname, 'dist/lib'),
    filename: 'niviz-bundle-latest.js'
  },
};

module.exports = config;
