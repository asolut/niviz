#!/bin/sh
#This is still a hacky way of finding new strings that need translations in the javascript code
# as well as in the pug files

#find ui -name "*.pug"
ROOT_PATH=./

show_help() {
        me=`basename $0`
        printf "Usage: \n"
        printf "\t-c           Return keys that are not in the dictionary file\n"
        printf "\t-d           Return the list of unique keys found in the french.js dictionaty file\n"
        printf "\t-p           extract translatable strings found in pug files\n"
}

#Extract strings from pug files
getPugStrings() {
	grep -rEoh "'([a-zA-Z]|_)+' \| t : lang" ${ROOT_PATH}/ui/ | cut -d'|' -f1 | sed -e "s/ *$//" | sort -u
}

#keep unique keys in the dictionary file
getUniqueDictKeys() {
	grep -Eoh "'.*'" ${ROOT_PATH}/lib/i18n/french.js | cut -d':' -f1 | sort -u 
}

#get keys that are NOT already in the dictionary file
getNewKeys() {
	tmp_dest=`mktemp`
	getUniqueDictKeys > ${tmp_dest}
	getPugStrings | comm ${tmp_dest} - -1 -3
	rm ${tmp_dest}
}

if [ $# -lt 1 ]; then
	show_help
	exit 0
fi

#Main loop
while getopts "h?cdpr" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    c)	getNewKeys
    	exit 0
    	;;
    d)  getUniqueDictKeys
    	exit 0
        ;;
    p)  getPugStrings
    	exit 0
        ;;
    esac
done
