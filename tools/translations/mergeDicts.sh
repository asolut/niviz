#!/bin/sh
#add to the destination dictionary file the missing entries from the reference file
#it takes one argument: the destination dictionary file

if [ $# -lt 1 ]; then
	me=`basename $0`
	printf "Sort the given dictionary file and add the missing keys from a reference dictionary\n"
	printf "(with the translation strings from the reference file)\n"
	printf "Usage: \n "
	printf "\t${me} {dictionary file}\n"
	printf "Example:\n\t${me} ./lib/i18n/french.js > french_new.js\n"
	exit 0
fi

ref="./lib/i18n/english.js"
dest=$1

#create the first associated array output
analyze_dict() {
	headers=$1_head
	footers=$1_foot
	data=$1
	cat $2 | awk -F':' '
		BEGIN {
			in_headers=1
			in_footers=0
			section=""
		}
		/^[[:space:]]+Dictionary\.[a-zà-ÿ]+ = {[[:space:]]*$/ { #headers
			in_headers=0
			print $0 >> "'"${headers}"'"
			next
		}
		(in_headers==1) {
			print $0 >> "'"${headers}"'"
			next
		}
		/^[[:space:]]+};[[:space:]]*$/ { #footers
			in_footers=1
		}
		(in_footers==1) {
			print $0 >> "'"${footers}"'"
			next
		}
		(NF<2) {
			next
		}
		{
			gsub(/[[:space:]]+/, " ")
		}
		/^[[:space:]]+[a-z]+: {[[:space:]]*$/ { #section
			gsub(" ", "", $1)
			section=$1
			next
		}
		{ #normal key/value
			if (section=="")
				key=$1
			else
				key=sprintf("%s:%s", section, $1)
			
			value=$2
			if (NF>2) for (ii=3; ii<=NF; ii++) value=sprintf("%s:%s", value, $(ii))
			values[key] = value
		}
		END {
			num = asorti(values, indices)
			for (ii=1; ii<=num; ii++) {
				key=indices[ii]
				printf("%s:%s\n", key, values[key])
			}
		}
	' > ${data}
}

#generate a new dictionary file from the previously created dumps
generate_dict() {
	ref_dump=$1
	dest_dump=$2
	
	cat ${dest_dump}_head
	
	cat ${ref_dump} ${dest_dump} | awk -F':' '
		(NF==2) {
			key=$1
			value=$2
			if (NF>2) for (ii=3; ii<=NF; ii++) value=sprintf("%s:%s", value, $(ii))
			last_char=substr(value,length(value),1)
			if (last_char!=",") value=sprintf("%s,", value)
			
			values[key] = value
			next
		}
		{
			key=sprintf("%s:%s", $1, $2)
			value=$3
			if (NF>3) for (ii=4; ii<=NF; ii++) value=sprintf("%s:%s", value, $(ii))
			last_char=substr(value,length(value),1)
			if (last_char!=",") value=sprintf("%s,", value)
			
			values[key] = value
		}
		END {
			hasSection=0
			num = asorti(values, indices)
			for (ii=1; ii<=num; ii++) {
				key=indices[ii]
				value=values[key]
				
				n=split(key, arr, ":")
				if (n==1)
					printf("   %s:%s\n", key, value)
				else {
					if (section[arr[1]]==0) {
						if (hasSection!=0) printf("    },\n")
						printf("\n    %s: {\n", arr[1])
						hasSection++
					}
					section[arr[1]]++
					printf("     %s:%s\n", arr[2], value)
				}
			}
			if (hasSection!=0) printf("    }\n")
		}
	'
	
	cat ${dest_dump}_foot
}

out1=`mktemp`
analyze_dict ${out1} ${ref}

out2=`mktemp`
analyze_dict ${out2} ${dest}

generate_dict ${out1} ${out2}

rm ${out1}*; rm ${out2}*
