#!/bin/sh
#sort a dictionary file in alphabetic order and re-indent it.
#The goal is to be able to easily compare various versions and translations together 
#(a simple diff will show the new translation strings)

if [ $# -lt 1 ]; then
	me=`basename $0`
	printf "Sort the keys in a dictionary file, within each section, so it becomes easy to\n"
	printf "compare dictionary files together.\n"
	printf "Usage: \n "
	printf "\t${me} {dictionary file}\n"
	printf "Example:\n\t${me} ./lib/i18n/english.js > sorted.js\n"
	exit 0
fi

awk -F':' '
		function printSectionContent() {
			num = asorti(values, indices)
			for (ii=1; ii<=num; ii++) { #all but the last key
				key=indices[ii]
				value=values[key]
				if (ii==num) {
					last_char=substr(value,length(value),1)
					if (last_char==",") value=substr(value, 1, length(value)-1)
				}
				printf("%s:%s\n", key, value)
			}
			if (section=="") printf("\n")
			delete values
		}
		BEGIN {
			in_headers=1
			in_footers=0
			section=""
		}
		/^[[:space:]]+Dictionary\.[a-z]+ = {[[:space:]]*$/ { #headers
			in_headers=0
			print $0
			next
		}
		(in_headers==1) {
			print $0
			next
		}
		/^[[:space:]]+};[[:space:]]*$/ { #footers
			printSectionContent()
			in_footers=1
			if (section!="") printf("    }\n")
		}
		(in_footers==1) {
			print $0
			next
		}
		(NF<2) {
			next
		}
		{ #from there, remove duplicate white spaces
			gsub(/[[:space:]]+/, " ")
		}
		/^[[:space:]]+[a-z]+: {[[:space:]]*$/ { #new section
			printSectionContent()
			if (section!="") printf("    },\n\n");
			gsub(" ", "", $1)
			section=$1
			printf("    %s: {\n", section)
			next
		}
		{ #normal key: value
			if (section=="")
				key=sprintf("   %s", $1)
			else
				key=sprintf("     %s", $1)
			
			value=$2
			if (NF>2) for (ii=3; ii<=NF; ii++) value=sprintf("%s:%s", value, $(ii))
			last_char=substr(value,length(value),1)
			if (last_char!=",") value=sprintf("%s,", value)
			
			values[key]=value
		}
' $1
