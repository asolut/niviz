
NBIN = ./node_modules/.bin

ASSETS = application.js niviz.js application.css \
         jquery.js moment.js snap.svg.js

# --- Development ---

server :
	@DEBUG=niviz:* node server.js

# --- Distribution ---

build : clean
	@DEBUG=niviz:build NODE_ENV=development node build.js ${ASSETS}

dist : clean
	@DEBUG=niviz:build ${NBIN}/yuidoc -q .
	@DEBUG=niviz:build node build.js ${ASSETS}
	@DEBUG=niviz:build cp -r assets/resources ./dist/resources
	rm -f dist/assets/jquery*
	rm -f dist/assets/moment*
	rm -f dist/assets/snap.svg*
	rm -f dist/assets/niviz*
	rm -f dist/assets/*.map* dist/assets/*.gz
	@DEBUG=niviz:build ${NBIN}/webpack
	@DEBUG=niviz:build cp -r assets/img dist/img
	@DEBUG=niviz:build cp -r doc dist/doc

debug: clean
	@DEBUG=niviz:build node build.js ${ASSETS}

# --- Documentation  ---

doc :
	@${NBIN}/yuidoc .

# --- Testing ---

lint :
	@${NBIN}/eslint server.js build.js convert.js \
		ui/javascripts/*.js test/ui/*.js \
		lib/*.js lib/parsers/*.js lib/values/*.js lib/graphs/*.js \
        test/lib/*.js test/lib/parsers/*.js test/lib/values/*.js \
		test/fixtures/*.js

test : lint
	@./node_modules/karma/bin/karma start test/karma.opts

watch :
	@./node_modules/karma/bin/karma start test/karma.opts --no-single-run --browsers

test-node : lint
	@${NBIN}/mocha test/lib/*.js test/lib/parsers/*.js

debug-node :
	@${NBIN}/mocha debug test/lib/*.js test/lib/parsers/*.js

coverage :
	@./node_modules/karma/bin/karma start test/karma.opts \
		--reporters dots,coverage \
		--single-run


# --- Dependency Management ---

# Install/update all third-party dependencies
dep : npm bower


npm :
	@npm install
	@[ $$? -ne 0 ] || echo -e "\e[0;32mNode.js installed modules to node_modules\e[0m"

bower :
	@${NBIN}/bower install
	@[ $$? -ne 0 ] || echo -e "\e[0;32mBower components installed to .bower\e[0m"

update :
	@npm update
	@${NBIN}/bower update

# --- Housekeeping ---

clean :
	@rm -rf ./dist

purge : clean
	rm -rf ./coverage
	rm -rf ./doc
	rm -rf ./node_modules
	rm -rf ./.bower


.PHONY: clean purge npm bower test test-node debug-node \
	doc coverage dist jade mince server
