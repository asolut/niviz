niViz - Snowpack Visualization
==============================
Welcome to niViz, an open source,  browser based tool for visualiazing and editing measured or simulated snow profiles including auxiliary data (such as meteorological time series). Use it online at [https://run.niviz.org](https://run.niviz.org) or on your own server! Find more at [https://niviz.org](https://niviz.org).


Setup
-----
Ensure that Node.js and make are available on your system as well as bower (installed through npm):

    $ node -v
    v8.5.0
    $ make -v
    GNU Make 4.0

Currently, it works properly with node 12.22.12 and npm 6.14.16 (Linux 4.4.0 x86_64). 
Then run `make dep` to fetch and install all dependencies. At any
time, you can run `make purge` to remove all downloaded dependencies
completely.

After this, `make server` starts a web server you can use for
development of the GUI; `make build` builds all applications;
`make dist` builds and compresses/minifies all applications.

Testing
-------
You can run the tests for the visualization library and the GUI
in Phantom JS (or a regular browser by visiting localhost at port
9876); additionally, the library tests can be run with Node.js or
in the Node debugger.

    $ make test        # Run all tests in Phantom JS
    $ make coverage    # Compile test coverage in Phantom JS
    $ make watch       # Start karma on port 9876

    $ make test-node   # Run library tests in Node.js
    $ make debug-node  # Run library tests in Node.js debugger

Documentation
-------------
Run `make doc` to generate the html API documentation in `./doc`.

Setting up a website
--------------------
If you want to run niViz hosted by an Apache server all requests (except
actual file requests) need to be redirected towards index.html. Please
add the following lines to your apache site configuration:

<pre><code>
RewriteEngine On
RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]
RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d
RewriteRule ^ - [L]
RewriteRule ^ /index.html
</code></pre>
