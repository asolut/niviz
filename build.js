/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*eslint strict: [2, "global"] */
'use strict';

var fs     = require('fs');
var debug  = require('debug')('niviz:build');

var Mincer = require('mincer');
var pug    = require('pug');
var exec   = require('child_process').execSync;
var moment = require('moment');

var assets = require('./assets.json');

var targets = process.argv.slice(2);
var dependencies = [];

var options = {
  compress: true,
  sourceMaps: true,
  embedMappingComments: true
};

// Macro processor allows to insert macros like version() and compiled()
Mincer.MacroProcessor.configure(['.js']);

var env = new Mincer.Environment('./');

env.registerHelper('compiled', function() {
  return "'" + moment().format() + "'";
});

env.registerHelper('version', function() {
  var version = exec( 'git describe --tags --abbrev=0' ).toString().replace('\n', '');
  return "'" + version + "'";
});

assets.paths.forEach(function (path) {
  env.appendPath(path);
});

env.ContextClass.defineAssetPath(function (path, opts) {
  if (opts.type === 'font') {
    path = path.replace(/[#?].*$/, '');
  }

  var asset = env.findAsset(path);

  if (!asset) throw new Error('asset not found: ' + path);

  dependencies.push(path);

  return asset.digestPath;
});

if (process.env.NODE_ENV === 'development') {
  debug('compiling assets in development mode...');

} else {
  debug('compiling assets in production mode...');

  env.enable('source_maps');
  env.jsCompressor = 'uglify';
  env.cssCompressor = 'csswring';

  env = env.index;
}

var manifest = new Mincer.Manifest(env, './dist/assets');

function asset_path(name) {
  return 'assets/' + manifest.assets[name];
}

var locals = {
  js: function (name) {
    return '<script src="/' + asset_path(name + '.js') + '"></script>';
  },

  css: function (name) {
    return '<link rel="stylesheet" href="/' + asset_path(name + '.css') + '"/>';
  }
};

try {
  manifest.compile(targets, options);

  debug('compiled main targets successfully.');

  if (dependencies.length) {
    manifest.compile(dependencies, options);
    debug('compiled dependent assets successfully');
  }

  var html = pug.compileFile('./ui/index.pug')(locals);
  fs.writeFileSync('./dist/index.html', html);

  html = pug.compileFile('./ui/index.pug')(locals);
  fs.writeFileSync('./dist/full.html', html);

  html = pug.compileFile('./ui/simple.pug')(locals);
  fs.writeFileSync('./dist/simple.html', html);

  fs.createReadStream('./.bower/angular-bootstrap-colorpicker/css/colorpicker.css')
    .pipe(fs.createWriteStream('./dist/assets/colorpicker.css'));

  debug('compiled index.html successfully');

  fs.mkdirSync('./dist/lib');
  fs.createReadStream('./dist/assets/' + manifest.assets['niviz.js'])
    .pipe(fs.createWriteStream('./dist/lib/niviz-latest.js'));

  debug('libaries compiled successfully');

} catch (error) {
  console.error('Failed to compile assets: %s', error.message);
  console.error(error.stack);
}
