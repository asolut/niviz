/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// NOTE: you must run make dep && make dist before running this script:
//       node convert-native.js

const fs        = require('fs');
const path      = require('path');
const moment    = require('moment');
const jsdom     = require('jsdom');
const { JSDOM } = jsdom;
const { convert }  = require('convert-svg-to-png');

let window = initEnv();

let niviz  = require('./dist/lib/niviz-latest.js'); // initEnv() needs to be run beforehand

// grab the SVG element from the window and resize it to 900x900px
let canvas  = jQuery('#canvas1').width(900).height(900);

let data    = fs.readFileSync('test/fixtures/fz_snowprofile_53930_de.caaml', 'utf-8');

// HACK: Unfortunatly the jsdom DOMParser does not deal well with namespaces
//       the solution is to strip it of namespaces altogether
data = data.replace(/<[a-z]+:/g, '<');
data = data.replace(/<\/[a-z]+:/g, '</');

// niviz.parse() parses the data with the respective parser (second argument)
// and returns a station object, that contains profiles and meta info
let station = niviz.parse(data, 'caaml', function (error, station) {

  // niviz.draw() lets you draw whichever profile you want to draw:
  // SimpleProfile, MobileProfile, SLFProfile, StructureProfile
  let graph = niviz.draw('SimpleProfile', station, canvas, {
    miniature: false,
    margin_top: 50,
    other_parameters: [{ name: 'comments' }],
    use_hhindex_as_primary_axis: true
  });

  // niviz.convert() converts your graph object into an SVG
  niviz.convert('SVG', graph, function (data, error) {

    fs.writeFile('out.svg', data, function () {

      // Adding 10px to the top of the SVG before converting it into a PNG
      // data = data.replace('<svg ', '<svg style="margin-top: 10px;" ');

      // Using the convert-svg-to-png library saves us some hassle with fonts
      const png = convert(data, { background: 'white', width: 1000 }).then(function (png) {
        fs.writeFile('out.png', png, function () {
          process.exit(1);
        });
      });
    });

  });
});


// Set up the jsdom environment so that niViz can thrive
function initEnv () {

  let dom = new JSDOM('<!DOCTYPE html><body><svg id="canvas1"></svg></body></html>', {
    runScripts: 'dangerously',
    url: 'http://localhost'
  });

  let window         = dom.window;
  window.ignoreLibs  = true;
  global.window      = window;
  global.document    = window.document;
  global.moment      = moment;
  global.canvg       = require('canvg');
  global.Snap        = require('snapsvg');
  global.NodeList    = window.NodeList;
  global.navigator   = window.navigator;
  global.DOMParser   = window.DOMParser;
  global.Element     = window.Element;

  // mute any output on console.dir()
  global.console.dir = function () {};

  // to initialize jQuery we require a window with a document
  global.jQuery      = require('jquery');

  return window;
}
