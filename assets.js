/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

var assert = require('assert');
var format = require('util').format;
var assign = require('object-assign');

var Mincer = require('mincer');
var config = require('./assets.json');

module.exports = function (env, options) {
  env = env || new Mincer.Environment();
  options = assign({}, config, options);

  if (options.autoprefixer) env.enable('autoprefixer');
  if (options.sourcemaps) env.enable('source_maps');

  Mincer.Autoprefixer.configure(options.browserlist);
  Mincer.SassEngine.configure(options.sass);

  options.paths.forEach(env.appendPath, env);

  var assets = [];

  function find(path, params) {
    if (params.type === 'font') {
      path = path.replace(/[#?].*$/, '');
    }

    var asset = env.findAsset(path);
    if (!asset) throw new Error('asset not found: ' + path);

    return asset;
  }

  function resolve(path, params) {
    if (!path) return null;
    var asset = find(path, params || {});

    if (assets.indexOf(asset.logicalPath) === -1) {
      assets.push(asset.logicalPath);
    }

    return [
      options.serve,
      options.fingerprinting ? asset.digestPath : asset.logicalPath
    ].join('/');
  }

  function template(path) {
    var html = find(path, {});

    assert((/html/).test(html.contentType), 'html file expected');

    return html.toString();
  }

  function js(name) {
    return format('<script src="%s"></script>', resolve(name + '.js'));
  }

  function css(name) {
    return format('<link rel="stylesheet" href="%s"/>', resolve(name + '.css'));
  }

  env.ContextClass.defineAssetPath(resolve);

  // Connect-Assets registers its helpers globally!
  // Since this is just for development, let's do that, too.
  global.asset_path = resolve;
  global.template = template;
  global.css = css;
  global.js = js;

  return { env: env, req: assets, options: options };
};
