/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('niviz.console', function () {
  'use strict';

  beforeEach(function () {
    module('niviz.console');
  });

  describe('$console', function () {
    var console;

    beforeEach(inject(function ($console) {
      console = $console;
    }));

    it('is a function', function () {
      expect(console).to.be.a('function');
    });

    it('adds alerts to the queue', function () {
      console.log('foo');
      console.error('bar %s', 'baz');

      expect(console.alerts).to.have.length(2);
      expect(console.alerts[0].message).to.eql('bar baz');
      expect(console.alerts[1].type).to.eql('warning');

      console.info('qux');
      console.success('quux');

      console.dismiss(0);

      expect(console.alerts).to.have.length(3);
      expect(console.alerts[0].message).to.eql('qux');
    });
  });
});
