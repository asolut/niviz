/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('niviz.Range', function () {
  'use strict';

  var Range = niviz.Range;

  it('is a constructor function', function () {
    expect(Range).to.be.a('function');
  });

  describe('an instance', function () {
    var range;

    beforeEach(function () {
      range = new Range();
    });

    it('is empty', function () {
      expect(range.empty).to.be.true;
    });

    describe('.height', function () {
      it('is zero by default', function () {
        expect(range.height).to.eql(0);
      });

      it('is kept up to dat by push', function () {
        range.push({ height: 1 });

        expect(range.height).to.eql(1);

        range.push({ height: 2 });
        expect(range.height).to.eql(2);

        range.push({ height: 4 });
        range.push({ height: 3 });

        expect(range.height).to.eql(4);
      });
    });

    describe('.max', function () {
      it('returns undefined by default', function () {
        expect(range.max('height')).to.be.undefined;
      });

      it('returns the max profile value for the given property', function () {
        range.profiles = [{ height: 1 }];

        expect(range.max('height')).to.eql(1);

        range.profiles = [{ height: 1 }, { height: 3 }];
        expect(range.max('height')).to.eql(3);

        range.profiles = [{ height: 1 }, { height: 3 }, { height: 5 }, { height: 4 }];
        expect(range.max('height')).to.eql(5);
      });
    });
  });

});
