/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('JSONParser', function () {
  'use strict';

  var Parser     = niviz.Parser;

  var Station    = niviz.Station;
  var Profile    = niviz.Profile;
  var Feature    = niviz.Feature;
  var Value      = niviz.Value;

  var JSONParser = Parser.format.json;

  it('is a constructor function', function () {
    expect(JSONParser).to.be.a('function');
  });

  it('creates parser instances', function () {
    expect(new JSONParser()).to.be.instanceof(Parser);
  });

  describe('.parse_station', function () {
    var parser;

    beforeEach(function () { parser = new JSONParser(); });

    it('accepts empty objects', function () {
      expect(parser.parse_station.bind(parser, {})).not.to.throw();
    });

    it('fails on invalid input', function () {
      expect(parser.parse_station.bind(parser)).to.throw();
      expect(parser.parse_station.bind(parser, '')).to.throw();
      expect(parser.parse_station.bind(parser, 42)).to.throw();
      expect(parser.parse_station.bind(parser, null)).to.throw();
    });

    it('sets properties', function () {
      var input = { name: 'foo', id: 23, position: { altitude: 2000 }};
      var station = parser.station;

      parser.parse_station(input);

      expect(station.name).to.eql(input.name);
      expect(station.id).to.eql(input.id);
      expect(station.position.altitude).to.eql(input.position.altitude);
    });
  });

  describe('.parse_profile', function () {
    var parser;
    var DATE = '2015-02-13T16:48:33.345Z';

    beforeEach(function () { parser = new JSONParser(); });

    it('parses the date', function () {
      parser.parse_profile({ date: DATE });

      expect(parser.station.profiles).to.have.length(1);

      var date = parser.station.profiles[0].date;

      expect(date.isValid()).to.be.true;
      expect(date.year()).to.eql(2015);
      expect(date.month()).to.eql(1); // zero-based month!
      expect(date.date()).to.eql(13);
    });

    it('fails on invalid input', function () {
      expect(parser.parse_profile.bind(parser)).to.throw();
      expect(parser.parse_profile.bind(parser, '')).to.throw();
      expect(parser.parse_profile.bind(parser, {})).to.throw();
      expect(parser.parse_profile.bind(parser, { date: 'foo' })).to.throw();
      expect(parser.parse_profile.bind(parser, 42)).to.throw();
      expect(parser.parse_profile.bind(parser, null)).to.throw();
    });

  });

  describe('parsing min.json', function () {
    var p;

    beforeEach(function () {
      p = new JSONParser(fixtures('min.json'));
    });

    function validate(station) {
      expect(station).to.be.instanceof(Station);

      expect(station.position).to.have.property('latitude', 45);
      expect(station.position).to.have.property('altitude', 2000);

      expect(station.profiles).to.have.length(5);

      var profile = station.get(-1);

      expect(profile).to.be.instanceof(Profile);
      expect(profile).to.have.property('height', 25.69);
      expect(profile).to.contain.keys('density', 'grainshape', 'grainsize');

      expect(profile.grainshape).to.be.instanceof(Feature);
      expect(profile.grainshape.layers[0]).to.be.instanceof(Value);
      expect(profile.grainshape.layers[0]).to.be.instanceof(Value.GrainShape);
      expect(profile.grainshape.layers[0]).to.have.property('height', 1.54);
      expect(profile.grainshape.layers[0]).to.have.property('code', 'c(e)');

      expect(profile.density).to.be.instanceof(Feature);
      expect(profile.density.layers[0]).to.be.instanceof(Value);
      expect(profile.density.layers[0]).to.have.property('height', 1.54);

      expect(profile.grainsize).to.be.instanceof(Feature);
      expect(profile.grainsize.layers[0]).to.be.instanceof(Value);
      expect(profile.grainsize.layers[0]).to.be.instanceof(Value.GrainSize);
      expect(profile.grainsize.layers[0]).to.have.property('text', 'medium');

      expect(profile.grainsize.min).to.eql(0.30);
      expect(profile.grainsize.max).to.eql(0.53);
    }

    it('works in async mode', function (done) {
      p.parse(function (err, station) {
        expect(err).to.be.null;

        validate(station);
        expect(station).to.be.equal(p.station);

        done();
      });
    });

    it('works in sync mode', function () {
      validate(p.sync.parse());
    });
  });
});
