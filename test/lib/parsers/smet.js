/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('SMETParser', function () {
  'use strict';

  var Meteo  = niviz.Meteo;
  var Parser = niviz.Parser;

  var StringParser  = Parser.format.string;
  var SMETParser    = Parser.format.smet;


  it('is a constructor function', function () {
    expect(SMETParser).to.be.a('function');
  });

  it('creates string parser instances', function () {
    expect(new SMETParser()).to.be.instanceof(Parser)
      .and.instanceof(StringParser);
  });

  describe('.converters', function () {
    describe('timestamp', function () {
      var fn = SMETParser.converter.timestamp.bind({ tz: '+02:00' });

      it('returns a valid moment for good dates', function () {
        expect(fn('2011-08-19T05:23:11').isValid()).to.be.true;
      });

      it('returns an invalid moment for bad dates', function () {
        expect(fn('2011-088-12').isValid()).to.be.false;
      });
    });
  });


  describe('#parse_line', function () {
    var parser;

    beforeEach(function () {
      parser = new SMETParser();

      sinon.spy(parser, 'parse_signature');
      sinon.spy(parser, 'parse_header');
      sinon.spy(parser, 'parse_ascii_data');
      sinon.spy(parser, 'parse_binary_data');
    });

    it('skips blank lines and comments', function () {
      parser.parse_line('');
      parser.parse_line(' ');
      parser.parse_line('\t');
      parser.parse_line(';foo bar');
      parser.parse_line('#foo bar bar');
      parser.parse_line('#[FOO]');
      parser.parse_line(' ;foo bar');
      parser.parse_line('\t#foo bar');

      expect(parser.parse_signature).to.not.have.been.called;
      expect(parser.parse_header).to.not.have.been.called;
      expect(parser.parse_ascii_data).to.not.have.been.called;
      expect(parser.parse_binary_data).to.not.have.been.called;
      expect(parser.mode).to.eql('signature');
    });

    it('parses signature in signature mode', function () {
      parser.parse_line('SMET 0.95 ASCII');
      expect(parser).to.have.property('version', '0.95');

      parser.mode = 'signature';
      parser.parse_line('SMET 0.95.1 ASCII');
      expect(parser).to.have.property('version', '0.95.1');

      parser.mode = 'signature';
      parser.parse_line('SMET 0.95 ASCII');
      expect(parser).to.have.property('type', 'ASCII');

      parser.mode = 'signature';
      parser.parse_line('SMET 0.95 BINARY');
      expect(parser).to.have.property('type', 'BINARY');

      expect(parser.parse_header).to.not.have.been.called;
      expect(parser.parse_ascii_data).to.not.have.been.called;
      expect(parser.parse_binary_data).to.not.have.been.called;
    });

    it('fails on signature in other modes', function () {
      delete parser.mode;
      expect(parser.parse_line.bind(parser, 'SMET 0.95 ASCII')).to.throw('bad section');
    });

    it('switches modes on section headers', function () {
      expect(parser.mode).to.eql('signature');

      parser.parse_line('SMET 0.95 BINARY');
      expect(parser.mode).to.be.undefined;

      parser.parse_line('[DATA]');
      expect(parser.mode).to.eql('data');

      parser.parse_line('[HEADER]');
      expect(parser.mode).to.eql('header');

      expect(parser.parse_header).to.not.have.been.called;
      expect(parser.parse_ascii_data).to.not.have.been.called;
      expect(parser.parse_binary_data).to.not.have.been.called;
    });

    it('parses header lines in header mode', function () {
      parser.mode = 'header';

      parser.parse_line('station_id = foo');
      expect(parser.meteo).to.have.property('id', 'foo');

      parser.parse_line('altitude   =     2440');
      expect(parser.meteo.position).to.have.property('altitude', 2440);

      parser.parse_line('nodata = 0');
      expect(parser).to.have.property('nodata', 0);

      parser.parse_line('tz = +01');
      expect(parser).to.have.property('tz', '+01:00');

      parser.parse_line('tz = -01');
      expect(parser).to.have.property('tz', '-01:00');

      parser.parse_line('fields = foo  bar\tbaz');
      expect(parser.fields).to.eql(['foo', 'bar', 'baz']);

      expect(parser.parse_signature).to.not.have.been.called;
      expect(parser.parse_ascii_data).to.not.have.been.called;
      expect(parser.parse_binary_data).to.not.have.been.called;
    });

    it('fails on header lines in other modes', function () {
      expect(parser.parse_line.bind(parser, 'nodata = 0'))
        .to.throw('bad section');
    });
  });

  describe('parsing example.smet', function () {
    var p;

    beforeEach(function () {
      p = new SMETParser(fixtures('example.smet'));
    });

    function validate(meteo) {
      expect(p.nodata).to.eql(-999);
      expect(p.tz).to.eql('+01:00');
      expect(p.fields).to.eql(['timestamp', 'TA', 'RH', 'VW', 'ISWR']);

      expect(meteo).to.be.instanceof(Meteo);

      expect(meteo.position).to.have.property('latitude', 46.5);
      expect(meteo.position).to.have.property('altitude', 1500);

      expect(meteo.data).to.have.length(3);

      expect(meteo.data[0].timestamp.isValid()).to.be.true;
      expect(meteo.data[0].timestamp.format('YYYYMMDD')).to.eql('20100622');
      expect(meteo.data[0].timestamp.utc().hour()).to.eql(11);

      expect(meteo.data[0].ta).to.eql(2 + 273.15);
      expect(meteo.data[0].rh).to.eql(52 * 0.01);
      expect(meteo.data[0].vw).to.eql(1.2);
      expect(meteo.data[0].iswr).to.eql(320);
    }

    it('works in async mode', function (done) {
      p.parse(function (err, meteo) {
        expect(err).to.be.null;

        validate(meteo);
        expect(meteo).to.be.equal(p.meteo);

        done();
      });
    });

    it('works in sync mode', function () {
      validate(p.sync.parse());
    });
  });
});
