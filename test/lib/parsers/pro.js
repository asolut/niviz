/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('ProParser', function () {
  'use strict';

  var Station      = niviz.Station;
  var Parser       = niviz.Parser;
  var Profile      = niviz.Profile;
  var Feature      = niviz.Feature;
  var Value        = niviz.Value;

  var StringParser = Parser.format.string;
  var ProParser    = Parser.format.pro;


  it('is a constructor function', function () {
    expect(ProParser).to.be.a('function');
  });

  it('creates string parser instances', function () {
    expect(new ProParser()).to.be.instanceof(Parser)
      .and.instanceof(StringParser);
  });

  describe('.converters', function () {
    describe('Date', function () {
      var fn = ProParser.converter.Date;

      it('returns a valid moment for good dates', function () {
        expect(fn('11.08.1980 05:23').isValid()).to.be.true;
      });

      it('returns an invalid moment for bad dates', function () {
        expect(fn('111.08.1980').isValid()).to.be.false;
      });
    });

    describe('nElems', function () {
      var fn = ProParser.converter.nElems;

      it('returns an empty list for empty input', function () {
        expect(fn('')).to.eql([]);
        expect(fn('0')).to.eql([]);
      });

      it('returns a list of numbers for valid input', function () {
        expect(fn('2,7.916e-002,6.846e-002')).to.eql([0.07916, 0.06846]);
        expect(fn('10,1,2,3')).to.eql([1, 2, 3]);
      });

      it('fails if there are more elements than n', function () {
        expect(fn.bind(null, '1,1,2,3')).to.throw('buffer overflow');
      });
    });
  });

  describe('#skip', function () {
    var parser;

    beforeEach(function () {
      parser = new ProParser();
    });

    it('returns the next line by default', function () {
      parser.data = '[foo]\n[bar]\r\n[baz]';

      expect(parser.skip()).to.eql('[foo]');
      expect(parser.skip()).to.eql('[bar]');
      expect(parser.skip()).to.eql('[baz]');
      expect(parser.skip()).to.be.undefined;
    });

    it('skips blank lines', function () {
      parser.data = '\n\n  \n \n[qux]';
      expect(parser.skip()).to.eql('[qux]');
    });

    it('skips comments', function () {
      parser.data = '#foo\n"bar\n;baz\n[qux]';
      expect(parser.skip()).to.eql('[qux]');
    });

    it('returns undefined if next line starts with until', function () {
      //             0--------7-----13----18----
      parser.data = '#foo\n \n[bar]\n;baz\n[qux]';

      expect(parser.position).to.eql(0);
      expect(parser.skip('[bar]')).to.be.undefined;
      expect(parser.position).to.eql(7);

      expect(parser.skip('[qux]')).to.eql('[bar]');
      expect(parser.position).to.eql(13);
      expect(parser.skip('[qux]')).to.be.undefined;
      expect(parser.position).to.eql(18);
      expect(parser.skip('[foo]')).to.eql('[qux]');
      expect(parser.skip('[foo]')).to.be.undefined;
    });
  });

  describe('#parse_line', function () {
    var parser;

    beforeEach(function () {
      parser = new ProParser();

      sinon.spy(parser, 'parse_parameter');
      sinon.spy(parser, 'parse_header');
      sinon.spy(parser, 'parse_data');
    });

    it('skips blank lines and comments', function () {
      parser.parse_line('');
      parser.parse_line(' ');
      parser.parse_line('\t');
      parser.parse_line(';foo bar');
      parser.parse_line('#foo bar');
      parser.parse_line('"foo bar');
      parser.parse_line(' ;foo bar');
      parser.parse_line('\t#foo bar');
      parser.parse_line('\t "foo bar');

      expect(parser.parse_parameter).to.not.have.been.called;
      expect(parser.parse_header).to.not.have.been.called;
      expect(parser.parse_data).to.not.have.been.called;
      expect(parser.mode).to.be.undefined;
    });

    it('parses parameters in station_parameters mode', function () {
      parser.mode = 'station_parameters';

      parser.parse_line('StationName=bar');
      expect(parser.station).to.have.property('name', 'bar');

      parser.parse_line('stationName  =  baz');
      expect(parser.station).to.have.property('name', 'baz');

      parser.parse_line('foo  =  baz');
      expect(parser.station).to.not.have.property('foo');

      parser.parse_line('Latitude=  37.03');
      expect(parser.station.position).to.have.property('latitude', 37.03);

      parser.parse_line('Longitude =0.1');
      expect(parser.station.position).to.have.property('longitude', 0.1);

      parser.parse_line('SlopeAzi=0.00');
      expect(parser.station.position).to.have.property('azimuth', 0);

      expect(parser.parse_header).to.not.have.been.called;
      expect(parser.parse_data).to.not.have.been.called;
    });

    it('fails on parameters in other modes', function () {
      expect(parser.parse_line.bind(parser, 'foo=bar')).to.throw('bad section');
    });

    it('switches modes on section headers', function () {
      expect(parser.mode).to.be.undefined;

      parser.parse_line('[DATA]');
      expect(parser.mode).to.eql('data');

      parser.parse_line('[STATION_PARAMETERS]');
      expect(parser.mode).to.eql('station_parameters');

      parser.parse_line('[HEADER]');
      expect(parser.mode).to.eql('header');

      expect(parser.parse_parameter).to.not.have.been.called;
      expect(parser.parse_header).to.not.have.been.called;
      expect(parser.parse_data).to.not.have.been.called;
    });

    it('parses header lines in header mode', function () {
      parser.mode = 'header';

      parser.parse_line('0500,Date');
      expect(parser.properties).to.have.property('0500');

      parser.parse_line('0512,nElems,grain size:');
      expect(parser.properties).to.have.property('0512');
      expect(parser.properties['0512'])
        .to.have.property('description', 'grain size:');

      expect(parser.parse_parameter).to.not.have.been.called;
      expect(parser.parse_data).to.not.have.been.called;
    });

    it('parses indented multi-line headers in header mode', function () {
      parser.mode = 'header';

      parser.data = ' in mm';
      parser.parse_line('0512,nElems,grain size:');
      expect(parser.properties).to.have.property('0512');
      expect(parser.properties['0512'])
        .to.have.property('description', 'grain size:  in mm');
    });

    it('parses unindented multi-line headers in header mode', function () {
      parser.mode = 'header';

      parser.data = 'in mm';
      parser.parse_line('0512,nElems,grain size:');
      expect(parser.properties).to.have.property('0512');
      expect(parser.properties['0512'])
        .to.have.property('description', 'grain size: in mm');
    });

    it('fails on header lines in other modes', function () {
      expect(parser.parse_line.bind(parser, '0500,Date'))
        .to.throw('bad section');
    });

    it('fails for invalid lines', function () {
      expect(parser.parse_line.bind(parser, 'foo')).to.throw('unmatched line');
      expect(parser.parse_line.bind(parser, '[f')).to.throw('unmatched line');

      parser.mode = 'station_parameters';
      expect(parser.parse_line.bind(parser, 'f o=o'))
        .to.throw('unmatched line');
    });
  });

  describe('parsing min.pro', function () {
    var p;

    beforeEach(function () {
      p = new ProParser(fixtures('min.pro'));
    });

    function validate(station) {
      expect(station).to.be.instanceof(Station);

      expect(station.position).to.have.property('latitude', 45);
      expect(station.position).to.have.property('altitude', 2000);

      expect(station.profiles).to.have.length(5);

      var profile = station.get(-1);

      expect(profile).to.be.instanceof(Profile);
      expect(profile).to.have.property('height', 25.69);
      expect(profile).to.contain.keys('density', 'grainshape', 'grainsize');

      expect(profile.grainshape).to.be.instanceof(Feature);
      expect(profile.grainshape.layers[0]).to.be.instanceof(Value);
      expect(profile.grainshape.layers[0]).to.be.instanceof(Value.GrainShape);
      expect(profile.grainshape.layers[0]).to.have.property('height', 1.54);
      expect(profile.grainshape.layers[0]).to.have.property('code', 'c(e)');

      expect(profile.density).to.be.instanceof(Feature);
      expect(profile.density.layers[0]).to.be.instanceof(Value);
      expect(profile.density.layers[0]).to.have.property('height', 1.54);

      expect(profile.grainsize).to.be.instanceof(Feature);
      expect(profile.grainsize.layers[0]).to.be.instanceof(Value);
      expect(profile.grainsize.layers[0]).to.be.instanceof(Value.GrainSize);
      expect(profile.grainsize.layers[0]).to.have.property('text', 'medium');

      expect(profile.grainsize.min).to.eql(0.30);
      expect(profile.grainsize.max).to.eql(0.53);
    }

    it('works in async mode', function (done) {
      p.parse(function (err, station) {
        expect(err).to.be.null;

        expect(p.properties).to.have.property('0500');

        // multi-line header!
        expect(p.properties)
          .to.have.property('0530')
          .and.have.property('description')
          .and.match(/indices:\s+profile type/);

        validate(station);
        expect(station).to.be.equal(p.station);

        done();
      });
    });

    it('works in sync mode', function () {
      validate(p.sync.parse());
    });
  });
});
