/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('CAAMLParser', function () {
  'use strict';

  var Parser      = niviz.Parser;
  var CAAMLParser = Parser.format.caaml;

  var Station     = niviz.Station;
  var Profile     = niviz.Profile;
  var Feature     = niviz.Feature;
  var Value       = niviz.Value;

  it('is a constructor function', function () {
    expect(CAAMLParser).to.be.a('function');
  });

  it('creates parser instances', function () {
    expect(new CAAMLParser()).to.be.instanceof(Parser);
  });

  describe('parsing a full profile', function () {
    var parser;

    beforeEach(function () {
      parser = new CAAMLParser(fixtures('fz_snowprofile_53930_de.caaml'));
    });

    function validate(station) {
      expect(station).to.be.instanceof(Station);

      expect(station.name).to.eql('Davos - Strela - Steintälli -  WAN7');

      expect(station.position)
        .to.have.keys('angle', 'altitude', 'latitude', 'longitude');

      expect(station.profiles).to.have.length(1);

      var profile = station.get(0);

      expect(profile).to.be.instanceof(Profile);

      expect(profile.date.isValid()).to.be.true;
      expect(profile.date.year()).to.eql(2009);

      expect(profile).to.have.property('height', 183);
      expect(profile).to.contain.keys('temperature', 'density', 'thickness');

      expect(profile.temperature).to.be.instanceof(Feature);

      expect(profile.temperature.min).to.eql(-18.5);
      expect(profile.temperature.max).to.be.closeTo(0, 0.3);

      expect(profile.temperature.layers.length).to.eql(21);

      var layer = profile.temperature.layers[20];

      expect(layer).to.be.instanceof(Value);
      expect(layer.height).to.eql(183);
      expect(layer.value).to.eql(-18.5);

      expect(profile.density).to.be.instanceof(Feature);
      expect(profile.temperature.layers.length).to.eql(21);

      layer = profile.density.layers[20];

      expect(layer.height).to.eql(183);
      expect(layer.value).to.eql(119);

      expect(profile).to.have.property('hardness')
        .and.to.be.instanceof(Feature);

      expect(profile.hardness.layers.length).to.eql(17);

      layer = profile.hardness.layers[16];

      expect(layer).to.be.instanceof(Value.Hardness);

      expect(layer.height).to.eql(183);
      expect(layer.code).to.eql('F-4F');

      expect(profile).to.have.property('grainshape')
        .and.to.be.instanceof(Feature);

      expect(profile.grainshape)
        .to.have.property('layers')
        .and.have.length(17);

      layer = profile.grainshape.layers[16];

      expect(layer).to.be.instanceof(Value.GrainShape);
      expect(layer.height).to.eql(183);
      expect(layer.code).to.eql('c(a)');
      expect(layer.text).to.eql('c(a)');
      expect(layer.color).to.eql('#228B22');

      //expect(profile).to.have.property('grainsize');
    }

    it('works in async mode', function (done) {
      parser.async.parse(function (error, station) {
        expect(error).to.be.null;
        validate(station);

        done();
      });
    });

    it('works in sync mode', function () {
      validate(parser.sync.parse());
    });
  });
});
