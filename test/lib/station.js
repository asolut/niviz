/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('niviz.Station', function () {
  'use strict';

  var Station  = niviz.Station;
  var Range    = niviz.Range;
  var Position = niviz.Position;

  it('is a constructor function', function () {
    expect(Station).to.be.a('function');
  });

  describe('an instance', function () {
    var station;

    beforeEach(function () {
      station = new Station();
    });

    it('has a position', function () {
      expect(station.position).to.be.instanceof(Position);
    });

    it('is a range', function () {
      expect(station).to.be.instanceof(Range);
    });

    it('is empty', function () {
      expect(station.empty).to.be.true;
    });
  });

});
