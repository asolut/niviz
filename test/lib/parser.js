/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('Parser', function () {
  'use strict';

  var Parser = niviz.Parser;

  it('is a constructor function', function () {
    expect(Parser).to.be.a('function');
  });

  describe('.formats', function () {
    it('contains supported formats', function () {
      expect(Parser.formats).to.contain('string');
      expect(Parser.formats).to.contain('xml');
      expect(Parser.formats).to.contain('pro');
      expect(Parser.formats).to.contain('caaml');
      expect(Parser.formats).to.contain('json');
      expect(Parser.formats).to.contain('smet');
    });
  });
});
