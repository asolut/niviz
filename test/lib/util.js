/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('niviz.util', function () {
  'use strict';

  var util = niviz.util;

  it('is a namespace object', function () {
    expect(util).to.be.an('object');
  });

  describe('.format', function () {
    it('works when there are no patterns', function () {
      expect(util.format('')).to.eql('');
      expect(util.format('foo')).to.eql('foo');
    });

    it('works like sprintf when there are patterns', function () {
      expect(util.format('%s', 'foo')).to.eql('foo');
      expect(util.format('%d', 42)).to.eql('42');
      expect(util.format('fo%s %s', 'o', 'bar')).to.eql('foo bar');
    });

    it('works with arrays', function () {
      expect(util.format(['%s', 'foo'])).to.eql('foo');
      expect(util.format(['%d', 42])).to.eql('42');
      expect(util.format(['fo%s %s', 'o', 'bar'])).to.eql('foo bar');
    });
  });

  describe('.underscore', function () {
    it('converts camel case to snake case', function () {
      expect(util.underscore('')).to.eql('');
      expect(util.underscore('foo')).to.eql('foo');
      expect(util.underscore('fooBar')).to.eql('foo_bar');
      expect(util.underscore('FOOBAR')).to.eql('foobar');
    });
  });
});
