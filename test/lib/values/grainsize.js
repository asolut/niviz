/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('niviz.Value.GrainSize', function () {
  'use strict';

  var Feature   = niviz.Feature;
  var GrainSize = niviz.Value.GrainSize;

  it('is a constructor function', function () {
    expect(GrainSize).to.be.a('function');
  });

  it('has symbol E', function () {
    expect(Feature.type.grainsize.symbol).to.eql('E');
  });

  describe('.text', function () {
    it('returns undefined by default', function () {
      expect((new GrainSize()).text).to.be.undefined;
    });

    it('returns the term corresponding to the value', function () {
      expect((new GrainSize(0, 0)).text).to.eql('very fine');
      expect((new GrainSize(0, 0.25)).text).to.eql('fine');
      expect((new GrainSize(0, 0.55)).text).to.eql('medium');
      expect((new GrainSize(0, 1.55)).text).to.eql('coarse');
      expect((new GrainSize(0, 2.55)).text).to.eql('very coarse');
      expect((new GrainSize(0, 5.55)).text).to.eql('extreme');
    });
  });
});
