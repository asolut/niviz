/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('niviz.Value.Hardness', function () {
  'use strict';

  var Hardness = niviz.Value.Hardness;

  it('is a constructor function', function () {
    expect(Hardness).to.be.a('function');
  });

  describe('constructor', function () {
    it('automatically converts code/newton', function () {
      expect(new Hardness(0, '4F')).to.have.property('value', 2);
      expect(new Hardness(0, '1F-P')).to.have.property('value', 3.5);
      expect(new Hardness(0, 9.34)).to.have.property('value', 1);
      expect(new Hardness(0, 2042)).to.have.property('value', 6);

      expect(new Hardness(0, 413)).to.have.property('value')
        .and.be.closeTo(4, 0.5);
    });
  });

  describe('.value', function () {
    it('must be between 1.0 and 6.0', function () {
      var h = new Hardness();
      function set(v) { h.value = v; }

      expect(set.bind(null, 1)).not.to.throw(Error);
      expect(h.value).to.eql(1);

      expect(set.bind(null, 1.1)).not.to.throw(Error);
      expect(h.value).to.eql(1.1);

      expect(set.bind(null, 6)).not.to.throw(Error);
      expect(h.value).to.eql(6);

      expect(set.bind(null, 0)).to.throw(Error);
      expect(set.bind(null, 0.9)).to.throw(Error);
      expect(set.bind(null, 6.001)).to.throw(Error);

      expect(set.bind(null)).not.to.throw(Error);
      expect(h.value).to.be.undefined;

      expect(set.bind(null, null)).not.to.throw(Error);
      expect(h.value).to.be.undefined;
    });
  });

  describe('.newton', function () {
    it('is undefined by default', function () {
      expect((new Hardness()).newton).to.be.undefined;
    });

    it('returns the index value as ram resistance', function () {
      expect((new Hardness(0, 1)).newton).to.be.closeTo(20, 1);
      expect((new Hardness(0, 2)).newton).to.be.closeTo(101, 1);
      expect((new Hardness(0, 3)).newton).to.be.closeTo(270, 1);
      expect((new Hardness(0, 4)).newton).to.be.closeTo(538, 1);
      expect((new Hardness(0, 5)).newton).to.be.closeTo(919, 1);
      expect((new Hardness(0, 5.5)).newton).to.be.closeTo(1155, 1);
      expect((new Hardness(0, 6)).newton).to.eql(1000);
    });

    it('caches the current value', function () {
      var h = new Hardness();

      expect(h.$newton).to.be.undefined;
      expect(h.newton).to.be.undefined;
      expect(h.$newton).to.be.undefined;

      h.value = 6;

      expect(h.$newton).to.be.undefined;
      expect(h.newton).to.eql(1000);
      expect(h.$newton).to.eql(1000);

      h.newton = 50;

      expect(h.$newton).to.be.undefined;
      expect(h.newton).to.be.closeTo(50, 0.0001);
      expect(h.$newton).to.be.closeTo(50, 0.0001);
    });

    it('sets the index when assigned a value', function () {
      var h = new Hardness();

      expect(h.value).to.be.undefined;

      h.newton = 1300;
      expect(h.value).to.eql(6);

      h.newton = 101;
      expect(h.value).to.be.closeTo(2, 0.1);

      h.newton = 0;
      expect(h.value).to.eql(1);
    });
  });

  describe('.code', function () {
    it('is undefined by default', function () {
      expect((new Hardness()).code).to.be.undefined;
    });

    it('returns the index value as an HHI code string', function () {
      expect((new Hardness(0, 1)).code).to.eql('F');
      expect((new Hardness(0, 2)).code).to.eql('4F');
      expect((new Hardness(0, 3)).code).to.eql('1F');
      expect((new Hardness(0, 4)).code).to.eql('P');
      expect((new Hardness(0, 5)).code).to.eql('K');
      expect((new Hardness(0, 6)).code).to.eql('I');
    });

    it('returns a range string for index fractions', function () {
      expect((new Hardness(0, 1.5)).code).to.eql('F-4F');
      expect((new Hardness(0, 2.5)).code).to.eql('4F-1F');
      expect((new Hardness(0, 3.5)).code).to.eql('1F-P');
      expect((new Hardness(0, 4.5)).code).to.eql('P-K');
      expect((new Hardness(0, 5.5)).code).to.eql('K-I');
    });
  });

  describe('JSON', function () {
    it('returns an empty value by default', function () {
      expect(JSON.stringify(new Hardness())).to.eql('{}');
    });

    it('exports height and value', function () {
      var h = new Hardness(42, 3);
      var o = JSON.parse(JSON.stringify(h));

      expect(o).to.have.keys('value', 'height');
      expect(o.value).to.eql(3);

      h.code = 'P';

      o = JSON.parse(JSON.stringify(h));

      expect(o).to.have.keys('value', 'height');
      expect(o.value).to.eql(4);
    });
  });
});
