/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('Profile', function () {
  'use strict';

  var Profile = niviz.Profile;

  it('is a constructor function', function () {
    expect(Profile).to.be.a('function');
  });

  describe('JSON Export', function () {
    it('exports only the date by default', function () {
      expect(JSON.stringify(new Profile())).to.match(/^\{"date":"[^"]+"\}$/);
    });

    it('exports the date as an ISO string', function () {
      var obj = JSON.parse(JSON.stringify(new Profile()));

      expect(obj.date).to.be.a('string');
      expect(obj.date).to.match(/^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d.\d+Z$/);
    });

    it('exports known features if present', function () {
      var p = new Profile();

      expect(JSON.parse(JSON.stringify(p))).to.have.keys('date');

      p.add('ramm', []);
      expect(JSON.parse(JSON.stringify(p))).to.have.keys('date', 'ramm');

      p.temperature = {};
      expect(JSON.parse(JSON.stringify(p))).to.have.keys('date', 'ramm');
    });
  });
});
