/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

describe('niviz.Value', function () {
  'use strict';

  var Value = niviz.Value;

  it('is a constructor function', function () {
    expect(Value).to.be.a('function');
  });

  describe('JSON Export', function () {
    it('is an empty object by default', function () {
      expect(JSON.stringify(new Value())).to.eql('{}');
    });

    it('contains height and value if present', function () {
      expect(JSON.stringify(new Value(0))).to.eql('{"height":0}');
      expect(JSON.stringify(new Value(0, 0))).to.eql('{"height":0,"value":0}');
    });
  });
});
