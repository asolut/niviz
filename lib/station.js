/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

(function (niviz, moment) {
  'use strict';

  // --- Module Dependencies ---
  var properties   = Object.defineProperties;
  var Range        = niviz.Range;
  var Meteo        = niviz.Meteo;
  var Position     = niviz.Position;
  var EventEmitter = niviz.EventEmitter;
  var t            = niviz.Translate.gettext;

  var inherits = niviz.util.inherits;
  var pick     = niviz.util.pick;

  /** @module niviz */

  /**
   * A Snow Station.
   *
   * @class Station
   * @constructor
   * @extends Range
   */
  function Station() {
    Range.call(this);

    /**
     * @property name
     * @type String
     */
    this.name;

    /**
     * @property id
     * @type String
     */
    this.id = niviz.CAAML.defaults.station_gmlid;

    /**
     * @property position
     * @type Position
     */
    this.position = new Position();

    /**
     * @property creationdate
     * @type moment
     */
    this.creationdate = moment();

    /**
     * @property lastedited
     * @type moment
     */
    this.lastedited = moment();

    /**
     * @property emitter
     * @type EventEmitter
     */
    this.emitter = new EventEmitter();
  }

  inherits(Station, Range);

  properties(Station.prototype, {
    /**
     * Collected metadata in one object.
     *
     * @property meta
     * @type Object
     */
    meta: {
      get: function () {
        var obj = {};

        if (this.position) obj.position = this.position.clone();
        if (this.name) obj.name = this.name;
        if (this.description) obj.description = this.description;
        if (this.id) obj.id = this.id;
        if (this.observer) obj.observer = this.observer;

        return obj;
      },
      set: function (obj) {
        if (obj.observer) this.observer = obj.observer;
        if (obj.position) this.position = obj.position;
        if (obj.name) this.name = obj.name;
        if (obj.description) this.description = obj.description;
        if (obj.id) this.id = obj.id;
      }
    }
  });

  /**
   * Extract a time series for a certain parameter from the profiles
   *
   * @method extract
   *
   * @param {String} parameter Parameter name
   * @param {Number} height Distance from top or 0 (default)
   * @param {Boolean} top If true height will be interpreted as depth from layers' top
   * @param {String} func Which function to use (only 'mean' implemented)
   * @return {Meteo}
   */
  Station.prototype.extract = function (parameter, height, top, func) {
    var meteo = new Meteo(), data, i, j, ii = this.profiles.length;
    if (this.position) meteo.position = this.position.clone();
    if (this.name) meteo.name = this.name;
    if (this.id) meteo.id = this.id;
    if (this.filename) meteo.filename = this.filename;
    if (ii) meteo.fields = ['timestamp', parameter];

    var profile = {}, param, last, unit, name;
    for (i = 0; i < ii; ++i) {
      profile = this.profiles[i];

      data = new Meteo.Data();
      data['timestamp'] = moment(profile.date);
      data[parameter] = null; // default value

      param = profile[parameter];
      if (param) {
        if (!unit && param.unit) unit = param.unit;
        if (!name && param.name) name = param.name;
        data[parameter] = param.elements[0].atheight(height, top);
      }

      meteo.push(data);

      if (data[parameter] !== null) {
        if (meteo.min[parameter] === undefined || data[parameter] < meteo.min[parameter])
          meteo.min[parameter] = data[parameter];

        if (meteo.max[parameter] === undefined || data[parameter] > meteo.max[parameter])
          meteo.max[parameter] = data[parameter];
      }
    }

    meteo.plot[parameter] = {
      description: t(name) + ' @' + height + ' cm',
      unit: unit,
      color: '#113'
    };

    return meteo;
  };

  Station.prototype.toJSON = function () {
    return pick(this, ['id', 'name', 'observer', 'position', 'profiles', 'lastedited', 'creationdate']);
  };

  // --- Module Export ---
  niviz.Station = Station;

}(niviz, moment));
