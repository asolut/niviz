/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* eslint no-loop-func: [0] */
(function (niviz, moment) {
  'use strict';

  // --- Module Dependencies ---
  var Parser  = niviz.Parser;
  var Station = niviz.Station;
  var Profile = niviz.Profile;
  var Feature = niviz.Feature;

  var alias   = niviz.util.alias;
  var extend  = niviz.util.extend;


  /** @module niviz */

  /**
   * A JSON parser.
   *
   * @class JSONParser
   * @constructor
   *
   * @extends Parser
   *
   * @param {String} [data=''] The data to parse.
   */
  function JSONParser(data) {
    Parser.call(this, data || '');

    /**
     * The snow station instance; the instance should
     * be populated by the parsing methods and represents
     * the parse result.
     *
     * @property station
     * @type Station
     */
    this.station = new Station();
    alias(this, 'result', 'station');
  }

  Parser.implement(JSONParser, 'json');


  /**
   * @property _parse
   * @returns {Object} The parse result.
   */
  JSONParser.prototype._parse = function () {
    try {
      this.parse_station(JSON.parse(this.data));

    } catch (error) {
      return this.emit('error', error);
    }

    this.end();
    return this.result;
  };

  /**
   * Parsers a simple station object.
   *
   * @method parse_station
   * @chainable
   *
   * @param {Object} obj
   */
  JSONParser.prototype.parse_station = function (obj) {
    var i, ii;

    if (!obj || typeof obj !== 'object')
      throw new Error('invalid station object');

    this.station.name     = obj.name;
    this.station.id       = obj.id;
    this.station.observer = obj.observer;

    this.station.creationdate = moment(obj.creationdate);
    this.station.lastedited   = moment(obj.lastedited);

    if (obj.position)
      extend(this.station.position, obj.position);

    if (obj.profiles) {

      for (i = 0, ii = obj.profiles.length; i < ii; ++i)
        this.parse_profile(obj.profiles[i]);

    }

    return this;
  };

  /**
   * Parsers a simple profile object
   * and adds it to the parser's station.
   *
   * @method parse_profile
   * @chainable
   *
   * @param {Object} profile
   */
  JSONParser.prototype.parse_profile = function (obj) {

    if (!obj || typeof obj !== 'object')
      throw new Error('invalid profile object');

    var type;
    var date = moment(obj.date, moment.ISO_8601, true);

    if (!date.isValid())
      throw new Error('invalid profile date: ' + obj.date);

    var profile = new Profile(date);

    for (type in obj) {
      if (type === 'date' || !obj.hasOwnProperty(type))
        continue;

      if (Feature.type[type] && obj[type].elements) {
        obj[type].elements.forEach(function (e, i) {
          if (e && e.layers) {
            profile.add(type, e.layers, i);
            if (e.meta) profile[type].elements[profile[type].elements.length - 1].meta = e.meta;
          }
        });
      } else {
        profile[type] = obj[type];
      }

    }

    this.station.push(profile);
    this.emit('profile', profile);

    return this;
  };

}(niviz, moment));
