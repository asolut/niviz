/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var Common     = niviz.Common;
  var Dictionary = niviz.Dictionary;

  /** @module niviz */

  /**
   * Translation service.
   *
   * @class Translate
   * @static
   * @constructor
   */
  function Translate() {}

  /**
   * Get the translation of a word or phrase. The target language
   * is predefined in the general settings. If the key is not present
   * in the dictionary return the key as it is.
   *
   * @method gettext
   * @static
   * @param {String} key Word or phrase to translate (in English)
   * @param {String} [context] Context within the dictionary (e. g. grainshape)
   */
  Translate.gettext = function (key, context) {
    var lang = Common.defaults.language;

    if (context && Dictionary[lang][context]) {
      return Dictionary[lang][context][key] || key;
    }

    return Dictionary[lang][key] || key;
  };

  // --- Module Export ---
  niviz.Translate = Translate;

}(niviz));
