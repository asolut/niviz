/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var Config = niviz.Config;

  /** @module niviz */

  /**
   * Common settings used by several configs and the general settings config.
   *
   * @class Common
   * @constructor
   */
  var Common = {};

  var language = navigator.language || navigator.userLanguage,
    languages = ['chinese', 'czech', 'deutsch', 'english', 'español', 'francais', 'italiano'];

  function get_default_language () {
    if (!language || language.length < 2) return 'english';

    var i, ii = languages.length, ss = language.substring(0, 2);

    for (i = 0; i < ii; ++i) {
      if (ss === languages[i].substring(0, 2)) return languages[i];
    }

    return 'english';
  }

  /**
   * The general settings.
   *
   * @property defaults
   * @type Array<Object>
   * @static
   */
  Common.defaults = new Config('General', [
    {
      name: 'preferred_profile_graph', type: 'string', default: 'SimpleProfile',
      values: ['SimpleProfile', 'SLFProfile', 'StructureProfile', 'MobileProfile']
    },
    { name: 'show_tooltips', type: 'boolean', default: true },
    { name: 'autoscale', type: 'boolean', default: true },
    {
      name: 'max_snowheight', type: 'number', default: '',
      hint: 'max_snowheight_hint'
    },
    {
      name: 'hand_hardness_format', type: 'string', default: 'International',
      values: ['North America', 'International']
    },
    {
      name: 'LWC_format', type: 'string', default: '1 - 5',
      values: ['D, M, W, V, S', '1 - 5']
    },
    {
      name: 'ECT_format', type: 'string', default: 'International',
      values: ['Swiss', 'International']
    },
    {
      name: 'display_yellow_flags', type: 'string', default: 'as flags',
      values: ['as flags', 'as asterisks']
    },
    {
      name: 'map_service', type: 'string', default: 'map.geo.admin.ch',
      values: ['maps.google.com', 'map.geo.admin.ch', 'mapy.cz']
    },
    {
      name: 'grain_shape_color_palette', type: 'string', default: '_gscp_standard',
      values: ['_gscp_standard', '_gscp_perception_informed']
    },
    {
      name: 'language', type: 'string', default: get_default_language(),
      values: languages
    },
    {
      name: 'PNG_background', type: 'string', default: 'transparent',
      values: ['transparent', 'white']
    }
  ]);

  Common.defaults.load();

  /**
   * The general settings.
   *
   * @property defaults
   * @type Object
   * @static
   */
  Common.show_arrows = {
    name: 'show_arrows', type: 'select2',
    default: [
      {id: 1, name: 'rb'}, {id: 2, name: 'ect'},
      {id: 3, name: 'ct'}, {id: 4, name: 'sk38'},
      {id: 5, name: 'saw'}, {id: 6, name: 'sf'}],
    values: [ { id: 1, name: 'rb', display: 'Rutschblock'},
              { id: 2, name: 'ect', display: 'Extended Column Test'},
              { id: 3, name: 'ct', display: 'Compression Test'},
              { id: 4, name: 'sk38', display: 'SK38'},
              { id: 5, name: 'saw', display: 'Propagation Saw Test'},
              { id: 6, name: 'sf', display: 'Shear Frame Test'} ]
  };

  /**
   * Other parameters to be selected by the user for display as columns in the layer table.
   *
   * @property other_parameters
   * @type Object
   * @static
   */
  Common.other_parameters = {
    name: 'other_parameters', type: 'select2',
    default: [
      {id: 1, name: 'density', width: 4}, {id: 2, name: 'threads', width: 5}],
    values: [ { id: 1, name: 'density', display: 'Density', width: 4 },
              { id: 2, name: 'threads', display: 'Threads', width: 5 },
              { id: 3, name: 'comments', display: 'Comments', width: 15 },
              { id: 4, name: 'thickness', display: 'Thickness', width: 4 } ]
  };

  /**
   * Stability parameters to be selected by the user for display as columns in the layer table.
   *
   * @property stability_parameters
   * @type Object
   * @static
   */
  Common.stability_parameters = {
    name: 'stability_parameters', type: 'select2',
    default: [
      {id: 2, name: 'flags', width: 7 },
      {id: 1, name: 'ct', width: 5 },
      {id: 4, name: 'ect', width: 5 },
      {id: 3, name: 'rb', width: 6 },
      {id: 5, name: 'saw', width: 4 },
      {id: 6, name: 'sf', width: 5 }],
    values: [{ id: 1, name: 'ct', display: 'Compression Test', width: 5 },
             { id: 2, name: 'flags', display: 'Yellow Flags', width: 7 },
             { id: 3, name: 'rb', display: 'Rutschblock', width: 6 },
             { id: 4, name: 'ect', display: 'Extended Column Test', width: 5 },
             { id: 5, name: 'saw', display: 'Propagation Saw Test', width: 4 },
             { id: 6, name: 'sf', display: 'Shear Frame Test', width: 5 }]
  };

  /**
   * The header options for the SimpleProfile, StructureProfile and SLFProfile classes.
   * Headers may be omitted, displayed in compact, full form or in full form with
   * a grain shape legend.
   *
   * @property head
   * @type Object
   * @static
   */
  Common.head = {
    name: 'head', type: 'string', default: 'full',
    values: ['none', 'compact', 'full', 'full+legend']
  };

  Common.timezones = [
    { hr: '-12:00', min: -720 }, { hr: '-11:00', min: -660 }, { hr: '-10:00', min: -600 },
    { hr: '-09:30', min: -570 }, { hr: '-09:00', min: -540 }, { hr: '-08:00', min: -480 },
    { hr: '-07:00', min: -420 }, { hr: '-06:00', min: -360 }, { hr: '-05:00', min: -300 },
    { hr: '-04:00', min: -240 }, { hr: '-03:00', min: -180 }, { hr: '-02:00', min: -120 },
    { hr: '-01:00', min: -60 }, { hr: '±00:00', min: 0 }, { hr: '+01:00', min: 60 },
    { hr: '+02:00', min: 120 }, { hr: '+03:00', min: 180 }, { hr: '+03:30', min: 210 },
    { hr: '+04:00', min: 240 }, { hr: '+04:30', min: 270 }, { hr: '+05:00', min: 300 },
    { hr: '+05:30', min: 330 }, { hr: '+05:45', min: 345 }, { hr: '+06:00', min: 360 },
    { hr: '+06:30', min: 390 }, { hr: '+07:00', min: 420 }, { hr: '+08:00', min: 480 },
    { hr: '+08:30', min: 510 }, { hr: '+08:45', min: 525 }, { hr: '+09:00', min: 540 },
    { hr: '+09:30', min: 570 }, { hr: '+10:00', min: 600 }, { hr: '+10:30', min: 630 },
    { hr: '+11:00', min: 660 }, { hr: '+12:00', min: 720 }, { hr: '+12:45', min: 765 }
  ];

  // --- Module Export ---
  niviz.Common = Common;

}(niviz));
