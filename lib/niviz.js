/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//= require namespace
//= require util
//= require config
//= require events
//= require synchronizable
//= require common
//= require graphs/snap_plugins
//= require graph
//= require value
//= require values/wetness
//= require values/ct
//= require values/ect
//= require values/rb
//= require values/sf
//= require values/saw
//= require values/grainshape
//= require values/grainsize
//= require values/hardness
//= require values/ramm
//= require values/smp
//= require values/sk38
//= require features
//= require featureset
//= require profile
//= require range
//= require position
//= require i18n/english
//= require i18n/german
//= require i18n/french
//= require i18n/italian
//= require i18n/spanish
//= require i18n/chinese
//= require i18n/czech
//= require i18n/translate
//= require meteo
//= require station
//= require parser
//= require parsers/string
//= require parsers/xml
//= require parsers/pro
//= require parsers/caaml.slf
//= require parsers/caaml
//= require parsers/json
//= require parsers/smet
//= require graphs/gradient
//= require graphs/visuals
//= require graphs/header
//= require graphs/axis
//= require graphs/grid
//= require graphs/cartesian
//= require graphs/bargraph
//= require graphs/tabular
//= require graphs/slf
//= require graphs/structure
//= require graphs/simple
//= require graphs/mobile
//= require graphs/rangegraph
//= require graphs/meteograph
//= require graphs/timeline
//= require serialize/csv
//= require serialize/caaml5
//= require serialize/caaml

(function (node, ignoreLibs) {
  'use strict';

  if (node && !ignoreLibs) {
    window.moment = require('moment');
    require('snapsvg-cjs');

    require('../lib/namespace');
    require('../lib/util');
    require('../lib/config');
    require('../lib/events');
    require('../lib/synchronizable');
    require('../lib/common');
    require('../lib/graphs/snap_plugins');
    require('../lib/graph');
    require('../lib/value');
    require('../lib/values/wetness');
    require('../lib/values/ct');
    require('../lib/values/ect');
    require('../lib/values/rb');
    require('../lib/values/sf');
    require('../lib/values/saw');
    require('../lib/values/grainshape');
    require('../lib/values/grainsize');
    require('../lib/values/hardness');
    require('../lib/values/ramm');
    require('../lib/values/smp');
    require('../lib/values/sk38');
    require('../lib/features');
    require('../lib/featureset');
    require('../lib/profile');
    require('../lib/range');
    require('../lib/position');
    require('../lib/i18n/english');
    require('../lib/i18n/german');
    require('../lib/i18n/french');
    require('../lib/i18n/italian');
    require('../lib/i18n/spanish');
    require('../lib/i18n/chinese');
    require('../lib/i18n/czech');
    require('../lib/i18n/translate');
    require('../lib/meteo');
    require('../lib/station');
    require('../lib/parser');
    require('../lib/parsers/string');
    require('../lib/parsers/xml');
    require('../lib/parsers/pro');
    require('../lib/parsers/caaml.slf');
    require('../lib/parsers/caaml');
    require('../lib/parsers/json');
    require('../lib/parsers/smet');
    require('../lib/graphs/gradient');
    require('../lib/graphs/visuals');
    require('../lib/graphs/header');
    require('../lib/graphs/axis');
    require('../lib/graphs/grid');
    require('../lib/graphs/cartesian');
    require('../lib/graphs/bargraph');
    require('../lib/graphs/tabular');
    require('../lib/graphs/slf');
    require('../lib/graphs/structure');
    require('../lib/graphs/simple');
    require('../lib/graphs/mobile');
    require('../lib/graphs/rangegraph');
    require('../lib/graphs/meteograph');
    require('../lib/graphs/timeline');
    require('../lib/serialize/csv');
    require('../lib/serialize/caaml5');
    require('../lib/serialize/caaml');

    module.exports = niviz;

  } else if (node && ignoreLibs) {
    module.exports = niviz;
  }

}(typeof process === 'object', !!window.ignoreLibs));
