/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

(function (niviz) {
  'use strict';

  /** @module niviz */
  var properties = Object.defineProperties;

  /**
   * @class synchronizable
   */
  function synchronizable(self) {
    /**
     * Whether or not this instance should operate
     * synchronously; defaults to the value of
     * `niviz.sync` at the time of initialization.
     *
     * @property synchronous
     * @type Boolean
     */
    self.synchronous = niviz.sync;

    properties(self, {
      /**
       * Returns this instance with synchronous
       * mode enabled.
       *
       * @property sync
       * @chainable
       */
      sync: {
        get: function () {
          this.synchronous = true;
          return this;
        }
      },

      /**
       * Returns this instance with synchronous
       * mode disabled.
       *
       * @property sync
       * @chainable
       */
      async: {
        get: function () {
          this.synchronous = false;
          return this;
        }
      }
    });
  }

  niviz.synchronizable = synchronizable;
  niviz.sync = false;

}(niviz));
