/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var property   = Object.defineProperty;
  var properties = Object.defineProperties;

  var Value      = niviz.Value;
  var inherits   = niviz.util.inherits;
  var round      = niviz.util.round;
  var pick       = niviz.util.pick;

  /** @module niviz.Value */

  /**
   *
   * @class Ramm
   * @extends Value
   * @constructor
   * @param {Number} [top]
   * @param {Number} [value]
   * @param {Number} [bottom]
   */
  function Ramm(top, val, bottom, profile) {
    this.$value = null;
    this.simple = false;

    /**
     * @property value
     * @type {String}
     */
    var value;

    property(this, 'value', {
      enumerable: true,

      get: function () {
        return Ramm.compute(this);
      },

      set: function value$set(to) {
        value = this.parse(to);
      }
    });

    property(this, 'top', {
      enumerable: true,
      get: function () {
        var t = profile && profile.hs || 0;
        return t - (this.depth || 0);
      },
      set: function top$set(to) {
        var t = profile && profile.hs || 0;
        this.depth = (profile.top || 0) - to;
      }
    });

    property(this, 'bottom', {
      enumerable: true,
      get: function () {
        var t = profile && profile.hs || 0;
        return t - ((this.depth  + this.height) || 0);
      },
      set: function bottom$set(to) {
        var t = profile && profile.hs || 0;
        this.height = this.top - to;
      }
    });

    // if top, bottom and val are set, then create a val object that can be parsed
    if ((top === 0 || top) && (bottom === 0 || bottom) && (typeof val === 'number')) {
      var tmp = val, t = profile && profile.hs || 0;

      val = {
        depth: (profile.top || 0) - top,
        height: top - bottom,
        value: tmp
      };
    }

    this.value = val;

    //Value.call(this, top, val, bottom);
  }

  inherits(Ramm, Value);

  properties(Ramm.prototype, {
    /**
     * @property weightHammer
     * @type Number
     */

    /**
     * @property weightTube
     * @type Number
     */

    /**
     * @property nDrops
     * @type Number
     */

    /**
     * @property dropHeight
     * @type Number
     */
  });

  Ramm.compute = function (layer) {
    var value;

    if ((layer.weightHammer === 0 || layer.weightHammer)
        && (layer.weightTube === 0 || layer.weightTube)
        && (layer.nDrops === 0 || layer.nDrops)
        && (layer.dropHeight === 0 || layer.dropHeight)) {


      if (layer.height > 0) {
        value = (layer.weightHammer * layer.nDrops * layer.dropHeight / layer.height
                 + layer.weightTube + layer.weightHammer) * 9.81;
      } else if (layer.height === 0) {
        value = (layer.weightTube + layer.weightHammer) * 9.81;
      } else {
        value = null;
      }

      if (value) value = round(value, 4);
    } else {
      layer.simple = true;
      value = layer.$value || null;
    }

    this.$value = value;

    return value;
  };

  /**
   * Parse the object or number passed and derive other properties
   *
   * @method parse
   * @param {Object} [value]
   */
  Ramm.prototype.parse = function (value) {

    this.$value = null; // reset
    this.simple = false;

    if (value.depth === 0 || value.depth) this.depth = value.depth;
    else delete this.depth;

    if (value.height === 0 || value.height) this.height = value.height;
    else delete this.height;

    if (value.weightHammer === 0 || value.weightHammer) this.weightHammer = value.weightHammer;
    else delete this.weightHammer;

    if (value.weightTube === 0 || value.weightTube) this.weightTube = value.weightTube;
    else delete this.weightTube;

    if (value.nDrops === 0 || value.nDrops) this.nDrops = value.nDrops;
    else delete this.nDrops;

    if (value.dropHeight === 0 || value.dropHeight) this.dropHeight = value.dropHeight;
    else delete this.dropHeight;

    // For initialization when parsing from JSON
    if (this.$value === null && (value.$value === 0 || value.$value)) {
      this.$value = value.$value;
      delete value.$value;
    }

    if (value.value === 0 || value.value) this.$value = value.value;

  };

  Ramm.prototype.toJSON = function () {
    var value = pick(this, [ 'depth', 'height', 'weightHammer', '$value', 'simple',
                             'weightTube', 'nDrops', 'dropHeight']);
    return { value: value };
  };

  // --- Module Exports ---
  Value.Ramm = Ramm;

}(niviz));
