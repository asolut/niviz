/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var properties = Object.defineProperties;

  var Value         = niviz.Value;
  var inherits      = niviz.util.inherits;
  var isUndefOrNull = niviz.util.isUndefinedOrNull;

  /** @module niviz.Value */

  /**
   *
   * @class SK38
   * @constructor
   * @extends Value
   */
  function SK38(height, value) {
    if (!isUndefOrNull(value)) {
      if (!value.classnum)
        throw Error('SK38: Data in an unexpected format, you need to specify a'
                    + ' classnum (1, 3 or 5), got: ' + value.classnum);

      if (!value.sk38)
        throw Error('You need to specify a sk38 property');

    } else {
      value = {};
    }

    Value.call(this, height, value);
  }

  inherits(SK38, Value);

  properties(SK38.prototype, {
    /**
     * @property classnum
     * @type String
     */
    classnum: {
      get: function () { return this.value && this.value.classnum; }
    },

    /**
     * @property sk38
     * @type Number
     */
    sk38: {
      get: function () { return this.value && this.value.sk38; }
    }
  });

  SK38.prototype.toString = function () {
    if (!this.value) return undefined;

    var classnum = this.classnum;

    switch (true) {

    case (typeof classnum !== 'number'):
    case (isNaN(classnum)):
      return String(classnum);

    case (classnum === 1):
      return 'poor';
    case (classnum === 3):
      return 'fair';
    case (classnum === 5):
      return 'good';
    default:
      return String(classnum);
    }
  };

  // --- Module Exports ---
  Value.SK38 = SK38;

}(niviz));
