/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var property   = Object.defineProperty;

  var Value      = niviz.Value;
  var inherits   = niviz.util.inherits;
  var pick       = niviz.util.pick;

  /** @module niviz.Value */

  /**
   * Stores propagation saw test
   *
   * @class Saw
   * @extends Value
   *
   * @constructor
   * @param {Number} [height]
   * @param {Number} [value]
   */
  function Saw (height, val) {

    /**
     * @property value
     * @type {String}
     */
    var value;

    property(this, 'value', {
      enumerable: true,

      get: function () { return value; },

      set: function value$set(to) {
        value = this.parse(to);
      }
    });

    // Call Value constructor
    Value.call(this, height, val);
  }

  inherits(Saw, Value);

  Saw.propagations = [ 'End', 'SF', 'Arr' ];

  /**
   * Parse the object or number passed and derive other properties
   *
   * @method parse
   * @param {Object|Number} [value]
   * @return {Number}
   */
  Saw.prototype.parse = function (value) {
    if (!value.value || Saw.propagations.indexOf(value.value) === -1)
      throw new Error('Saw value must exist and must be [Arr | End | SF]');

    if (isNaN(value.cutlength) || isNaN(value.cutlength)
        || value.cutlength < 0 || value.columnlength < 0)
      throw new Error('PropSawTest: cutLength and columnLength must be numbers >0');

    this.cutlength    = value.cutlength;
    this.columnlength = value.columnlength;
    this.comment      = value.comment;

    return value.value;
  };

  Saw.prototype.toString = function () {
    return this.value;
  };

  Saw.prototype.toJSON = function () {
    var value = pick(this, [ 'cutlength', 'columnlength', 'comment', 'value']);

    return {
      top: this.top,
      value: value
    };
  };

  // --- Helpers ---

  // --- Module Exports ---
  Value.Saw = Saw;

}(niviz));
