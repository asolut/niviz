/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var property   = Object.defineProperty;
  var properties = Object.defineProperties;

  var Value      = niviz.Value;
  var inherits   = niviz.util.inherits;
  var pick       = niviz.util.pick;

  /** @module niviz.Value */

  /**
   *
   * @class SMP
   * @extends Value
   * @constructor
   * @param {Number} [top]
   * @param {Number} [value]
   * @param {Number} [bottom]
   */
  function SMP(top, val, bottom, profile) {
    bottom = undefined;

    if (top || top === 0)
      val = { depth: top, value: val };

    /**
     * @property value
     * @type {String}
     */
    var value;

    property(this, 'value', {
      enumerable: true,

      get: function () {
        return value;
      },

      set: function value$set(to) {
        value = this.parse(to);
      }
    });

    property(this, 'top', {
      enumerable: true,
      get: function () {
        var t = profile && profile.hs || 0;
        return t - (this.depth || 0);
      },
      set: function top$set(to) {}
    });

    property(this, 'bottom', {
      enumerable: true,
      get: function () {
        return this.top;
      },
      set: function bottom$set(to) {}
    });

    this.value = val;

    //Value.call(this, top, val, bottom);
  }

  inherits(SMP, Value);

  properties(SMP.prototype, {
  });

  /**
   * Parse the object and derive other properties
   *
   * @method parse
   * @param {Object} [value]
   */
  SMP.prototype.parse = function (value) {
    if (value.depth !== undefined) this.depth = value.depth;
    else delete this.depth;

    return value.value;
  };

  SMP.prototype.toJSON = function () {
    var value = pick(this, [ 'depth', 'value' ]);
    return { value: value };
  };

  // --- Module Exports ---
  Value.SMP = SMP;

}(niviz));
