/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var properties = Object.defineProperties;

  var Value      = niviz.Value;
  var inherits   = niviz.util.inherits;

  /** @module niviz.Value */

  /**
   *
   * @class GrainSize
   * @extends Value
   * @constructor
   * @param {Number} [top]
   * @param {Number} [value]
   * @param {Number} [bottom]
   */
  function GrainSize(top, value, bottom) {
    if (typeof value === 'number')
      value = { avg: value, max: value };

    Value.call(this, top, value, bottom);
  }

  inherits(GrainSize, Value);

  properties(GrainSize.prototype, {
    /**
     * @property avg
     * @type Number
     */
    avg: {
      get: function () { return this.value && this.value.avg; }
    },

    /**
     * @property max
     * @type Number
     */
    max: {
      get: function () { return this.value && this.value.max; }
    },

    /**
     * @property numeric
     * @type Number
     */
    numeric: {
      get: function () {
        if (!this.value)
          return undefined;

        // Prefer max for comparisons!
        if (this.value.hasOwnProperty('max'))
          return this.value.max;

        return this.value.avg;
      }
    }
  });

  GrainSize.prototype.toString = function () {
    if (!this.value) return undefined;

    var avg = this.avg;

    switch (true) {

    case (typeof avg !== 'number'):
    case (isNaN(avg)):
      return String(avg);

    case (avg < 0.2):
      return 'very fine';
    case (avg < 0.5):
      return 'fine';
    case (avg < 1.0):
      return 'medium';
    case (avg < 2.0):
      return 'coarse';
    case (avg < 5.0):
      return 'very coarse';
    default:
      return 'extreme';
    }
  };

  // --- Module Exports ---
  Value.GrainSize = GrainSize;

}(niviz));
