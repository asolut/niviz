/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var properties = Object.defineProperties;
  var Value      = niviz.Value;
  var inherits   = niviz.util.inherits;
  var Common   = niviz.Common;

  /** @module niviz.Value */

  /**
   *
   * @class GrainShape
   * @constructor
   * @extends Value
   */
  function GrainShape(top, value, bottom) {
    if (value != null) {
      if (typeof value === 'number')
        value = GrainShape.convert(value);

      // Commented because there may be layers without grainshape specified
      // if (!value.primary)
      //   throw Error('You need to specify a primary grainshape');

    } else {
      value = {};
    }

    Value.call(this, top, value, bottom);
  }

  inherits(GrainShape, Value);

  properties(GrainShape.prototype, {

    definition: {
      get: function () {
        return this.value && GrainShape.type[this.value.primary];
      }
    },

    /**
     * @property code
     * @type String
     */
    code: {
      get: function () {
        var p = GrainShape.type[this.value.primary];

        if (!p) return undefined;

        var s = GrainShape.type[this.value.secondary];

        if (this.value.primary === 'MFcr' && !s)
          return p.key + 'h';

        if (!s) return p.key;

        if (this.value.primary === 'MFcr')
          return p.key + s.key;

        if (this.value.primary === this.value.secondary)
          return p.key;

        return p.key + '(' + s.key + ')';
      }
    },

    /**
     * @property color
     * @type String
     */
    color: {
      get: function () {
        if (Common.defaults.values.grain_shape_color_palette === '_gscp_perception_informed') {
          //GrainShape.enableCanadianColors) {
          return GrainShape.canadianColors[this.value.primary];
        }
        var d = GrainShape.type[this.value.primary];
        return (d) ? d.color : undefined;
      }
    },

    // GrainShape does not have a useful numeric value!
    numeric: { value: 0 }
  });

  /**
   * @property type
   * @type Object
   */
  GrainShape.type = {
    'PP':   { name: 'Precipitation Particles', color: '#00FF00', key: 'a' },
    'MM':   { name: 'Machine Made snow', color: '#FFD700', key: 'b' },
    'DF':   {
              name: 'Decomposing and Fragmented precipitation particles',
              color: '#228B22',
              key: 'c'
            },
    'RG':   { name: 'Rounded Grains', color: '#FFB6C1', key: 'd' },
    'FC':   { name: 'Faceted Crystals', color: '#ADD8E6', key: 'e' },
    'DH':   { name: 'Depth Hoar', color: '#0000FF', key: 'f' },
    'SH':   { name: 'Surface Hoar', color: '#FF00FF', key: 'g' },
    'MF':   { name: 'Melt Forms', color: '#FF0000', key: 'h' },
    'IF':   { name: 'Ice Formations', color: '#00FFFF', key: 'i' },
    'PPco': { name: 'Columns', color: '#00FF00', key: 'j' },
    'PPnd': { name: 'Needles', color: '#00FF00', key: 'k' },
    'PPpl': { name: 'Plates', color: '#00FF00', key: 'l' },
    'PPsd': { name: 'Stellars, Dendrites', color: '#00FF00', key: 'm' },
    'PPir': { name: 'Irregular crystals', color: '#00FF00', key: 'n' },
    'PPgp': { name: 'Graupel', color: '#808080', key: 'o' },
    'PPhl': { name: 'Hail', color: '#00FF00', key: 'p' },
    'PPip': { name: 'Ice pellets', color: '#00FF00', key: 'q' },
    'PPrm': { name: 'Rime', color: '#00FF00', key: 'r' },
    'MMrp': { name: 'Round polycrystalline particles', color: '#FFD700', key: 's' },
    'MMci': { name: 'Crushed ice particles', color: '#FFD700', key: 't' },
    'DFdc': {
              name: 'Partly decomposed precipitation particles',
              color: '#228B22',
              key: 'u'
            },
    'DFbk': {
              name: 'Wind-broken precipitation particles',
              color: '#228B22',
              key: 'v'
            },
    'RGsr': { name: 'Small rounded particles', color: '#FFB6C1', key: 'w' },
    'RGlr': { name: 'Large rounded particles', color: '#FFB6C1', key: 'x' },
    'RGwp': { name: 'Wind packed', color: '#FFB6C1', key: 'y' },
    'RGxf': { name: 'Faceted rounded particles', color: '#FFB6C1', key: 'z' },
    'FCso': { name: 'Solid faceted particles', color: '#ADD8E6', key: 'A' },
    'FCsf': { name: 'Near surface faceted particles', color: '#ADD8E6', key: 'B' },
    'FCxr': { name: 'Rounding faceted particles', color: '#ADD8E6', key: 'C' },
    'DHcp': { name: 'Hollow cups', color: '#0000FF', key: 'D' },
    'DHpr': { name: 'Hollow prisms', color: '#0000FF', key: 'E' },
    'DHch': { name: 'Chains of depth hoar', color: '#0000FF', key: 'F' },
    'DHla': { name: 'Large striated crystals', color: '#0000FF', key: 'G' },
    'DHxr': { name: 'Rounding depth hoar', color: ' #0000FF', key: 'H' },
    'SHsu': { name: 'Surface hoar crystals', color: '#FF00FF', key: 'I' },
    'SHcv': { name: 'Cavity or crevasse hoar', color: '#FF00FF', key: 'J' },
    'SHxr': { name: 'Rounding surface hoar', color: '#FF00FF', key: 'K' },
    'MFcl': { name: 'Clustered rounded grains', color: '#FF0000', key: 'L' },
    'MFpc': { name: 'Rounded polycrystals', color: '#FF0000', key: 'M' },
    'MFsl': { name: 'Slush', color: '#FF0000', key: 'N' },
    'MFcr': { name: 'Melt-freeze crust', color: '#FF0000', key: 'O' },
    'IFil': { name: 'Ice layer', color: '#00FFFF', key: 'P' },
    'IFic': { name: 'Ice column', color: '#00FFFF', key: 'Q' },
    'IFbi': { name: 'Basal ice', color: '#00FFFF', key: 'R' },
    'IFrc': { name: 'Rain crust', color: '#00FFFF', key: 'S' },
    'IFsc': { name: 'Sun crust, Firnspiegel', color: '#00FFFF', key: 'T' }
  };

  /**
   * Color definition alternative to GrainShape.type
   *
   * Horton, S., Nowak, S., and Haegeli, P.:
   * Enhancing the operational value of snowpack models
   * with visualization design principles,
   * Nat. Hazards Earth Syst. Sci., 20, 1557–1572,
   * https://doi.org/10.5194/nhess-20-1557-2020 , 2020.
   *
   * @property canadianColors
   * @type Object
   */
  GrainShape.canadianColors = {
    'SH': '#ee3a1d',
    'DH': '#4678e8',
    'PP': '#ffde00',
    'DF': '#f1f501',
    'RG': '#ffccd9',
    'FCxr': '#dacef4',
    'FC': '#b2edff',
    'MFcr': '#addd8e',
    'MF': '#d5ebb5',
    'IF': '#a3ddbb',
    'MM': '#999999'
  };

  // Let's fill in the missing colors in GrainShape.canadianColors
  Object.keys(GrainShape.type).forEach(function (key) {
    if (!GrainShape.canadianColors[key]) {
      // Canadian color not defined for grain shape key `key`...
      Object.keys(GrainShape.canadianColors).forEach(function (cck) {
        if(key.startsWith(cck)) {
          // Use canadian color with key as prefix of the missing key `key`.
          GrainShape.canadianColors[key] = GrainShape.canadianColors[cck];
        }
      });
    }
  });

  GrainShape.enableCanadianColors = (Common.defaults.values.grain_shape_color_palette === '_gscp_perception_informed');

  /**
   * The index of each type corresponds to
   * the type's swiss code number.
   *
   * @property swisscode
   * @type Array.<String>
   * @static
   */
  GrainShape.swisscode = [
    'PPgp', 'PP', 'DF', 'RG', 'FC', 'DH', 'SH', 'MF', 'IF', 'FCxr'
  ];

  /**
   * An array of all grainforms and their most imporant properties
   *
   * @property options
   * @type Array.<Object>
   * @static
   */
  GrainShape.options = Object.keys(GrainShape.type).map(function (key) {
    var o = {
      id: key,
      name: GrainShape.type[key].name,
      key: GrainShape.type[key].key
    };

    return o;
  });

  /**
   * Convert Swiss Code F1F2F3 to GrainShape value.
   *
   * @method convert
   * @static
   *
   * @param {Number} code
   * @return {Object}
   */
  GrainShape.convert = function (code) {
    var values = String(code).split('').map(Number);

    return {
      primary:  (values[2] === 2) ? 'MFcr' : this.swisscode[values[0]],
      secondary: (values[2] === 2 && values[1] === 7) ? null : this.swisscode[values[1]]
    };
  };

  GrainShape.prototype.toString = function () {
    return this.code;
  };

  // --- Module Exports ---
  Value.GrainShape = GrainShape;

}(niviz));
