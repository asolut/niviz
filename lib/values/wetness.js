/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var properties    = Object.defineProperties;
  var property      = Object.defineProperty;
  var Value         = niviz.Value;
  var inherits      = niviz.util.inherits;
  var isUndefOrNull = niviz.util.isUndefinedOrNull;

  /** @module niviz.Value */

  /**
   * Stores wetness as volume fraction in %.
   *
   * @class Wetness
   * @extends Value
   *
   * @constructor
   * @param {Number} [top]
   * @param {Number} [value]
   * @param {Number} [bottom]
   */
  function Wetness(top, val, bottom) {

    if (typeof val === 'string') val = Wetness.parse(val);

    /**
     * The wetness as volume fraction in %.
     *
     * @property value
     * @type {Number}
     */
    var value;

    property(this, 'value', {
      enumerable: true,

      get: function () { return value; },

      set: function value$set(to) {
        value = this.$index = undefined;

        if (typeof to === 'string') to = Wetness.parse(to);

        if (!isUndefOrNull(to)) {
          var v = parseFloat(to);

          if (isNaN(v))
            throw new Error('Wetness must be a number, was: ' + to);

          value = v;
        }
      }
    });

    // Prepare index cache
    property(this, '$index', { writable: true, enumerable: false });

    // Call Value constructor
    Value.call(this, top, val, bottom);
  }

  inherits(Wetness, Value);

  properties(Wetness, {
    /**
     * Wetness code to numeric index mapping.
     *
     * @property index
     * @type {Object}
     */
    index: {
      value: { 'D': 1, 'D-M': 1.5, 'M': 2, 'M-W': 2.5, 'W': 3, 'W-V': 3.5, 'V': 4, 'V-S': 4.5, 'S': 5 }
    },

    /**
     * Wetness code to mean value mapping.
     *
     * @property mean
     * @type {Object}
     */
    mean: {
      value: { 'D': 0, 'D-M': 0.5, 'M': 1.5, 'M-W': 3, 'W': 5.5, 'W-V': 8, 'V': 11.5, 'V-S': 15, 'S': 19.5 }
    },

    /**
     * @property codes
     * @type {Array.<String>}
     */
    codes: {
      value: [ 'D', 'D-M', 'M', 'M-W', 'W', 'W-V', 'V', 'V-S', 'S' ]
    },

    /**
     * @property caamlcodes
     * @type {Array.<String>}
     */
    caamlcodes: {
      value: [ 'D', 'D-M', 'M', 'M-W', 'W', 'W-V', 'V', 'V-S', 'S' ]
    }
  });

  /**
   * Converts a string 'value' to an index. It is interpreted
   * as a wetness code;
   *
   * @method parse
   * @static
   *
   * @param {String} value
   *
   * @return {Number}
   */
  Wetness.parse = function (value) {
    if (!value) return null;

    if (Wetness.codes.indexOf(value) > -1) {
      return Wetness.mean[value];
    }

    throw new Error('Cannot convert string to wetness index: ' + value);
  };

  properties(Wetness.prototype, {
    /**
     * Wetness code string.
     *
     * @property code
     * @type {String}
     */
    code: {
      get: function code$get() {
        if (this.value === undefined) return undefined;
        return Wetness.codes[Math.round((this.index - 1) * 2)];
      },

      set: function code$set(value) {
        this.value = (!isUndefOrNull(value)) ? Wetness.parse(String(value)) : undefined;
      }
    },

    /**
     * Wetness index from 1 to 9
     *
     * @property index
     * @type {Number}
     */
    index: {
      get: function index$get() {
        if (this.value === undefined) return undefined;

        if (isUndefOrNull(this.$index)) {
          if (this.value > 15)        this.$index = 5;
          else if (this.value > 13)   this.$index = 4.5;
          else if (this.value >= 11.5) this.$index = 4;
          else if (this.value > 7.5)    this.$index = 3.5;
          else if (this.value >= 5.5)  this.$index = 3;
          else if (this.value > 2.5)    this.$index = 2.5;
          else if (this.value >= 1.5)  this.$index = 2;
          else if (this.value >= 0.49)  this.$index = 1.5;
          else if (this.value < 0.49)   this.$index = 1;
        }

        return this.$index;
      },

      set: function index$set(value) {
        this.value = undefined;

        if ((value > 0 && value < 10) && value === parseInt(value))
          this.value = value;
      }
    }
  });

  // --- Helpers ---

  // --- Module Exports ---
  Value.Wetness = Wetness;

}(niviz));
