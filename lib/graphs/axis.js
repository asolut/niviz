/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var properties = Object.defineProperties;
  var log10      = niviz.util.log10;

  /** @module niviz */

  /**
   * The Axis class is a representation of a single axis as it is typically used
   * in diagrams and charts. An Axis object has a minimum and a maximum value
   * and associated minimum and maximum pixel values. Given a coordinate, that
   * should lie within minimum and maximum, the Axis object can return the
   * corresponding pixel value and vice versa. Furthermore the Axis class can be
   * configured to enable a logarithmic scale and can invert the scale.
   *
   * @class Axis
   * @constructor
   * @param {Number} min The minimal coordinate value for this  axis
   * @param {Number} max The maximal coordinate value for this axis
   * @param {Number} pxmin The pixel value associated to the minimum
   * @param {Number} pxmax The pixel value associated to the maximum
   * @param {Boolean} [log] true = logarthmic scale, false = not logarithmic
   */
  function Axis (min, max, pxmin, pxmax, log) {
    if (isNaN(min) || isNaN(max) || isNaN(pxmin) || isNaN(pxmax)
        || (log !== undefined && typeof log !== 'boolean'))
      throw new TypeError('Axis constructor requires four numeric values');

    this.min = min;
    this.max = max;

    this.pxmin = pxmin;
    this.pxmax = pxmax;

    this.$log = log || false;
  }

  properties(Axis.prototype, {
    /**
     * Enable/disable logarithmic scaling
     *
     * @property log
     * @type Boolean
     */
    log: {
      get: function () {
        return this.$log;
      },

      set: function log$set(enable) {
        this.$log = enable;
      }
    }
  });

  /**
   * Invert the axis (swap minimum for maximum)
   * @method invert
   */
  Axis.prototype.invert = function () {
    this.pxmax = [this.pxmin, this.pxmin = this.pxmax][0]; //swap
  };

  /**
   * Redefine minimal and maximal coordinate of the axis
   *
   * @method range
   * @param {Number} min Minimal coordinate
   * @param {Number} max Maximal coordinate
   */
  Axis.prototype.range = function (min, max) {
    this.min = min;
    this.max = max;
  };

  /**
   * Redefine pixel range for current minimal and maximal coordinates
   *
   * @method pxrange
   * @param {Number} min Pixel value for minimal coordinate
   * @param {Number} max Pixel value for maximal coordinate
   */
  Axis.prototype.pxrange = function (min, max) {
    this.pxmin = min;
    this.pxmax = max;
  };

  /**
   * Get pixel value for a given coord (not rounded)
   *
   * @method pixel
   * @param {Number} coord Coordinate (should be between minimum and maximum)
   * @return {Number}
   */
  Axis.prototype.pixel = function (coord) {
    // TODO: test validity of coord
    var pxmin = this.pxmin, pxmax = this.pxmax, min = this.min, max = this.max;
    if (this.$log) {
      max = log10(max);
      min = log10(min);
      coord = log10(coord);
    }

    if (pxmin >= pxmax) {
      return pxmin - (pxmin - pxmax) * (1 - (max - coord) / (max - min));
    } else {
      return pxmin + (pxmax - pxmin) * (1 - (max - coord) / (max - min));
    }
  };

  /**
   * Get coordinate value for a given pixel (not rounded)
   *
   * @method coord
   * @param {Number} pixel Pixel (should be between pixel minimum and pixel maximum)
   * @return {Number}
   */
  Axis.prototype.coord = function (pixel) {
    var pxmin = this.pxmin, pxmax = this.pxmax, min = this.min, max = this.max, coord;

    if (this.$log) {
      max = log10(max);
      min = log10(min);
    }

    if (pxmin >= pxmax) {
      coord = min - (min - max) * (1 - (pxmax - pixel) / (pxmax - pxmin));
    } else {
      coord = min + (max - min) * (1 - (pxmax - pixel) / (pxmax - pxmin));
    }

    if (this.$log) return Math.pow(10, coord);
    return coord;
  };

  // --- Module Exports ---
  niviz.Axis = Axis;

}(niviz));
