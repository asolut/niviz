/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

(function (niviz) {
  'use strict';

  // --- Module Dependencies ---
  var Axis = niviz.Axis;

  /** @module niviz */

  /**
   * The Cartesian class is a means to bundle Axis objects and provides
   * neat accessors to calculate pixel/coord of two Axis objects at a time,
   * a x-axis and a y-axis.
   *
   * @class Cartesian
   * @constructor
   */
  function Cartesian () {
    this.axisx = {};
    this.axisy = {};
  }

  /**
   * Invert the axis (swap minimum for maximum)
   *
   * @method invert
   * @param {String} name Name of axis to invert
   */
  Cartesian.prototype.invert = function (name) {
    if (this.axisx[name]) this.axisx[name].invert();
    if (this.axisy[name]) this.axisy[name].invert();
  };

  /**
   * Add an x-axis to the cartesian instance.
   *
   * @method addx
   * @param {String} name Name of axis to add
   * @param {Number} min Minimal coordinate
   * @param {Number} max Maximal coordinate
   * @param {Number} pxmin Pixel value for minimal coordinate
   * @param {Number} pxmax Pixel value for maximal coordinate
   */
  Cartesian.prototype.addx = function (name, min, max, pxmin, pxmax) {
    var axis = this.axisx[name];

    if (axis) {
      axis.range(min, max);
      axis.pxrange(pxmin, pxmax);
    } else {
      this.axisx[name] = new Axis(min, max, pxmin, pxmax);
    }
  };

  /**
   * Add an y-axis to the cartesian instance.
   *
   * @method addy
   * @param {String} name Name of axis to add
   * @param {Number} min Minimal coordinate
   * @param {Number} max Maximal coordinate
   * @param {Number} pxmin Pixel value for minimal coordinate
   * @param {Number} pxmax Pixel value for maximal coordinate
   */
  Cartesian.prototype.addy = function (name, min, max, pxmin, pxmax) {
    var axis = this.axisy[name];

    if (axis) {
      axis.range(min, max);
      axis.pxrange(pxmin, pxmax);
    } else {
      this.axisy[name] = new Axis(min, max, pxmin, pxmax);
    }
  };

  /**
   * Get pixel value for a given coord (not rounded) on x-axis.
   *
   * @method px
   * @param {String} namex The name of the x-axis
   * @param {Number} x Coordinate (should be between minimum and maximum)
   * @return {Number}
   */
  Cartesian.prototype.px = function (namex, x) {
    var axisx = this.axisx[namex];
    return axisx.pixel(x);
  };

  /**
   * Get pixel value for a given coord (not rounded) on y-axis.
   *
   * @method py
   * @param {String} namey The name of the y-axis
   * @param {Number} y Coordinate (should be between minimum and maximum)
   * @return {Number}
   */
  Cartesian.prototype.py = function (namey, y) {
    var axisy = this.axisy[namey];
    return axisy.pixel(y);
  };

  /**
   * Get coordinate value for a given pixel (not rounded) on the x-axis.
   *
   * @method coordx
   * @param {String} namex The name of the x-axis
   * @param {Number} x Pixel (should be between pixel minimum and pixel maximum)
   * @return {Number}
   */
  Cartesian.prototype.coordx = function (namex, x) {
    var axisx = this.axisx[namex];
    return axisx.coord(x);
  };

  /**
   * Get coordinate value for a given pixel (not rounded) on the y-axis.
   *
   * @method coordy
   * @param {String} namey The name of the y-axis
   * @param {Number} y Pixel (should be between pixel minimum and pixel maximum)
   * @return {Number}
   */
  Cartesian.prototype.coordy = function (namey, y) {
    var axisy = this.axisy[namey];
    return axisy.pixel(y);
  };

  /**
   * Get pixel value for a given (x, y) coord (not rounded).
   *
   * @method pixel
   * @param {String} namex The name of the x-axis
   * @param {String} namey The name of the y-axis
   * @param {Number} x Coordinate (should be between minimum and maximum)
   * @param {Number} y Coordinate (should be between minimum and maximum)
   * @return {Object} Object with x and y property
   */
  Cartesian.prototype.pixel = function (namex, namey, x, y) {
    var axisx = this.axisx[namex], axisy = this.axisy[namey];

    return {
      x: axisx.pixel(x),
      y: axisy.pixel(y)
    };
  };

  /**
   * Get coord for a given (px, py) pixel value (not rounded).
   *
   * @method coord
   * @param {String} namex The name of the x-axis
   * @param {String} namey The name of the y-axis
   * @param {Number} px Pixel (should be between pixel minimum and pixel maximum)
   * @param {Number} py Pixel (should be between pixel minimum and pixel maximum)
   * @return {Object} Object with x and y property
   */
  Cartesian.prototype.coord = function (namex, namey, px, py) {
    var axisx = this.axisx[namex], axisy = this.axisy[namey];

    return {
      x: axisx.coord(px),
      y: axisy.coord(py)
    };
  };

  // --- Module Exports ---
  niviz.Cartesian = Cartesian;

}(niviz));
