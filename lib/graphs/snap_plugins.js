/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

(function (niviz) {
  'use strict';

  Snap.plugin(function (Snap, Element, Paper, glob) {

    /**
     * Print out multilined text, each line is nested in a
     * separate <tspan>
     *
     * @method multitext
     * @public
     * @param {Number} x x-coordinate position
     * @param {Number} y y-coordinate position
     * @param {String} text Text string with newlines denoted as '\n'
     * @return {Object} Multilined text element
     */
    Paper.prototype.multitext = function (x, y, txt) {
      txt = txt.split('\n');

      // Preserve lines with linebreak by turning them into a single space
      txt.forEach(function (line, index) {
        if (!line.length) txt[index] = ' ';
      });

      var t = this.text(x, y, txt);
      t.selectAll('tspan:nth-child(n+2)').attr({
        dy: '1.2em',
        x: x
      });

      return t;
    };
  });

  Snap.plugin(function (Snap, Element, Paper, glob) {

    /**
     * Draw the hatched pattern
     *
     * @method hatching
     * @public
     * @param {String} color RGB color
     * @param {Number} [angle] patternTransform angle
     * @return {Object} The pattern to be used for some fill property
     */
    Paper.prototype.hatching = function (color, angle, bgcolor) {

      var upp = 'M10-5-10,15M15,0,0,15M0-5-20,15', g = this.g();

      g.add(this.rect(0, 0, 10, 10).attr({
        fill: bgcolor || 'none'
      }));

      g.add(this.path(upp).attr({
        stroke: color,
        strokeWidth: 1.5
      }));

      g = g.toPattern(0, 0, 10, 10).attr({
        patternTransform: 'rotate(' + angle + ')'
      });

      return g;
    };

    /**
     * Draw a vertically hatched pattern
     *
     * @method hatching
     * @public
     * @param {String} color RGB color
     * @param {String} bgcolor RGB background color
     * @return {Object} The pattern to be used for some fill property
     */
    Paper.prototype.verticalHatching = function (color, bgcolor) {

      var upp = 'M 0 5 L 0 0', g = this.g();

      g.add(this.rect(0, 0, 5, 5).attr({
        fill: bgcolor || 'none'
      }));

      g.add(this.path(upp).attr({
        stroke: color,
        strokeWidth: 3.5
      }));

      g = g.toPattern(0, 0, 5, 5);

      return g;
    };
  });

  Snap.plugin(function (Snap, Element, Paper, glob) {

    var tokenRegex = /\{([^\}]+)\}/g,
      // matches .xxxxx or ["xxxxx"] to run over object properties:
      objNotationRegex = /(?:(?:^|\.)(.+?)(?=\[|\.|$|\()|\[('|")(.+?)\2\])(\(\))?/g;

    var replacer = function (all, key, obj) {
      var res = obj;
      key.replace(objNotationRegex, function (all, name, quote, quotedName, isFunc) {
        name = name || quotedName;
        if (res) {
          if (name in res) {
            res = res[name];
          }
          typeof res == "function" && isFunc && (res = res());
        }
      });
      res = (res == null || res == obj ? all : res) + '';
      return res;
    };

    var fill = function (str, obj) {
      return String(str).replace(tokenRegex, function (all, key) {
        return replacer(all, key, obj);
      });
    };

    /**
     * Draw a niViz popup
     *
     * @method popup
     * @public
     * @param {Number} X x-coordinate position
     * @param {Number} Y y-coordinate position
     * @param {Object} set A group contained by the popup
     * @param {String} pos Possible values: top, bottom, left, right
     * @param {Number} stretch Margin in pixels around the content
     * @return {Object} The popup
     */
    Paper.prototype.popup = function (X, Y, set, pos, stretch) {
      var pos = String(pos || 'top-middle').split('-');
      pos[1] = pos[1] || 'middle';

      var r = 5, stretch = stretch || 0, bb = set.getBBox(),
        w = Math.round(bb.width) + stretch,
        h = Math.round(bb.height),
        x = Math.round(bb.x) - r - stretch / 2,
        y = Math.round(bb.y) - r,
        gap = Math.min(h / 2, w / 2, 10);

      var shapes = {
        top: 'M{x},{y}h{w4},{w4},{w4},{w4}a{r},{r},0,0,1,{r},{r}v{h4},{h4},{h4},{h4}a{r},{r},0,0,1,-{r},{r}l-{right},0-{gap},{gap}-{gap}-{gap}-{left},0a{r},{r},0,0,1-{r}-{r}v-{h4}-{h4}-{h4}-{h4}a{r},{r},0,0,1,{r}-{r}z',
        bottom: 'M{x},{y}l{left},0,{gap}-{gap},{gap},{gap},{right},0a{r},{r},0,0,1,{r},{r}v{h4},{h4},{h4},{h4}a{r},{r},0,0,1,-{r},{r}h-{w4}-{w4}-{w4}-{w4}a{r},{r},0,0,1-{r}-{r}v-{h4}-{h4}-{h4}-{h4}a{r},{r},0,0,1,{r}-{r}z',
        right: 'M{x},{y}h{w4},{w4},{w4},{w4}a{r},{r},0,0,1,{r},{r}v{h4},{h4},{h4},{h4}a{r},{r},0,0,1,-{r},{r}h-{w4}-{w4}-{w4}-{w4}a{r},{r},0,0,1-{r}-{r}l0-{bottom}-{gap}-{gap},{gap}-{gap},0-{top}a{r},{r},0,0,1,{r}-{r}z',
        left: 'M{x},{y}h{w4},{w4},{w4},{w4}a{r},{r},0,0,1,{r},{r}l0,{top},{gap},{gap}-{gap},{gap},0,{bottom}a{r},{r},0,0,1,-{r},{r}h-{w4}-{w4}-{w4}-{w4}a{r},{r},0,0,1-{r}-{r}v-{h4}-{h4}-{h4}-{h4}a{r},{r},0,0,1,{r}-{r}z'
        },
        offset = {
          hx0: X - (x + r + w - gap * 2),
          hx1: X - (x + r + w / 2 - gap),
          hx2: X - (x + r + gap),
          vhy: Y - (y + r + h + r + gap),
          '^hy': Y - (y - gap)
        },
        mask = [{
          x: x + r,
          y: y,
          w: w,
          w4: w / 4,
          h4: h / 4,
          right: 0,
          left: w - gap * 2,
          bottom: 0,
          top: h - gap * 2,
          r: r,
          h: h,
          gap: gap
        }, {
          x: x + r,
          y: y,
          w: w,
          w4: w / 4,
          h4: h / 4,
          left: w / 2 - gap,
          right: w / 2 - gap,
          top: h / 2 - gap,
          bottom: h / 2 - gap,
          r: r,
          h: h,
          gap: gap
        }, {
          x: x + r,
          y: y,
          w: w,
          w4: w / 4,
          h4: h / 4,
          left: 0,
          right: w - gap * 2,
          top: 0,
          bottom: h - gap * 2,
          r: r,
          h: h,
          gap: gap
        }][pos[1] == 'middle' ? 1 : (pos[1] == 'top' || pos[1] == 'left') * 2];

      var dx = 0, dy = 0;

      switch (pos[0]) {
      case 'top':
        dx = X - (x + r + mask.left + gap);
        dy = Y - (y + r + h + r + gap);
        break;
      case 'bottom':
        dx = X - (x + r + mask.left + gap);
        dy = Y - (y - gap);
        break;
      case 'left':
        dx = X - (x + r + w + r + gap);
        dy = Y - (y + r + mask.top + gap);
        break;
      case 'right':
        dx = X - (x - gap);
        dy = Y - (y + r + mask.top + gap);
        break;
      }

      var out = this.path(fill(shapes[pos[0]], mask)).insertBefore(set);
      out.transform('t' + dx + ',' + dy);
      set.transform('t' + dx + ',' + dy);

      return out;
    };
  });

  niviz.SVGFonts = '<defs><style type="text/css"><![CDATA[@font-face { font-family: "snowsymbolsiacs"; src: url(data:application/vnd.ms-fontobject;charset=utf-8;base64,Yx0AAHccAAACAAIABAAAAAUFBgACAgICAgQBAJABAAAEAExQAQAAAAAAABAAAAAAAAAAAAEAAAAAAAAAj76YWQAAAAAAAAAAAAAAAAAAAAAAAB4AUwBuAG8AdwBTAHkAbQBiAG8AbABzAEkAQQBDAFMAAAAOAFIAZQBnAHUAbABhAHIAAAAaAFYAZQByAHMAaQBvAG4AIAAxAC4AMAAzADAAAAAuAFMAbgBvAHcAUwB5AG0AYgBvAGwAcwBJAEEAQwBTACAAUgBlAGcAdQBsAGEAcgAAAAAAQlNHUAAAAAAAAAAAAAAAAAAAAAADAEJYABRCABRIABMsEs3pish4WlfJaFtxSn94ImO6cixnLEX3JvcFQ4LcGsS2ivb0/JQIoxtZRgG1mKw0ynVBooDarMSmRHBFx0xGTbwmXGhuqw0M+nPFzpXP5EkWSSYnlUNmUMECxmZtcIee7qCWytxwohvoNFlo1uUxy1JDYNuxogbI9STGPN5yWrTSpVQYo8oT57Sq0VBhaPBvEBgRN/NaTuuuGgHPtEs63pVzu1hSoTGviks0KlCcRcQpqK3ZbmyAtcOzx0QrMTPW0C2vqpsTKEx34G2xqy7VNCNt9fkHEEg5At2fgYijFf9gZuX4Pkn2nPhRebA5dyiD5hjDDSl3HyYlEBFupfYgSCgcsTSvNFe/23RA+htFH81wJDVQTpIdAB+zlPpOJQEL6fP+knZ3Y7NqMm5oir8+3Y+m2c7xU0wApCQuJu1fozDDcicbK7p8S8G+VkwnDxZxEWdFRC0j4uCY5F4EvQBbBOJ+FlbF/pXPPAhnKsNh3i3lLkRjp11Nl3xRC0OC+JS2brQ0g42bFEWtuxCJUcqv2L3ldieKYhMNu1FiCFaLYWdsBAQVQEZE5Y8BSz7J2xMQlMJHCV0lcJXxgcfLoRlsfnnIvYyAgNDW+gxx6gMKL1VEneFGxupXKkQ9CF3OtgFqSRnrb9fkZ5PdYuaBnm5wP8+L1cubXHeOM6eB1IoYBjoELECakf+9vTFMQVgEhif7KYG4IpF4U6pQ0QXv34UL9dyK6QkPKwkWxQCYuwux4RDLmuMXpHwbjucD6bwgsMwthvIIxKil7qp2XhFicBgfpchoAtUf3j0qGQqLQv+MtQhlYTgTE+IuRSaD5WxKndFMq3zK2zZp0BYPvb676BZMLbqs7qlMNVTVRYs1cFhsKrS8cRvXw3a/3hEbMG+v/0AV4xyNVmzBE7xyKn0usroZfh4FKv+K927T6wBwoWDjcMJvw20DRsHfF4Ivi43UItUC29Gcv2ovHBtKeipvT2ReLfP4qlXUKvzDip5PYAeYDpmPE7M5U0raRYpWgQwhnTZmZ7Teb2DgX9EecSXABXcAX0msC3FpEB4+KWKhfgKXndqQRsQ9ww9nhtS/HZsaO7pmG1JXO/CnmAS8LGQZ04RHTxwx8va0/y81QXTgU4ZJkNFZmWMlJIDKk9DRksvRNHcXwlBRJmmd5QT2xDYgIf5SkGtirRHQRPmqw2UuWQxHjDUmGQZkcs4xn79SWkcKLT3hbCCqzWnKhQUs62O2rV4iS3o46L/UB62wCwJ8IWCpnkRhLdaL0b1lCWKbZQZZqaMVpEmUFi00foXWfOK6KigiGECabhrQBW36AozHiwtmPGIhp2ctcdQV2FiAo0kRHKgQAECVkLCQg8bhYQq+q7fJDhu3m4cD1pWCJHUwVDdPh/AY4zYxAjsVbaZeKo+K60Taw/SUSFlT6Vej1TZCvfZp3asNwqd0ZczhUaMF80/Ks7aZDBs9osx7sIoUx5R8XGwSMhXq6B4hmbp2g6NdCRdSEZqdRNVb9AVBOolfdt6KQB6Gj+gF2ysaNLOOxlp/2ezAMDGyDyWUA2KR9u6RWQnQuSG0ILsuX/3w+j85BdKtIKfnTHLoraP8nEaRYVGScuQW9pHg3FU2FhXoJm5s78dLDATh1o5mislVMikeSFdKNdavvi5yW/a6JbAPrLY7iICTNXLh72n6uwX7mWhs84ePg1fqi2Qpn+/gMzQbv0dy4CiSuEkSImiTwu6l28G2b2CYjkdPvszhcMR9uFj8Uui1afVyX0a15niCn8gXSMUNQY8OdE9slHj/bPBIuVBKU/Q5K8hsqmw5+MnAjVgp4qasTtB/b+sCc13wfxBYJsaBxGDwLY7sleSpHGHaobHQbjM5C4xQLXy+iZZOFcoaj5ho+kIQ4SxOoX2tBsYK/5B6/5IaLaf+Dmk0atUIxm0961OnLEsH1lLoz62fmsgH2Cei0A4IGqN/otiGB8S+OIUf/5iV8ipj4KGYVTEnHHd1oeI7gcvq2LLKgaab7iuW16SrU5vYoW3sR4SBQ12ajnRgU29Qv1QyQxk8aA5xXa2VHcknuVnrQpppPVqcTAU6jSeq9m4RApkCKflQqqA8egw8APp3rwEUehq7LGEBHBVgJ/ta+tNgSUpBOrYt9hzx5JeuC2NWVEMRQ7ZTIyVYMNI6BEUv5EHeAwI45CFwGJxiXERFI40crfBz2d5OI6jyENI6rE/5NEkaImIHu8C85IYSCI+pGDSOmQ/fMgCpI6+bpO8ZfM4GAg603Kswp8sByF3PjAAOqPncSYT7zD4SaC+vKquqtcmAAC4ScP0g85Y1+w2MqZ/1gDUmNGjF+xAA8JIAikNGE6mMcHtTgAGtKJ4fY5bCJLoYlB22iEVHAujOtlmqQIG/Xc1PVYqaxr2ZkDunkCSyRyF1yWU1ZJAAYugHDLuvbeUMU7s6t6nZ+Qzzodtw+LOjnvRxuWVCyBS+WFBVNOD6tZFw1dydUsfJI1ftWEnht/JHURJKU+PlJpGfVgPpnHRciKs3lq+lRHrrHcVx8IUgUXsFfgBGQgRXQsKro3z5gBHRVsXnYGAny1fTMSzW34hE6cVEog8aEjR0kIjCp0Pd/SMkkQafwg4SJqa/bKtYNt1l+W2gDZWnGfqmBaFYxgZmTVHm5nJYCsqvVl5+ThKZjwEcFlYPXdWOU+JSlEAp0xUjs+0j/beO8Hubcw8A7kHPAZjUNBDboxTQIGlwd2e98NQanAB3pvd4khnblXIa44s4Z5IfzGnri6Kw8AJ7S6yOaxMvpUjXViHwNgj3Q1ZoEzalIBYFlEUTA6QcFFUaVKzN8UUyd/zl/JeEB4Q4FRGhlNaysHAUthsP3cxop0fORa9FRbhcLuII2rCCyODBcih52A6nUCmBK2PZOK2TBFfMsJurznHpsgxseklon2HSBaIMbgwyZ/D6kRLja4TGSiAa81bGXGFMbDQdgnAb6kMl7eq+xr6CGRR2X91TCpKkL0ZQnWjBDDOB6nWyoFtDRpkN1kCiolWzTVw0hp9hPNDmpgjLRYTQLo4OdJmRIIbQ+vEVnBHdAbNlDCzJU0mIc2thTARCCPnRh8OAbXEsgatBlqA8HDaziVgMPKNmi8CU+4jOvGyUWjElOQkSw5Son4360wycnJVoTmOi95HYn0UEv+zUKzXHnXPyB+3hS7I3ah5+/AGlSZUmFdLhYZYzBY+suxuZLxjljwFl4x71mTGXuIxFbhHjxmvv472+z2hqUkmkYhXti2h7MekAGNdf9A9odnYSzmCYNdgUD11IhQxgSX7UgDR5ymXvsylK3oH8VemcwRT3QUcR2pMhcU+6mIrzXuYXXTRU6S4M1aOZgsdXG08QzVJKcOYIWsWeFIri5YLeLAAiKhusN9R0FWZZyFTgGViPeE8EPf4cWXIwdr/MLM478RhtCC7RYU5Dd3OAHj/PMjbTa5iluRmxn5TRYOt8AjtwLSjzsEZqRW6EZCXZswpjF/xIoOePA4yturkgEiUAJEQa98O646pA+TpCECDuRkwn5bIfnV8JDuxfOIhoRt7wQSG7xWEKUF6a/GY4vPBcBslskSg+fpOtNn2aKWSKaJD4HPv56ikjmdW5N/QY4U7UnVgTqT6MZh/EXxYSRGrAL+IGr5xk+gHEPxE7FjB4mDGaisfwTHSgjKBkqR16xb7lKQkRjWRPVn44EQzzaPwsNdVZo/qUXH+/zosbEe4UAoflVkmIbV+X3wfdVHF4QUOAyAVHTWVea7JepAP6iL0G5rEMTZYdlLuXkbrCA27jtVDKcDE7JHXTWKdCLBenfA8RJsYTQ1IaEt+rF3IqAumTnqnYVToTywLExP4GxIXVUynpBRzdJ6ylpOZqbWwTSumPlGYpSndC5IyAU1iK9ovNxXHcmVlReptY2Su4RFJtMq8aLjJlyB8AwdZbL+o0snXClC0s/8F3ifL32q13HlfrLaLVYo3YW1hZKreHZZBQmJFwsvLBe3C/keBSIgbAFE2l2mDTC+QH1OSqsA9vNopkskAJIQOa/3Js47mXUDnutz0+Xwh+OCIqsHojoXmrNwJBWYXc1jB5JkmMLi9dfu/Oh56w+NY+FVUwEMQnxviaM7pCSVQPKNxaNwA8HNpzimuBTY6fofyXKJXk3ZRN2wFrU4twfcybLFHnjmy8agyg1Z2gG8ZGI717rCK18jHBjBCjbcyVhFfpoMMNJuGtwoiYmjcZ6l0QBpBpM4eENlREihQUJS2nzERuxNvFlp9C6QnC3QB3cAVhw8wAHvTM0uHpwB0hocotPH6tx46mstAIw0EEBYRYC+JC0hNfR+BVOQx1UB6Bp0GIVQ1dxZKxt146t0gpnYr+CAmh5PQdzzMjBQDWcj094hh1F3pWEagkGrwSDNZT5fJc1EOWUUjgAIDoElHc+SWrQj1hUJpMET04QlvXpI7KPTS6Zgduk7acWM9fH5xEroTaDcsZxwSDoCqwHocEzQPgVbjKTVx227kPQgNiQuhMRfALSlArsUaP8wRlXwu/kshMcJsG6i4wL4GkYIMiOYfMusnkaqcSZJs8oUG3Ytc7q1cEIHcKDfCzra0hMQhTIINv5Zz6rHPDIpipRzZzAX2ItFRIRy9CBcA9SHT+LAdNX6bGQPWFXyYgui18sqiRro3NJe8VYn06nA8se0AScLOIYTYxHu3FS/w0iMTfGU8SOgVT3NLCNAIzepj/EnSAwR5ujgKqjUnA7gNIQ+iEMW+UDjin6IBkieHmeP58QIsFgnOE/Sh5sFt9FcjjeZtNk4fxYKo2U4Co8gm41rqPhQvGx4IPQtcjtLIXi4Op7ATCPOzDTnekCSF4AkQDSSgBEgiImVxeor0RK+qZJyj88sDQyjtUMa66mT4cXQ5GSLFUM2UQYK3a6bwAhBiXGkMnPj+s7rViEgV6hbNUYiAU5oYrbLJcdh8QVU074jUhp4Pk71rPxEGOny4JJCp/cRSOHNq03INhHcuTG+ITGXeTKR97FWRea/nqsBjgKwtwVK5qegiSKV9Ex34j6hz85bbUNcHzYc094xZphCXHpa0EqZs4oCdWLBOapP0nvUFoiOX+paoe3wpWybGyQzETgJQsCAssvBCK4xSNzOvwBmK4vi78M0brkMGHNqEskYCFsYO4z2UHoMqFTX+oohlAGRoYwsH6eyOcL2eINXUOB7Q6ObIqpdsb68MGwS3/CbKShqoBZyyZxWSgf5N2xGEUUQdnDo0jebIcSATsA7I2kZvsYFDTDK4kgXuEKUR3aOXKJ7Pn4Evt+1CS5ghmkkRjKSDM0U40W3zyXkae/FP5ABK7Al3SH0jHjARlyxlPNeDRcBLEv7qAutX+4+RVEikFdt46U2mitmTJsVbQkhDjMPBh8M4fezhp2ZgKJigbjYIR2+j0x5eCHOOm3hCsICVI6oiUDwiwiCDZjJhZF8DQ8WqewRojSBv532f+Z/04cVanBcSCnSbqtGY838HoN4sT6Ibmvo31C8kPoKGX8ZX1gapqVpNUD3KSRxNOYR/tV2s0zFjN27l3mHpEYDRx44jaSaeLaSaOTgHymMmpktfVCeuCeo0xyH+YBl8XJ31tznHbmGCD6U3ueGsiQUZMQSai24gSpcC0Dr2i7tGAO9TNV9LJ/LUiIm/oYaYaAwz/uz+ha1GDc7OY60AITmjWa/BoM/MNx9pnIYrT7k+GohFkR4EtFqUq4vUBizOwoX23RmgzoeWB0Qnf8AASYZToleQGGRDpGU87Fy52UOD8A66yP81exaXO3ox/NksbjY+OZABQyXLLq7AU6CbrkjmhPJ+NU3VFueZvKQmmrXI0qznIybJdZ0QnUfwKvwz0FlyYopk0WRqZUpBcBu1Mjx5uRlJlahVYWqTEKQ1qIFCa4raoYUBgtFpXY5kyhBcd/TNjZFWJyxZmRB6ASxb5YUWGGFJ8A6i0sUWYw/x8IRJV9CtNgToohQXoFwP6R4ZXcHFTm7k+kcOB+HCbZE11+sI5i/alPYBawxVY4tZh8BxE2BSrTR62nmAx3lgkyDRxTIXzqjmzJGAxtaHhnl2GNhJf7rI7rgNmVDLyi7kLjY/YwOYLjulwAMYxENpR7ISjBBY0cA9S1Q40JJvmJ8lOwaEt1sNnJAWD9qSI0nbw4dz0h159AsdgnZ6lpuN51dLtatBmiwkT2xzE9k79AJMzspwdtAZT+GmaQASm1/xLAucGt+82YZ4SjM1xe3QT5Q9mAGkLMLXleGjSuFGj8FSpGP8kQZGDr5P/qiu5ngV2pKIVmIAibVDcI08JCagwAXbAbo1lB5GHA6I3GV92hID5fLA+rNG9pc8rqd1iX88wx1/3wrAkpv14ZEv5SiXuEm7GRRwWHVWgdTled4JY95momG8aERdSHuLs2GOmxPZoUpEZw6j6h1stXiGj0WIa2vbLpwyOgzO+QGlrVEgX4nbOIIJNNjK+Q5yQ75nQqOv2yIXvIGZBtAqN0dltXfmeyNeIRD6V7uu0Gl7LQIT8xQ4TJY3dWcg0U+L3AZd/GDIel4f6FwbwSReuTPstNXpKKpIUsyRd89Fo0W1rm6ZmvKZysPs4kCxdrvJoXwafQS6AhEMSN4jJkUAUyrWvXUBFRJ9kX/4L1/8R4MF+wfCX7RKg0kvsj/JL4GWEcvnjLgd//ggCoUyU8riksxT7r3LYIleKogAKIvAkxQBE0AQkgCiHRG7yQ84IL7lLckuzrk9G1NWRHhdswsivDwA+v4SMV+Ib74G3qBcSCRFZnCWKhjuE3tvYD89Qia3exEpQk2MRTPE9sE8ljdNs5CmZiP31leYpMNcZSpJYdFYyy/WFAfKKN8TmDGY9C3KYN4iCmrUqYiC9VKZUxFMsCrk/IaLnlzjroIH048Y4Mwv4vAxraFevYMIIxu5K0WzPiD6NYtiteBXNIthPaAhQ500oqonqhgaNQbC9AAAAAQMBYAAIZK0z7psWi+GF3c3WcDjMgqFMaAis/REdGUQRqohxIA1E7AIWDZE5QLJQ/NFC6NSH99WjEjqDTSs7aPRjtSFN4/rqeiFhyvHI3hKgC3Z7rG1mM2yzODLcA6X5WUtMHieZYCcs0RxUKdk4Tsq2yK6V7pXKDgWzPyAvgHd13EqFO+RoRZaanjFEg6d7RtZOsr1vIDn3MFy8O4IsHmGyUZo3DiVXOUc6VjXLVmbq2IctaSsZ8NrfuxZgHQ/rko7ugoeEkAKCuJD+eFz+2/DdrmJhTCzhjgBPv5o8zZWB2S7WDm4jyW396Gw4jGyXHCZnlMu5GSP4B2eD1y6P36ODKOCIkuRcQDpU4uhDo3wQ/lEvSEQ0JjEbEhfkMARR+yqa4oBYJcmrpA/k4MzAeYVjFiGCQndqEqxQED10OMOOtURyQe/IfdYVGkDoPckJV5TXhyoLAQFCBbm1ZxiuJGAYnLDpjh+r+h9pezYAKdTCFx64DjFFMzAJYGQWpIpRz176V25B27daMEXfe5gURsQTqDfnTzefPVzTko+xvXkFMZSdAWyTyxhRNk+YCCThzokhaJDT4/05KCi9Nxni0JoBj6Ol555tDACa4NsGF5A54k+ThGTh43ZY74ZwSkE/zmrgRDE44Ar/thBZ0lhUEGsKnIpFIl2VXMayhlYdNCN5bS0LLkcgUyw8G5PEHp5jnfYaEVSuKz2ZCjYuVsk2CJt+HCgJziYXPscrpI9ZZxeN0EaBQs7FlgHpklUqLpGkd/YpjGo2p1PYgu71ex5SGco/NGT3vxsS2C9PJam+0C6jAU0DsIQoAGNP/EkRa/EAH2xrsKVaGvQCTJnrwQIwk/5iCoOqgdEK0w7cH/TVh2juFdkOn4miiO6BL4TSYWSqJ8UYXJBzlZiJqviLsrk14eHkYjbKAkj/LQJu10DqsZjG72bRJlQlMMsKJsrqVjMIxRUnquByFXR733MvfNLf3E8s7+eQCKciCYOiCAUW8wLS2ebaiIQir3EQWAM8Iq7gJ3AB4RUl/nESgVQwopoIHtBiIrfVDOhEX06njk/WbsUnIdJfC8avzCbNX/yN3+ycFF/0Q6rqfx6/d1P/K1nEW5QqRegkryLGO0gaypGtFEj7IzTaPAMOomwE5idXQbOPSKjrA+kALc6Sq/3INIE7mizGFFE2JmApeqeg1YRykDhGCFSA+10pfNgUKhuSgPwwMsZwpIDDJJRCWXl5fyzlea655nOFNZkf0j+Iuws1ZnqMVnJRgXRAcZdp9DuCOK2SPzeOQNkaXSO0FPT0StrqndufiKARuOxD2mZYdiLaZu4rA6KxopOPIiR2QQsdxDIDTpIUdvr7scFfOCINgoO8AxL1XQrNBJ9GSTGI7OIg1RzuWvraqZUctEq0cVizfS7lIY2skNkVs3rJtWCYpjhKNzDPy11irGwifNw4+rZIsMvDRR34Bqwy3zjj5FZVjfB0HJeYn0XOQvc49RMtRxcjLLGIFg/NQGwXef89BkQ3htGerwdeBHdRgiPDTYQwPhAMPQ6bIsjCGJYiW+tyRlCQELVB7rqNL4sXQVkf+feACGmNKGd+KauWOmhHg3WKcR8oWPrt/NTcIlKBR6AAK/twFr8b1cKjjPxImwNiKqF2sz8DUTwHgAoVlYVcJaiTNRYVUVZwAA5rCrvazwqkWFVxR93TVZFBsc6NPVZFRznz7ClQ1xxzhFcBeWWjENcwVArCq3SEpJGFy6FA2RBEB5QgNl1eaUJxsiGISK6o2SYNHKsxssANiFAERqVa6SotCsgjZN6HVBeU53V0oxyL8RJLoxSfXhpKZmeekMSH43JjB/3cyqGuVCKeg6hKQUH8S/p2B/KxUkHgEfYHsYUeSePJTOTPcm1cUw9RqYAdreWCNgGKUSvWWA5MczOGIvbODZ6zlWYIJa6CfZg/rIDQhBHSz9YWLVtVIrP2zgwCc77CmHpO/KJ32ctgdEnnOvccEwDrR1AwWRwouPAP0ggaYPk+NThalL1IW1Pe5lWgaFnmSb2jERLV/9ZM/iMgRgRQfGFM9t6gJitvbgOAMQ0QA7MlGIaHfLRZudaEBgJhYq+mM4JVRWhtoA7WvFTc0jlpmGpK8K0KKrojyWkuSM54yWsTYem0legSjlLoJc9KyUfsp1yQivVd51NGzdNrh4EQ3I8/Hwdjmt1ThyeYXJjDnP5DXDdOOgbvBTiBRIMIqqR54cVpEWcVjGpcBAAT24IiwUDDRg+JQwvqyUgZ7C1bgO2eIJhdTQMy28KAWHdzoEIpd3UaTACyZWXLFWFGBCLt5VQGm1L6rxXpqfghvRsLvc/ywvroEBAzVw732BHKlVtmta8IxcnRds0YQAkWvevPvo652eJILlFaj/g6apErVAHWeD9gGQe7lAls2zcd6sEAcBgK5TJihJDlCH0aAZ1twfk+0QdNBC/i7YIQAavlmHIitvA/pFGzCtIKZ9gbac9AupKUWIOyerxIbiabZSa203mlSkJ7IxW33YliYciSLaPMQM7mxSakMUyZARPghzEoOxfkihvGEECW0WdrKhEx/USiot8DwArKzzD7RnmnkMx81NLY2wT4MkPv1J0LR3cbGBwmuuqf0LIhmC8Mao3H5vSuQWEQPwEPINarKihRv0M96YHiH33nUEGa3TT5mAteLLuTbEdxqDgk57HEnnGAaNgO3YIMuwHrHsUJXpeEBLXogPJTAd2RQSYGxBDFqe5qQrc7guTZEMdFsMUPzjcXsygRRHE5fVmEO3kAeWaXUiMXkIChLYB5AVTOruggt1AySOfhuH4=) format("embedded-opentype"),url(data:application/x-font-woff;charset=utf-8;base64,d09GRgABAAAAACKgABMAAAAAPFAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABGRlRNAAABqAAAABwAAAAcYdMeikdERUYAAAHEAAAAHQAAACAAgQAER1BPUwAAAeQAAAAgAAAAIGyRdI9HU1VCAAACBAAAAFEAAABy0K/hyk9TLzIAAAJYAAAAUgAAAGBsELtHY21hcAAAAqwAAADcAAABqtwUe69jdnQgAAADiAAAACwAAAAsEIwTzmZwZ20AAAO0AAABsQAAAmVTtC+nZ2FzcAAABWgAAAAQAAAAEAAXAAlnbHlmAAAFeAAAF2UAAClYJPh6PGhlYWQAABzgAAAAMwAAADYgBk6EaGhlYQAAHRQAAAAgAAAAJCXLFZxobXR4AAAdNAAAAMEAAAFQq6Rcr2xvY2EAAB34AAAAlQAAAKr14+t2bWF4cAAAHpAAAAAgAAAAIAFyAZBuYW1lAAAesAAAAmoAAAdSoHrnuXBvc3QAACEcAAAA0AAAAVCFYc/OcHJlcAAAIewAAACrAAABGB8tljd3ZWJmAAAimAAAAAYAAAAGTlxUdAAAAAEAAAAAzD2izwAAAADE+3zgAAAAANCZ/tp42mNgZGBg4ANiCQYQYGJgBMJgIGYB8xgAB9sAhgAAAAABAAAACgAcAB4AAWxhdG4ACAAEAAAAAP//AAAAAAAAeNpjYGRgYOBikGPQYWDMSSzJY+BgYAGKMPz/zwCSYUxOzCkBijFAeEA5NiDNBNShxsAM5IsBMR9IPZBmYhBlMADSjAwKWOSkGAwhcgA1mQcoAAAAeNpjYOaIY5zAwMrAwjqL1ZiBgVEaQjNfZEhjYuBhZ2VlY2ACARYGoKQAAwKE+DorMDgwKKj+YUv7l8bAwKbJpANSA5JjUWdRB1IKDEwA300JdwAAeNpjYGBgZoBgGQZGBhBYAuQxgvksDB1AWo5BACjCx6DIoMagyWDAYMpgyWDPEMlQxbBAgUtBXyFe9c///0B1CgyqDBpAeSMGc6C8I0MiUJ4BJv//8f8H/+//v/n/xv/r/y/9v/j/1P+lD9weKN3/fCscai8ewMjGAFfEyAQkmNAVAL3AAqRZ2RjYOSBCnAxc3Dy8DHz8AmCuIIOQsIiomLiEpJS0jKycvIKikrKKqpq6hqYW1AxtHV09fQNDI2MTUzNzC0sraxtbO3sHRydnFwbqAleydAEA7cAv9gAAAecC0QCTA38AlgEcAR0BNAF9ArAC7gCUAJMAlACWAR0BfQKzA38ARAUReNpdUbtOW0EQ3Q0PA4HE2CA52hSzmZDGe6EFCcTVjWJkO4XlCGk3cpGLcQEfQIFEDdqvGaChpEibBiEXSHxCPiESM2uIojQ7O7NzzpkzS8qRqnfpa89T5ySQwt0GzTb9Tki1swD3pOvrjYy0gwdabGb0ynX7/gsGm9GUO2oA5T1vKQ8ZTTuBWrSn/tH8Cob7/B/zOxi0NNP01DoJ6SEE5ptxS4PvGc26yw/6gtXhYjAwpJim4i4/plL+tzTnasuwtZHRvIMzEfnJNEBTa20Emv7UIdXzcRRLkMumsTaYmLL+JBPBhcl0VVO1zPjawV2ys+hggyrNgQfYw1Z5DB4ODyYU0rckyiwNEfZiq8QIEZMcCjnl3Mn+pED5SBLGvElKO+OGtQbGkdfAoDZPs/88m01tbx3C+FkcwXe/GUs6+MiG2hgRYjtiKYAJREJGVfmGGs+9LAbkUvvPQJSA5fGPf50ItO7YRDyXtXUOMVYIen7b3PLLirtWuc6LQndvqmqo0inN+17OvscDnh4Lw0FjwZvP+/5Kgfo8LK40aA4EQ3o3ev+iteqIq7wXPrIn07+xWgAAAAAAAAMACAACABAAAf//AAN42u1afXRTx5Wfmff05Q9Zki0rNjZC2EYIY4QlhBDyF2AbMMYYRyjGaxKSEmNbgEMIdRwv68MhhLIcYEHGcQO4CfVpciib855MaJZ2e9olX5RNONQHODS7J0vpllXa3e0mOWkWrOe9dyQZk6SQtN39a4/RzJ15782dub/7NTMQRqoJYRtUa4hANGSOTImzLKIRs/7dJatV/1QWERiQRBawW4XdEY3aPFYWodjvNtqMRTajrZpNUwrpoNKuWnPr+9XiuwSGJHXjN7UNqo/IDDKbbCKRmYQUR0hqttvtjliRnk5JsZTnBjbRkdnTZ2qLqVTilMgVmaZHJWqQU2mxnJYRldIMcjaQloyoPAfqVGo0RabbZ/t8PinNGLHOLEYq2xTJc8wCiswtzXRnTWVuVwXzTCL0rGD6DLueFkyfw3RMo6N14apNhwOBQ5srByo3Hwo0hYEIDr6zdVBZuuzWt7e9fSRIX2EtrLlDMSp6lhY4FKqsDB0KJOttbw0EB3p7oXibKqwl9r0O+pGiJ4TiujXXVaMkj3STiJbBIrPcET3D1cOKZaqKSrmuiBroiJj9AIhjRE302uKRNK0eZZDvlNgVKcUl54IYjK5ILkspHqnKzdMVR1gukozoiuWpIIlcZjRJeh+RtWqj6TVBpcu05BRafHNLLR6bEVZo11GvUQOlxm60QAk9Rpu5TthzWXlfeb+d7qdFly910p10d8fFy9RK97Yp/6xcvyzsyBmgb47Sacq/tNM9ygeX6fClDrpb6e24qDwM7ym97TRP+dWo4h9I4Lw3LSiWkwxiJR4SyQRs5fS0aEREkFWwbnmKEOVrl1LcVJrmlG20mMjpxGiSDdk+nK9JSwvdLlFLM2dT2zTRzksVKA7AlQYSDVAfPUavj43RG/QF6gsoO8Y+UfYqe8c+DdApbBNtoA1Q5j+s3FJ+ohx6402lH+rPuqmd/pSWHj2qXFQqlWsffPrp1j2HD+8Zoik/DoV+rHwKUyLj3XSr+Coxk+mEzzVeGGGyVMrm2piaHpUtMOG5pcZ5Fcxr0Qtm0CqLZo5gX7LO3V3SWFFQUNFY0u1et8TOTJvPnDi4LFjZd+D4utbjB/oqg7WHTpwBMRE6fhN4NSd5pSIbXpiTvFRXZMMEL02F4Jk3h9m9U4Gdnmly7uKDnN3IpLV1CJksO3jizObNr504XENEKirtaQWqs9yeUwGV1QSMFVemS0fbo0lNjIjaNOigkoGvU9BHJcEgi6BYGiA1BlmL1gZkqkFOAzJDH5WNfHLUiIZ/50fFfvaf/ey/sFTaNU2fvTr5B2un4vi7GrvqdzCnbJgNKobAly06ZRUfsoha4C3V2Y5btW7V30PJZTbxXSqxkYgA30kqdyQFv09FfdLB7NOccnp8CFB3HY0rOhXZyViwg73MTnbEgryIrXWzk0gF2ClsBtvYK7FmwibNTUNmJWY3obt8npLGLQsaw7SfOKmkdco6zg5Y8T8qih+237a0i9GO29lULX7YcdsSwjInMbYDxp4CtlFBIhrCJx3J5hJgyWIK+gWzxe2Wp5jjXJJGIkwBI8m3opHomJd6+QrxD1aJf2DSaOYWr52K9Kri6KBX2xVHO71Kf9Gu2Dt5Sa92KI5Oeq1dmaEUw6NOxcEqQ+0hqobX8Fk7vdYBr2HRiaWDjxBSHG30slLcxklW2dkeSmCitNNBrl8zEpo1oVIIaEKX+E81oS6gInHNwDEYjqGaktBRd3wURHZiIEl0o6i/qJcJ4dO7NO+OzhEi4NjaFyf0f3F8dFmlm6z7sqh1c925p+YnFIt+marfrecJv69tYGkglWawb/SCJPtLYh2V7PeJcjMTUU6ajhFOsmJ0k/K+UmS7f1C7ZyRDbBgjRO0Q7Vx+cxPS06iiCbkJ0SQwIsxaNCDGspCeBMaWyAgYE+1jXUyJQX37WpgpiLswaWw10UHmER9dnRxdg6OnTB5dmxgdxaQVwRhUGojxsloDpKhDu+AcqS3TZkzwPBILtbFBZDsGNGODwDdIiOoyxKg0YgBbBI9oRHwMEKXUPC3BQo1TsSRClZyOU8njU0nngVhKN8h6WiyZXHIaTCgfJqRPB5DUPr5siA1u11Rm5qv32MB3F0zXMxas7f3uutaXemtjWWxt7GW2du7qzeXlT6yey3a0DvfW1PQOt8Z2hWmwv7yryels6gL7Ojj+vvqYuIeoSMFk+xK5D0c9EkBz1U5Zk/R66O8Osm+x3X2xW7HfF4mP3R6qE3aN7YCxTpId6p3CSUByXlLWyVgsuBMmG0dTk841X7wLzSKQbQH86Enxanhs1Q6hZGyUvRBr434gOU8dKZo8T8x4IjoUoaBCG0txIngTM7VTITlb5abyb310ybeTc6a/VnLrmJOmwNTBR4RJq3oQ5q4j6aQ4MXtAhY8N/oHXPI2SUxArvVPOSIQSG2Q8Nj3VZNoEGw3TjeA5Nz63ZexGvidXYkf2K7uPCfqxj6p+5+8po7voAlhLM9mpRv3IJDMJl7SsB06Z8RjD6xFjpojpWZZTNifYVFC3K9sCbtg8B4xPrfHAOHSf78Ta4HcX0n07le0F652ljxQp299lrz62fv1jsaZ6JrHyivIKFmskCRkOgQzr72CNsNAk0hHGAZrAOu796cE+yGDVfbFekdSP9Qq7628PobxGYaxmWEM6xFh3XMOlTHdctzHMoJgi2biUdKOgTZRUsjjlB+Kjz8ccDBIbWIyosVQgN1geHd1Bi06coIW9dZtj+/cr45tW2hbbqIPOrK5SAsK+YGtrYGddIFC/O8tmjI0x0VYUy8G1NY/fVF8WhokR8x2+Nl1ibXoETYWu0OSUMxNLo1NBnPO9sEB0ZTPstLmP7it62OVcX8CF6RsONp9YKLTUxZr8FeVl7NW6WONjjz76GKxPpOeB1wZYu514yRKw77+OZ3NSqlt2C1FppSuShzz9yLOJ6/tM0PeZBoyy8gIgFxjkuUBWA1ltkCvQEwNpMch1QGJa9iC+aDOaRlQFJW7IsuXqudDI0/oXYcNSAY1U89KV0CCy2w9blJJFS33onyj3DNkW1BbwD5jMZZu5057DPPO8cwQvd+XzPfNmoMegWdnJhjoukjitMdPz7paemobdJc5vNdQ+3eIuXrHRn+1p6amthkaWf+Py4nB+afWMho6OBvuS0nzKQisaNm9uqO+kRAzVN3SGVtaHRPYJvr3I51sEgy2p7AqWaRdWQtf8+dCs9mjLgluq7NWl+SH4Lr90iZJb39lZ3xAK0VDdRuxrq1OjriXlPZOsJK+RyBzUtTJ3RIu6NnMi718Boq9OiN4DuzwyR6stjjgWLcVND29QqYGj4QBhO+JouIB0GeRlQC4FcqlBXnAHjUUJNFbhixyNwtkrQObSMqM8xwn7wKWwD/RocUe4wCj7y6C2IDAV1RyYOR4IG7Mr7g/LfI4LIAH5PjUnIbF772BjLohDdN7T8nR1w+65zt1xUJZv9GcBKEtqe1rcKSn+x1cUh0MrSxx1jpJQQ0NniaOmdBZC0RAKbwZM7oajfGuwLMWzOAmH1Z+fUhZ8oqIhZOnLDzgRCWcgv8+COIRWwA99iJoWjQ9q1CoCvssKdl9JHiSPkCMkUoQRrhwiK27IIg9iYUGTeMgtZ4Aj4ADlA0DrXBEnAuRFJ7qeo5EFws4yyNMg2uW65HkQiVNc0jyDvBA6qlzyLOgIuKRZBrkFYKgDOB6FeloWwJGhsuSjPSycB4IuAkDklllA1aziG3OaDJIJOXsn6/of+6zI2bSlonIzRs/NlRhFwy1Vi9cEF1X+BS1xBrrKyzcH5s4NbKoo7wo4B1oqK9c2V1W20NLk28mvB/AL/FK4XN7V6HQ2dpUnB13c3Ly4cu3aMTkRoxMPNlXxB83N7NIf+ALxgZ1EbEzfq3aAb1pFvkG6yXfAN6G9ZLjlFaqotMgV+QaCswYB2QaA1LoiBRh4ctxyCTS9rsgjiE8HQOlC772tYA0aztOT3VgjALAByA0GeQuQLYBQi0EuAzILEsoeqLdsMJpeKyhZseYRFQJU1gLNlJyproxabhnbOgCmmWVoGcZ5JlKI4rZUCBAQiMmcxYiIcrfrhYLphQTkb8oWMfuE3anbVTjf5JnHZhSi94JdMQQPNVN9AbFEuprIi4ZpPv3LX3T/68XTS5edvnij5xrdQfOHh5Ubyq5rPTcunl62bOTir795Tdml3BgOH/iYLvvh9uvv/d3yujOXfvnUWVr7yf79nyhnz26/fulMXd3r70Gf8oOPD9C1wecvbNv2syNr1x658OS2C88Hw5VdA4FAuKuysqv/wcBAVyUzxTl5N+4LBvdt9CLHZ3+BfH417H18fzC4/3HvMM2jO66dVV7/+EDtzjNdXWd21uIMznK+NX3Y01cD/H/4w20XjjQ3H0GWA83NAz/bFucUDgQGtlRVbRkgFPLds2qH0An57tREBkNSohPE5P2vMZ6/nmVSIjdgP4KctYvnyn94n2WK8p9qIvdmP2IjmPryPD7Jm+fxIt+RaBPcNffJ43E+FPLYO3k8nxmdF+V7hG5qVZ0WqvnYU+OxVhaTWTyNj53cqnlsZvyxbqF87JxQTq3hMD04MMDX2E1vqk6z3oR8+AwhJ08Sk+STHIHe7O/Hcxt60/riV/nOCN9ZX+Rf4RnMmfGb1qi4lbxEPiQ8M/rBarPV4S5fmlpocUceRzvswWI/FgZcVRhWdRxzsUe374WwxU8ryXEDWuCJ+D5OP7GPGwQLHDTIB4A0A2k2yFYgnwX5PmuQnwHSkcHDnBvIJ+CFJwxyG5DlQJYb5KVAtsJgrQa5CcjVgMV3kzvB4z75wCDUBp9sNUO91yc/8yzUhT7Z7YB6u09uewLqBT55aTnUj/rkplao69HtChObxQmr/JrtgoTZ3gmQX69d179k+1BL89A3q1uHRnu6fz7UUt19tKVlaPuSdcehffnouiXbj0Ebnh8bfbpnFJ4/he3uJfz90eMt4eqnhpqTHT2XjsMAx9e2HO2GAX/e0zN6rBUYNOOA/PnPj7VWbz+O71e3Hr3cjQOy4pZjT1XjsD2jx9ch26/dhtFxGT2jR9etO/q126h/hID+vQg2vYbsI3zDGTHm2fFUTp007ZE5CxbXgzLiOfFImmV6SaHFRaWgU1JP7NGMoBUm0CSTQc4DMh968w2yHciZ+qj8ENRGDcdeMhmlpT4pzyQt9kn5RqncJ9lRR2D/DEo1Amq/mp8Xmz2u2dTkccfdOvrzBDmpN0GmUbd5+myK5w1pkKbfi/opDVISDvcrNPgP52hQCQ+EFRo4d46uoaQ/3h1/pT9MCTw4pbyhfHbe7z+vfKa8ceqU8iZv/SO03oTWW8pnPyv73LPzyu+VN3Nw2OC5c8pJJXxvamDj68rgoPL6xon6DNZnJtokjhEhgFE5YHQ47nchf7Q6EBIiREdmeaog4XRFVqNPcC6sQfBWJ3eiEQMWuVgUcjcNuK2+IjcCQo0GuRaQqQHcagyyH8iF0Lswvu1wZsRxq20E3FJ9Uo0RLF3ymySzT1polHJ90lyTZPVxf+ZKo5i1zoa89Z6UGeMwWC6PzAkSz9Mn9abRRO8p6qe6d0C8VEv9p07RMqoF8V6AVtmpP/xs4alzyikOq3ISJPx9BVAdJ8rJn8Slnej+23FAO4z9wslwW1zSbW1xybd9vh2Of4Tacm8KfPm745J1VOyGvHd5/Gx4hKo0OvTjaei99dyF333QyosMhCc13YibkMwM4a5NvaDTWnRaNysVPhI+Fj6BP17St9tio2Np8JfOS92YXvGxWlY6liJ8MpaW/NE3N8YuT+rQCp8qPh7nkudeqaQpHq8kFVcnSeOaNLMUmij4yWTKFYm5ZB1oieiK6FLw6ken0cELOiRT8BYoPRkb8XTSjGc1jNELYXpe8UIRphcUT1jx0vN8j/xb2LMt5mccduInS8k3SaQEI1xNcjdQAAQKR86BmOdFohIj+bLJewI8mywDsswgL76zWcPbqBTQ4eVQLy6DzDIjp6DEq+KZZU0lZppZU2e6CHc29D6poeVeyf5v75vhbXU2dlb4OxtLSho7/eWhRme4ucwfCPj9a5npPvlawB/Cz0Lw2aqSklWhcn8w6PevCRBKz45fUG8TWyDfmDI5F/uyS4yzYtPG2696xKLO2+9z7IMkoLosjILcHYlMKUOMThCofCjflMwo/8X18PNCunOOWNv7UmvrcG8tM2HJe088U1v7zIn/5bPbP++55J/z3GuyXleTL6oylWZO1uCkrjqgngp7VjlDhSfKBTlAphDfV9DQr6CF99W1P2lv8IXcPpnSJ3BOu39ufzfOfOR+upfHv1vkjKZU+DFJR31FvyqnCtFJgKfTRDHpzDWTWjQ2O8Bk99osmlt0d9t7Quy9Nrw1bhu9/d+jbcxPd3VevNip7FB6O0ZHO4hAJdDVXtBVFTETG/ijSA7ysmqi8SNLviYjZPWoEbIOdXe6U8q+IucZonIBXo7lWI2miDotHU97jEY5I4tfk1Eb/BOmUoutgnrnULtNL6jmzXe7ss1Z6oLpM6hES1kHLVUuxp7f43u8nP7Ut6HC/41ypTLf4/FQfRNafJm/iZmYOfab2H8wU5vUsWGkfcNr7e6HW9fP8z/0kN/f2Aj4FcH81TB/IyKAO3pZ0EIyoErGGCnDiQecUsYVcOCyHrRP7UqedpZQrwp8dpElnWqKwG2rWBFtVYbr6MNh2lqnDPMirJygde+c33mBDiibzu88/87Od5QueugdsJ/68Rvq6+J6UogY5XG5gaRSkjcYmHyMZFhTMLYVOeUZcdjneysELz+0tlLNDDvIJn5urdZA7NBTqGl93SpbfUOjPXijp/uDxid3mMwGh8E7FAy+UuX105eUDPdD0+0F+Se/U33owE731ief3H6q0ZqX45sSbGpurG/vf/j0skA92CRsotQR1Qckg5iIj/DohmexDCJeuosnu3gYPJH0ygaEN9MpZyGuzABZkAmvdnAHKngzPZkWAaZIi2yU9rB8ZRPtf7vv/Ft9b9Gwspnl3X5b8CmPQ3ZB1y5Xvsdo83Ll5fBYMMwucL/F5wJ+i5Fpn99HJzbTghPvPdAD6pDBEDsq7rndg3eJd75VAc61JKJDWWeoJ+nopEtFQeBH2piqp4GSZuLVnhrvrCh6GKqDZal8nAusRJislJxpfB0HmuLx6kGcA6xBsfEwBCoHdh8cH1JdFhfz2zMnibCk3XMXKiZcqJovKnHvImtgBpIW3Zo305ZpocIUCEl7e2hvz15l8XPP7RJTFijvUdeC2/3sA+U0rf//XOFPyhUEuofL7iDovh08w5OTdY6nnvH8E0WWqYqbqjwLZVfqlEquyFaQktWAVzKyGpw3ZIAuIK0lRrwClh4wysIUcHdqk0RBXrMK8f/Q2KGDGkcemGKdy6UGaYhmKjUnLoY1RXiLgDID+k538ijbTveITW3fXhZAKW2/2PA8Fb2DQVytf35TUTFKpjLk6Cyu8Acspk7Iavrr3+hCoaxbeegzfxWKpOlvvI2ODWUoCaejrTjozy81/0k5E5v4NpVY8H9+GFlCZDwYjFhSjejZHnDKOROejeJJPbqwyfRZf7X/heDG4AtQs0oojkHjGNQeb7XvcCAUOOSr9rJPvUug0Rk4DA1ChSANiKPs0uR8TRedICbyNWOU/748XxOCVnftDHutJz/fU2u317itzGSvdVuhl7eg/j/ODe/gMYUsAD/GQwXwSuchPfm/UPiVt3hFNmXGb7hNotE0QtMyctE8QdAQUD2JixPvXUpGz35Y5ECO9c8t+M3G3yL9Umv9noVRzy89f7UMp+Msvt75S8/OWpyS03E9nscEx2+CP7OTNJJDlpGICedhSqoK9y1aPqvciYv4dAP+DymezUzh90LcuxHZpAVTSLfw/A3ym6RDwCNsvJW3saDQl5RTmLkaO/ktfOxl+grri+2My2u409/RUIIH+fTmxFmAcFW4CjpsI19y/ApSwl9ck0uBLb485oCv/gdnUuGAAAAAeNpjYGRgYGBkXzf1yIb58fw2XxnkORhA4MLMf7dg9P9tf//Iv2TTBHI5GJhAogDJwRCXAHjaY2BkYGDT/HuFgUF+/v9t36/Iv2QAiqCAEACxUQedeNpjesPgwgAETKsYGFjEGBg4pjN4cWgzePHMZ/BijGBgYIz4/5xXjZGFQxsVM81mZGGTY2ThXAzkg/UwMUFxGFC+H4hXQenJQBwBZV+Gsk9CsSqbP9BOiL49ULochOXnMzDIz/+/DUKD2Wch8oxvgHgXxB4wvx+K3yDcwPALyN8AZKsCaR8grkRgsPtAZrRDzAFh5jCoONhcUDgwizAwsGkiaCZeYFh0ATEbBDMcBdLWQDocQoPkmK8zMAAADLY0KwAAAHjaY2Bg0IHCFYwKjHWMWxhfMBkx+TE1MK1jlmC2Ys5gXsLCwWLGUsRyguUdqwyrD2sT6z7WJ2w6bEnsUuwPODZwruM8w/mGS4rLjiuKq4x7G08HbwzvJt4vfJP4NvA94xfht+JP4N/Df0uAQ8BGYI7ALUEtwQLBPsFrQlxCK4R1hL2EO4T3CX8RsRPpwwvXAACGdS0RAAAAAAEAAABUAKIABgAAAAAAAgABAAIAFgAAAQAA6gAAAAB42sVUy27TQBQ9TprQ8sgCJBZdRBZiURakbgoSBAkJVULAokIEgZDYOLbzUN04NHZQ+yV8AYt+AR/AAj4BiTULPoA1594ZJ6EUG5UFsjw+c+fc9x0DuILvqMJZWQNwxNdgB03uDK6ggXcWV/EU7y1eQQs/LK5h3blucR1N577F5zB0Xlu8imvOR4vX8MT5ZvF5HFU8iy+gXTm2+CLxV4svVY6rDYsbaNc+WHwZzVoe/ydcra9a/BlevWnwlyrW67vYQYIJDnGAEQYYIoWLDQS4wW8bHp87RD0yXLzVp4WpcsfwEXMfUXeP0oDaLbIeUBrzu7A41V3Er3BnXEMyu7SQUL9L2/v0kFBrisfU36HsGVkDZJT51Cn33KGPYoudpYyKme4J7y/Uk/hOqOdiiz49bPMttlPmZaSV8fmmtO+zKhF5vmblkt3/y5qXc8oZLm5qBVNK+zyXPB+xAj1lLSr3/+ZFTlL67mCTT7ntzTNovGJ2Gb1LFySDTGOQ/gxtt/o6A6nKEq6hyic6KYfazTFlLiWSj2EGVieye18tTdT3PlmpnoXzuok38RrzLFCtPAqjkcdxsMQVfwltZNRIdbbG2oGh2hj+IQezF25Ab5lOYKhdOVkJ0YgVbZAv3Y50pk3cp9se/0PuC+uhWhrMZyLVykqW5j6elkHu/fe47i31SDIxuaTqb6I19dW+yTXU+yuZJ3o/iybB/6Xr5tYkdjVZGZzpTGaqKdHObJdzO8KMySiaobPchuK/0Uuye0s5mX/cc8Ypke1SPtMbfpurh1v0u427djU3f4vSh3P9Lt5Qd8QIpGvxT46mOJQAAHjabc1HM0NhHEbx8ySRItFHGaxMtGjvfZObgk0GV4sE0csaM8bYWPggPq92/3bO5rc8JPjts8gH/3UGSihJkhRZcuQpMMIoY4wzwSRTTDNDkVnmmGeBRUosscwKq6zhCPCUqRBSpUadButssMk2O0Tsssc+BxzS4og2HY454ZTu9/ecCy654pobbrnjXin1KK2MssqpV3kV1Kd+DWhQQxpOPzy/vz4GMT7z9vLkXNP96J37MzC9WTYrZmhWzZpZNxtmM9ZHsWFsGG19AWrNNVZ42kXOsQ6CMBAG4NaWFqhQQOJmgnN3n0BYXIwuNHHzHZxdHPVZDifjy+lhjrrd91/uz7345wb8znYQ7/uB84cfOuX6NRR+B/UBh6tfgXKnnoFoWhBuC7Jpn0LP3A8RQk5QiMgSdNO+mZIbRo5xqXNCgogNIUUkU4dBpEvCHGFowyGjB/KxOZNHbB5Ed8HEjknOz/+kwEtrA0tkYQIrZKkDF8gq0EPtvvWZSeUAAAFUdE5bAAA=) format("woff");}]]></style><font id="snowsymbolsiacs" horiz-adv-x="2091" ><font-face font-family="snowsymbolsiacs" units-per-em="2048" ascent="1638" descent="-410"></font-face><missing-glyph horiz-adv-x="1046" /><glyph horiz-adv-x="0" /><glyph horiz-adv-x="682" /><glyph unicode=" "  horiz-adv-x="1046" /><glyph unicode="&#x09;" horiz-adv-x="1046" /><glyph unicode="&#xa0;" horiz-adv-x="1046" /><glyph unicode="!" horiz-adv-x="2199" d="M74 524h147q0 -84 32 -156.5t86 -126t127 -85t157 -31.5t156.5 31.5t127 85t86 126t31.5 156.5h150q0 -84 31.5 -156.5t86 -126t127 -85t156.5 -31.5q82 0 155.5 31.5t128 85t86 126t31.5 156.5h150q0 -115 -43 -214t-119 -174t-176 -118t-213 -43q-152 0 -279 75 t-198 198q-72 -123 -197 -198t-278 -75q-115 0 -214.5 43t-174 118t-117.5 174.5t-43 213.5zM498 920l602 602l600 -602l-105 -105l-495 498l-498 -498z" /><glyph unicode="%" d="M74 -25v150h903v453l-213 -211l-293 288l-293 -288l-104 104l397 391l293 -289l213 213v420l-209 -209l-106 105l385 389l389 -389l-105 -105l-207 207v-418l213 -213l287 287l393 -389l-102 -104l-291 284l-287 -284l-213 211v-453h893v-150h-1943z" /><glyph unicode="&#x26;" horiz-adv-x="3231" d="M74 -119q84 295 240.5 519.5t362.5 377t446.5 230t492.5 77.5t493.5 -77.5t446.5 -230t361.5 -377t240.5 -519.5q-387 223 -777 346t-765 123t-764 -123t-778 -346zM379 217q283 135 593 208t644 73t644 -73t595 -208q-94 158 -225 291t-291 228t-343.5 148.5 t-379.5 53.5q-197 0 -379 -53.5t-342 -148.5t-292 -228t-224 -291z" /><glyph unicode="(" horiz-adv-x="344" d="M0 530q0 109 19.5 200t53 172t80 152.5t97.5 143.5l119 -86q-45 -63 -85 -126.5t-71 -134.5t-48.5 -149.5t-17.5 -171.5q0 -92 17.5 -170.5t48.5 -149.5t71 -134.5t85 -126.5l-119 -86q-51 70 -97.5 142.5t-80 153.5t-53 172t-19.5 199z" /><glyph unicode=")" horiz-adv-x="344" d="M-25 -51q45 63 85 126.5t71 134.5t48.5 149.5t17.5 170.5t-17.5 171t-48.5 150t-71 134.5t-85 126.5l119 86q51 -72 97.5 -143.5t80 -152.5t53 -172t19.5 -200t-19.5 -199.5t-53 -171.5t-80 -154t-97.5 -142z" /><glyph unicode="0" horiz-adv-x="3366" d="M260 -408v1874h148v-1874h-148zM1159 -408v1874h148v-1874h-148zM2060 -408v1874h148v-1874h-148zM2959 -408v1874h148v-1874h-148z" /><glyph unicode="2" d="M260 51l1466 1469l105 -105l-1466 -1466z" /><glyph unicode="3" d="M260 51l682 682l-682 682l105 105l679 -684l682 684l105 -105l-682 -682l682 -682l-105 -102l-682 680l-679 -680z" /><glyph unicode="4" d="M260 262l1258 1258l104 -105l-1257 -1257zM469 51l1257 1258l105 -105l-1258 -1255z" /><glyph unicode="5" d="M260 262l471 471l-471 471l105 105l471 -471l104 104l-471 473l104 105l471 -473l474 473l104 -105l-473 -473l106 -106l474 473l104 -105l-473 -471l471 -471l-105 -104l-471 471l-106 -107l473 -471l-104 -102l-474 469l-469 -469l-104 102l471 471l-106 107l-471 -471 zM940 733l107 -106l104 104l-107 107z" /><glyph unicode="7" horiz-adv-x="667" d="M260 -408v1874h148v-1874h-148z" /><glyph unicode="8" horiz-adv-x="1566" d="M260 -408v1874h148v-1874h-148zM1159 -408v1874h148v-1874h-148z" /><glyph unicode="9" horiz-adv-x="2467" d="M260 -408v1874h148v-1874h-148zM1159 -408v1874h148v-1874h-148zM2060 -408v1874h148v-1874h-148z" /><glyph unicode="?" horiz-adv-x="2199" d="M74 524h147q0 -84 32 -156.5t86 -126t127 -85t157 -31.5t156.5 31.5t127 85t86 126t31.5 156.5h150q0 -84 31.5 -156.5t86 -126t127 -85t156.5 -31.5q82 0 155.5 31.5t128 85t86 126t31.5 156.5h150q0 -115 -43 -214t-119 -174t-176 -118t-213 -43q-152 0 -279 75 t-198 198q-72 -123 -197 -198t-278 -75q-115 0 -214.5 43t-174 118t-117.5 174.5t-43 213.5z" /><glyph unicode="A" d="M514 0v1063h1063v-1063h-1063zM664 147h766v766h-766v-766z" /><glyph unicode="B" d="M514 0v1063h1063v-1063h-1063zM664 252l661 661h-661v-661zM766 147h664v664z" /><glyph unicode="C" d="M598 0v637q0 92 35 174t96.5 143.5t143 96t174.5 34.5q92 0 172.5 -34.5t142 -96t96.5 -143.5t35 -174v-637h-895zM745 147h601v342h-601v-342zM745 637h601q0 61 -24 116.5t-64.5 96.5t-95 64.5t-115.5 23.5t-117 -23.5t-96.5 -64.5t-64.5 -96.5t-24 -116.5z" /><glyph unicode="D" d="M399 37l648 1122l645 -1122l-127 -74l-518 899l-521 -899z" /><glyph unicode="E" d="M426 125v813h1239v-813h-147v666h-945v-666h-147z" /><glyph unicode="F" d="M399 37l648 1122l645 -1122l-127 -74l-281 486l-280 -486l-127 74l323 559l-153 266l-521 -899z" /><glyph unicode="G" d="M403 156l359 782h567l359 -782l-134 -64l-110 240h-797q-29 -59 -55.5 -119.5t-52.5 -120.5zM715 479h661l-141 312h-379z" /><glyph unicode="H" d="M344 205l395 686q57 98 138 146t170 48q86 0 167.5 -48t139.5 -146l395 -686l-129 -76l-395 688v-2q-35 61 -83 92t-95 31q-49 0 -96.5 -30.5t-84.5 -92.5v2l-395 -688z" /><glyph unicode="I" d="M399 1024l127 76l521 -901l518 901l127 -76l-645 -1122z" /><glyph unicode="J" d="M467 907q125 86 271.5 132t308.5 46t308 -46t269 -130l-74 -129q-109 74 -235.5 116t-267.5 42t-269.5 -42t-236.5 -118zM618 645q33 20 66 36.5t66 33.5l297 -516l296 516q68 -33 132 -70l-428 -743z" /><glyph unicode="K" d="M344 858l127 74l395 -686q37 -59 84 -90t97 -31q47 0 95 30.5t83 90.5l395 686l129 -74l-395 -688q-57 -98 -139 -146.5t-168 -48.5q-88 0 -169 48.5t-139 146.5z" /><glyph unicode="L" d="M457 756q0 68 25.5 128t70.5 105t105.5 70.5t129.5 25.5q78 0 144.5 -32.5t111.5 -89.5q47 57 115 89.5t146 32.5q70 0 130 -25.5t105 -70.5t70.5 -105.5t25.5 -127.5q0 -63 -21.5 -118.5t-59 -98.5t-89 -73t-110.5 -38q22 -58 22 -121q0 -68 -26.5 -128t-71.5 -105 t-105.5 -72t-130.5 -27q-68 0 -128 27t-105 72t-70.5 105.5t-25.5 127.5q0 68 20 121q-59 8 -110.5 38t-88 73t-58 98t-21.5 119zM604 756q0 -39 14.5 -73t39 -58.5t58.5 -39t72 -14.5q78 0 130.5 53.5t52.5 131.5q0 76 -52.5 129t-130.5 53q-39 0 -72.5 -14.5t-58 -39 t-39 -58t-14.5 -70.5zM862 307q0 -76 53.5 -129t128.5 -53q78 0 132.5 53t54.5 129q0 78 -54.5 131.5t-132.5 53.5q-76 0 -129 -53.5t-53 -131.5zM1118 748q4 -74 57.5 -125.5t129.5 -51.5q78 0 131 53.5t53 131.5q0 76 -53.5 129t-130.5 53q-76 0 -129.5 -51t-57.5 -125v-6 v-8z" /><glyph unicode="M" d="M457 756q0 68 26.5 128t71.5 105t105 70.5t128 25.5q78 0 144.5 -32.5t114.5 -89.5q47 57 113.5 89.5t144.5 32.5q70 0 130 -25.5t105 -70.5t70.5 -105.5t25.5 -127.5q0 -61 -21.5 -117.5t-58 -99.5t-88 -73t-110.5 -40q20 -53 20 -119q0 -68 -26.5 -128t-71.5 -105 t-105.5 -72t-127.5 -27q-70 0 -130.5 27t-105.5 72t-70.5 105.5t-25.5 127.5q0 31 5 60.5t15 58.5q-59 10 -109.5 40t-88 73t-59 99t-21.5 118zM604 756q0 -78 53.5 -131.5t130.5 -53.5q45 0 86 23l74 -127q-41 -29 -63.5 -71t-22.5 -89q0 -76 53.5 -129t131.5 -53t131 53 t53 129q0 47 -22.5 89t-63.5 71l69 127q49 -23 91 -23q78 0 131 53.5t53 131.5q0 76 -53.5 129t-130.5 53q-78 0 -131.5 -53.5t-53.5 -128.5h-147q0 76 -54.5 129t-130.5 53q-78 0 -131 -53.5t-53 -128.5z" /><glyph unicode="N" d="M293 981q0 61 23.5 116.5t64.5 95.5t96 63.5t117 23.5q63 0 117.5 -23.5t95.5 -63.5t64.5 -95.5t23.5 -116.5q0 -63 -23.5 -117.5t-64.5 -95.5t-95.5 -64.5t-117.5 -23.5q-61 0 -116.5 23.5t-96.5 64.5t-64.5 95.5t-23.5 117.5zM440 981q0 -66 45 -110t109 -44 q66 0 108.5 44t42.5 110q0 63 -43 107.5t-108 44.5q-63 0 -108.5 -44.5t-45.5 -107.5zM741 197q0 61 23.5 116.5t65.5 96.5t97.5 64.5t116.5 23.5t115.5 -23.5t96.5 -64.5t66 -96.5t24 -116.5q0 -63 -24 -117.5t-66 -95.5t-96 -64.5t-116 -23.5q-61 0 -116.5 23.5 t-97.5 64.5t-65.5 95t-23.5 118zM891 197q0 -66 45 -110t108 -44t107.5 44t44.5 110q0 63 -44 107t-108 44q-63 0 -108 -44t-45 -107zM1196 981q0 61 23.5 116.5t64.5 95.5t95.5 63.5t117.5 23.5q61 0 116.5 -23.5t96.5 -63.5t64.5 -95.5t23.5 -116.5q0 -63 -23.5 -117.5 t-64.5 -95.5t-96 -64.5t-117 -23.5q-63 0 -117.5 23.5t-95.5 64.5t-64.5 95.5t-23.5 117.5zM1346 981q0 -66 43 -110t108 -44q63 0 108.5 44t45.5 110q0 63 -45 107.5t-109 44.5q-66 0 -108.5 -44.5t-42.5 -107.5z" /><glyph unicode="O" horiz-adv-x="1615" d="M0 530q0 217 82 407.5t224.5 333t332.5 224.5t408 82q119 0 233.5 -27.5t218 -79t193.5 -121t161 -155.5q72 86 162 155.5t193.5 121t217 79t234.5 27.5q217 0 407.5 -82t333 -224.5t224.5 -332.5t82 -408q0 -217 -82 -407.5t-224.5 -332.5t-333 -224t-407.5 -82 q-121 0 -234.5 27.5t-217 78t-193.5 121t-162 156.5q-72 -86 -161.5 -156.5t-193 -121t-218.5 -78t-233 -27.5q-217 0 -407.5 82t-333 224t-224.5 332.5t-82 407.5zM147 530q0 -186 71 -350t192.5 -285.5t285.5 -192.5t351 -71q117 0 229.5 35t215.5 99.5t195.5 155.5 t165.5 202q74 -111 165 -202t195.5 -155.5t217.5 -99.5t229 -35q186 0 349 71t285 192.5t192.5 285.5t70.5 350t-70.5 349t-192.5 285t-285 192.5t-349 70.5q-117 0 -229.5 -34.5t-217 -98t-195.5 -154.5t-165 -202q-74 111 -166 202t-195 154.5t-216 98t-229 34.5 q-186 0 -350.5 -70.5t-286 -192.5t-192.5 -284.5t-71 -349.5zM492 530q0 115 43 216.5t118.5 176t177 118.5t216.5 44t216 -44t175.5 -118.5t119 -176t44.5 -216.5t-44.5 -216t-119 -176t-176 -119t-215.5 -44q-115 0 -216.5 44t-177 119t-118.5 176t-43 216zM639 530 q0 -84 31.5 -157.5t87 -129t130.5 -87t159 -31.5t157.5 31.5t129 87t87 129t31.5 157.5t-31.5 159t-87 130t-129.5 87t-157 32q-84 0 -159 -32t-130.5 -87t-87 -130t-31.5 -159z" /><glyph unicode="P" d="M514 186v688h1063v-688h-1063z" /><glyph unicode="Q" d="M700 0v1063h691v-1063h-691z" /><glyph unicode="R" d="M514 186v688h1063v-688h-1063zM664 494h766v233h-766v-233z" /><glyph unicode="S" d="M631 287v147h829v-147h-829zM631 686v150h829v-150h-829z" /><glyph unicode="T" d="M631 487v148h829v-148h-829z" /><glyph unicode="U" horiz-adv-x="8095" d="M0 487v148h8096v-148h-8096z" /><glyph unicode="V" horiz-adv-x="8095" d="M-74 555h148q0 -90 33.5 -168t92 -136t137.5 -92t169 -34q88 0 167 34t138 92t93 136t34 168q0 121 46 226.5t125 184t184.5 125t226.5 46.5q119 0 224 -46.5t184 -125t124 -184t45 -226.5q0 -90 34 -168t93.5 -136t137 -92t167.5 -34t169 34t137.5 92t92 136t33.5 168 q0 121 46.5 226.5t125 184t184 125t226.5 46.5q119 0 225.5 -46.5t184 -125t124 -184t46.5 -226.5q0 -90 33.5 -168t92 -136t136.5 -92t168 -34t169 34t138 92t93 136t34 168q0 121 46 226.5t124 184t183.5 125t225.5 46.5q121 0 226.5 -46.5t184.5 -125t124 -184t45 -226.5 q0 -90 34 -168t92 -136t136 -92t168 -34t169 34t138.5 92t93 136t33.5 168q0 121 46 226.5t125 184t184.5 125t226.5 46.5q119 0 224.5 -46.5t184 -125t123.5 -184t45 -226.5h-147q0 90 -34 169t-92 137.5t-137 92t-167 33.5q-90 0 -169 -33.5t-138.5 -92t-93 -137.5 t-33.5 -169q0 -121 -46 -226.5t-125 -183t-184.5 -124t-226.5 -46.5t-225.5 46.5t-183 124t-123.5 183t-45 226.5q0 90 -34 169t-93.5 137.5t-138 92t-167.5 33.5q-90 0 -167.5 -33.5t-137 -92t-93.5 -137.5t-34 -169q0 -121 -46 -226.5t-125 -183t-184 -124t-226 -46.5 t-226.5 46.5t-183.5 124t-124 183t-46 226.5q0 90 -33.5 169t-92 137.5t-137.5 92t-167 33.5q-90 0 -169 -33.5t-137.5 -92t-92 -137.5t-33.5 -169q0 -121 -46 -226.5t-125 -183t-184.5 -124t-226.5 -46.5t-226 46.5t-183 124t-124 183t-46 226.5q0 90 -34 169t-92.5 137.5 t-137 92t-166.5 33.5q-90 0 -169 -33.5t-138.5 -92t-93.5 -137.5t-34 -169q0 -121 -46 -226.5t-123.5 -183t-184 -124t-225.5 -46.5q-121 0 -226.5 46.5t-184.5 124t-124 183t-45 226.5z" /><glyph unicode="W" horiz-adv-x="8095" d="M0 -25v150q193 0 364 73.5t299 200.5t201.5 298t73.5 366h147q0 -195 74 -366t201 -298t298 -200.5t365 -73.5q195 0 366 73.5t298 200.5t200.5 298t73.5 366h150q0 -195 73.5 -366t200.5 -298t297 -200.5t365 -73.5t365.5 73.5t298.5 200.5t202 298t74 366h147 q0 -195 74 -366t201 -298t298 -200.5t365 -73.5q193 0 364 73.5t299 200.5t201.5 298t73.5 366h148q0 -195 73.5 -366t201.5 -298t298 -200.5t365 -73.5v-150q-172 0 -330 51.5t-290 143.5t-232.5 219t-159.5 279q-59 -152 -159.5 -279t-232.5 -219t-290 -143.5t-330 -51.5 t-329.5 51.5t-289.5 143.5t-232.5 219t-159.5 279q-59 -152 -161 -279t-234 -219t-289.5 -143.5t-329.5 -51.5t-329 51.5t-289 142.5t-233 218t-161 279q-59 -152 -159.5 -279t-232.5 -218t-290 -142.5t-330 -51.5t-329.5 51.5t-289.5 143.5t-232.5 219t-159.5 279 q-59 -152 -159.5 -279t-233 -219t-290 -143.5t-329.5 -51.5z" /><glyph unicode="X" horiz-adv-x="8095" d="M0 938v147q172 0 329.5 -51t290 -143t232.5 -219t160 -279q59 152 159.5 279t232.5 219t289.5 143t329.5 51t330 -51t290 -142t232.5 -218t159.5 -279q59 152 160.5 279t233.5 218t290 142t330 51t329.5 -51t290 -143t232.5 -219t160 -279q59 152 159.5 279t232.5 219 t289.5 143t329.5 51t330 -51t290 -142t232.5 -218t159.5 -279q59 152 160.5 279t233.5 218t289 142t329 51v-147q-195 0 -365 -73.5t-297 -200.5t-200.5 -298t-73.5 -366h-150q0 195 -73.5 366t-200.5 298t-298 200.5t-366 73.5t-365.5 -73.5t-297.5 -200.5t-201 -298 t-74 -366h-147q0 195 -74 366t-202 298t-299 200.5t-363 73.5q-195 0 -366 -73.5t-298 -200.5t-200.5 -298t-73.5 -366h-150q0 195 -73.5 366t-200.5 298t-298 200.5t-366 73.5t-364.5 -73.5t-297.5 -200.5t-202 -298t-74 -366h-147q0 195 -73.5 366t-201.5 298t-299 200.5 t-364 73.5z" /><glyph unicode="Y" horiz-adv-x="8095" d="M-51 582l561 561l1010 -1014l1011 1012l1012 -1012l1012 1012l1011 -1012l1012 1012l1012 -1012l455 453l102 -103l-557 -555l-1012 1012l-1012 -1012l-1011 1012l-1012 -1012l-1012 1012l-1015 -1016l-1010 1014l-455 -455z" /><glyph unicode="a" d="M514 457v147h459v459h147v-459h457v-147h-457v-457h-147v457h-459z" /><glyph unicode="b" d="M492 530q0 115 43 216.5t118.5 176t177 118.5t216.5 44t216 -44t175.5 -118.5t119 -176t44.5 -216.5t-44.5 -216t-119 -176t-176 -119t-215.5 -44q-115 0 -216.5 44t-177 119t-118.5 176t-43 216zM639 530q0 -84 31.5 -157.5t87 -129t130.5 -87t159 -31.5t157.5 31.5 t129 87t87 129t31.5 157.5t-31.5 159t-87 130t-129.5 87t-157 32q-84 0 -159 -32t-130.5 -87t-87 -130t-31.5 -159zM752 530q0 59 23.5 112.5t63.5 93.5t93 62.5t115 22.5q59 0 112 -22.5t93 -62.5t62.5 -93.5t22.5 -112.5q0 -61 -22.5 -114.5t-62.5 -93t-93 -62t-112 -22.5 q-61 0 -114.5 22.5t-93.5 62t-63.5 93t-23.5 114.5zM899 530q0 -59 44 -102t104 -43q59 0 101 43t42 102t-42 101.5t-101 42.5t-103.5 -42t-44.5 -102z" /><glyph unicode="c" d="M442 53l1106 1061l103 -106l-1106 -1061z" /><glyph unicode="d" d="M598 530q0 92 35 174t96.5 143.5t143 96.5t174.5 35q92 0 172.5 -35t142 -96.5t96.5 -143t35 -174.5q0 -92 -35 -173t-96.5 -142t-142 -96t-172.5 -35t-174 35t-143.5 96t-96.5 142t-35 173z" /><glyph unicode="e" d="M514 0v1063h1063v-1063h-1063zM664 147h766v766h-766v-766z" /><glyph unicode="f" d="M399 37l648 1122l645 -1122l-127 -74l-518 899l-521 -899z" /><glyph unicode="g" d="M399 1024l127 76l521 -901l518 901l127 -76l-645 -1122z" /><glyph unicode="h" d="M492 530q0 115 43 216.5t118.5 176t177 118.5t216.5 44t216 -44t175.5 -118.5t119 -176t44.5 -216.5t-44.5 -216t-119 -176t-176 -119t-215.5 -44q-115 0 -216.5 44t-177 119t-118.5 176t-43 216zM639 530q0 -84 31.5 -157.5t87 -129t130.5 -87t159 -31.5t157.5 31.5 t129 87t87 129t31.5 157.5t-31.5 159t-87 130t-129.5 87t-157 32q-84 0 -159 -32t-130.5 -87t-87 -130t-31.5 -159z" /><glyph unicode="i" d="M514 186v688h1063v-688h-1063z" /><glyph unicode="j" d="M514 186v688h1063v-688h-1063zM664 334h766v393h-766v-393z" /><glyph unicode="k" d="M250 571l389 387l102 -106l-206 -207h1021l-206 207l102 106l389 -387l-389 -389l-102 105l211 211h-1031l211 -211l-102 -105z" /><glyph unicode="l" d="M432 530l305 533h617l305 -533l-305 -530h-617zM602 530q57 -102 107.5 -190t111.5 -193h449q57 100 107 189.5t112 193.5q-59 100 -108.5 190.5t-110.5 194.5h-449q-29 -51 -55.5 -98t-53 -93t-53 -94t-57.5 -100zM903 530q0 59 41 102.5t100 43.5t101.5 -43.5 t42.5 -102.5t-42 -99t-102 -40q-59 0 -100 40t-41 99z" /><glyph unicode="m" d="M549 330l348 200l-348 201l74 129l350 -203v406h147v-404l348 201l74 -129l-348 -201l348 -200l-74 -129l-348 200v-401h-147v401l-350 -200z" /><glyph unicode="n" d="M332 29q74 170 153 329q33 68 71 140.5t77 143.5t79 135.5t79 115.5q86 113 199.5 170t234.5 57q119 0 230.5 -57t191.5 -174l114 -172l-125 -80q-18 31 -39 59q-16 25 -36.5 53.5t-36.5 55.5q-55 86 -134 127t-165 41t-170 -44t-148 -124q-55 -76 -114 -180h417v-148 h-497q-51 -94 -94.5 -184t-77.5 -162q-39 -84 -74 -160z" /><glyph unicode="o" d="M377 0l541 938h-404v147h404l-199 345l127 73l201 -346v2l198 344l127 -73l-198 -345h403v-147h-403l540 -938h-1337zM633 147h825l-411 715z" /><glyph unicode="p" d="M377 0l670 1159l667 -1159h-1337z" /><glyph unicode="q" d="M377 0l670 1159l667 -1159h-1337zM633 147h825l-411 715zM905 383q0 59 41 102t101 43q59 0 101 -43t42 -102t-42 -99t-101 -40t-100.5 40t-41.5 99z" /><glyph unicode="r" d="M598 1034l137 56l121 -306h379l121 306l137 -56l-446 -1132zM913 637l134 -332l131 332h-265z" /><glyph unicode="s" d="M492 530q0 115 43 216.5t118.5 176t177 118.5t216.5 44t216 -44t175.5 -118.5t119 -176t44.5 -216.5t-44.5 -216t-119 -176t-176 -119t-215.5 -44q-115 0 -216.5 44t-177 119t-118.5 176t-43 216zM639 530q0 -84 31.5 -157.5t87 -129t130.5 -87t159 -31.5t157.5 31.5 t129 87t87 129t31.5 157.5t-31.5 159t-87 130t-129.5 87t-157 32q-84 0 -159 -32t-130.5 -87t-87 -130t-31.5 -159zM752 530q0 59 23.5 112.5t63.5 93.5t93 62.5t115 22.5q59 0 112 -22.5t93 -62.5t62.5 -93.5t22.5 -112.5q0 -61 -22.5 -114.5t-62.5 -93t-93 -62t-112 -22.5 q-61 0 -114.5 22.5t-93.5 62t-63.5 93t-23.5 114.5zM899 530q0 -59 44 -102t104 -43q59 0 101 43t42 102t-42 101.5t-101 42.5t-103.5 -42t-44.5 -102z" /><glyph unicode="t" d="M391 106l1106 1061l102 -106l-153 -148q72 -76 114 -174t42 -209q0 -115 -44.5 -216t-119 -176t-176 -119t-215.5 -44q-117 0 -220.5 46.5t-181.5 123.5l-151 -145zM754 248q55 -59 131 -91t162 -32q84 0 157.5 31.5t129 87t87 129t31.5 157.5q0 82 -29.5 154t-83.5 127 l-82 -80q37 -41 58.5 -91t21.5 -110q0 -61 -22.5 -114.5t-62.5 -93t-93 -62t-112 -22.5q-63 0 -116.5 23.5t-94.5 64.5zM942 428q43 -43 105 -43q59 0 101 43t42 102q0 29 -11.5 53.5t-29.5 45.5z" /><glyph unicode="u" d="M442 53l1106 1061l103 -106l-1106 -1061z" /><glyph unicode="v" d="M442 53q59 55 127 123q59 57 136 130t163 157l103 -107q-86 -84 -163 -156.5t-136 -129.5q-68 -68 -127 -123zM1120 705q59 55 127 122q59 57 137 130t164 157l103 -106q-86 -84 -164 -157t-137 -130q-68 -68 -127 -123z" /><glyph unicode="w" d="M854 530q0 39 15.5 74t41 60.5t60.5 41t73 15.5q39 0 74 -15.5t61.5 -41t41 -60.5t14.5 -74t-14.5 -73.5t-41 -60t-61.5 -41t-74 -15.5t-73.5 15.5t-60 41t-41 60.5t-15.5 73z" /><glyph unicode="x" d="M598 530q0 92 35 174t96.5 143.5t143 96.5t174.5 35q92 0 172.5 -35t142 -96.5t96.5 -143t35 -174.5q0 -92 -35 -173t-96.5 -142t-142 -96t-172.5 -35t-174 35t-143.5 96t-96.5 142t-35 173z" /><glyph unicode="y" d="M442 53l234 226q-37 53 -57.5 116t-20.5 135q0 92 35 174t96.5 143.5t143 96.5t174.5 35q76 0 143 -23.5t123 -66.5l235 225l103 -106l-236 -226q37 -53 57.5 -117.5t20.5 -134.5q0 -92 -35 -173t-96.5 -142t-142 -96t-172.5 -35q-76 0 -143.5 23.5t-125.5 64.5 l-233 -225z" /><glyph unicode="z" d="M598 424v639h895v-639q0 -92 -35 -174t-96.5 -143.5t-143 -96.5t-174.5 -35q-92 0 -172.5 35t-142 96.5t-96.5 143.5t-35 174zM745 438q2 59 27 111.5t65 91.5t93 61.5t114 22.5t117 -23.5t96.5 -63.5t64.5 -95.5t24 -116.5v487h-601v-475z" /><glyph unicode="&#x2000;" horiz-adv-x="788" /><glyph unicode="&#x2001;" horiz-adv-x="1577" /><glyph unicode="&#x2002;" horiz-adv-x="788" /><glyph unicode="&#x2003;" horiz-adv-x="1577" /><glyph unicode="&#x2004;" horiz-adv-x="525" /><glyph unicode="&#x2005;" horiz-adv-x="394" /><glyph unicode="&#x2006;" horiz-adv-x="262" /><glyph unicode="&#x2007;" horiz-adv-x="262" /><glyph unicode="&#x2008;" horiz-adv-x="197" /><glyph unicode="&#x2009;" horiz-adv-x="315" /><glyph unicode="&#x200a;" horiz-adv-x="87" /><glyph unicode="&#x202f;" horiz-adv-x="315" /><glyph unicode="&#x205f;" horiz-adv-x="394" /><glyph unicode="&#x25fc;" horiz-adv-x="983" d="M0 0v983h983v-983h-983z" /></font>';

}(niviz));
