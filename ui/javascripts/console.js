/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

(function (angular) {
  'use strict';

  angular
    .module('niviz.console', [
      'niviz.lib',
      'ui.bootstrap.alert'
    ])

    .factory('$console', [
      'sprintf',

      function (sprintf) {
        var alerts = [];

        function console(type, message) {
          return alerts.unshift({
            type: type, message: sprintf.apply(null, message)
          });
        }

        console.log = function () {
          return console('warning', arguments);
        };

        console.info = function () {
          return console('info', arguments);
        };

        console.error = function () {
          return console('danger', arguments);
        };

        console.success = function () {
          return console('success', arguments);
        };

        console.dismiss = function (index) {
          alerts.splice(index, 1);
          return this;
        };

        console.alerts = alerts;

        return console;
      }
    ])

    .directive('console', [
      '$console',

      function ($console) {
        return {
          restrict: 'A',
          templateUrl: 'console.html',

          link: function (scope) {
            scope.alerts  = $console.alerts;
            scope.dismiss = $console.dismiss;
          }
        };
      }
    ]);

}(angular));
