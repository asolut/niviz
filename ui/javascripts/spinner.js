/*
* niViz -- snow profiles visualization
* Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

(function (angular, Spinner, $) {
  'use strict';

  angular
    .module('niviz.spinner', []).

    factory('$spinner', [
      function () {
        var extend = angular.extend,

          defaults = {
            lines: 11, // The number of lines to draw
            length: 8, // The length of each line
            width: 3, // The line thickness
            radius: 7, // The radius of the inner circle
            corners: 0, // Corner roundness (0..1)
            direction: 1, // 1: clockwise, -1: counterclockwise
            rotate: 0, // The rotation offset
            color: '#a0a0a0', // #rgb or #rrggbb
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '50%', // Top position relative to parent in px
            left: '50%' // Left position relative to parent in px
          };

        return function $spinner(element) {
          var el = $(element), width = el.width() / 2, height = el.height() / 2,
              options = {
                left: (el.offset().left + width) + 'px',
                top: (el.offset().top + height) + 'px'
              };

          return new Spinner(extend({}, defaults, options));
        };
      }
    ]).

    directive('spinner', [
      '$spinner',
      '$timeout',

      function ($spinner, $timeout) {
        return {
          restrict: 'A',

          link: function (scope, element, attributes) {
            var spinner = $spinner(element), giveup, target = element[0];

            function stop() {
              spinner && spinner.stop();
            }

            scope.$watch(attributes.spinner, function (condition) {
              $timeout.cancel(giveup);

              if (!condition) return stop();
              spinner = $spinner(element);
              spinner.spin(target);

              // hide spinner after max 2 minutes
              giveup = $timeout(stop, 1000 * 60 * 2);
            });

            scope.$on('$destroy', function () {
              $timeout.cancel(giveup);

              stop();
              spinner = element = target = null;
            });
          }
        };
      }
    ]);

}(angular, Spinner, jQuery));
