/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (angular, $, niviz) {
  'use strict';

  var forEach = angular.forEach;
  var t       = niviz.Translate.gettext;
  var round   = niviz.util.round;

  angular
    .module('niviz.lib', [ 'niviz.settings' ])

    .value('niviz', niviz)

    .value('assert', niviz.util.assert)
    .value('sprintf', niviz.util.format)
    .value('zip', niviz.util.zip);

  angular
    .module('niviz.util', []).

  factory('open', [
    function () {

      function extname(filename) {
        var idx = filename.lastIndexOf('.');
        return (idx) ? filename.substr(idx + 1) : null;
      }

      function loadfile (file, callback) {
        var reader = new FileReader();

        reader.addEventListener('load', function () {
          var type = extname(file.name) || file.type;
          var result = reader.result;

          if (type === 'gz') {
            result = pako.inflate(reader.result, { to: 'string' });
            type = extname(file.name.replace('.' + type, ''));
          }

          niviz.parse(result, type, function (error, station) {
            if (!error) station.filename = file.name;
            callback(error, station);
          });
        });

        // Gzipped files are parsed into an ArrayBuffer
        if ((extname(file.name) || file.type) === 'gz') {
          reader.readAsArrayBuffer(file);
        } else {
          reader.readAsText(file, 'utf-8');
        }
      }

      return {
        loadfile: loadfile,
        extname: extname
      };
    }
  ]).

  factory('writecsv', [
    'save',
    function (save) {

      var round = niviz.util.round;

      function header (station, profile, feature) {
        var buffer = '';
        // Example header:
        // ; Location: Versuchsfeld Weissfluhjoch
        // ; Observer: Neige Calonne; Judith ter Schure.
        // ; Date: 2017-02-01
        // ; HS: 145 cm
        // ; Top is top of cutter
        // ; Comments: warm and wet condition for measurements.
        // ;
        // ; Top	bottom	Density
        // ; (cm)	(cm)	(kg m-3)

        var location = station.name || (station.position && station.position.description);
        var observer = profile.meta && profile.meta.observer;
        var date     = profile.date && profile.date.format('YYYY-MM-DD HH:mm');
        var hs       = profile.meta && profile.meta.hs;
        var fraction = feature.info && feature.info.fractiontype || '';
        var impurity = feature.info && feature.info.impuritytype || '';
        var method   = feature.info && feature.info.method || '';
        var comments = feature.info && feature.info.comment || '';

        if (location) buffer += '; Location: ' + location + '\n';
        if (observer) buffer += '; Observer: ' + observer + '\n';
        if (date) buffer += '; Date: ' + date + '\n';
        if (hs) buffer += '; HS: ' + hs + ' cm\n';
        if (fraction) buffer += '; Fraction type: ' + fraction + '\n';
        if (impurity) buffer += '; Impurity type: ' + impurity + '\n';
        if (method) buffer += '; Method: ' + method + '\n';
        if (comments) buffer += '; Comment: ' + comments + '\n';

        if (feature.info && !feature.info.pointprofile) {
          buffer += ';\n; top\tbottom\t' + feature.type + '\n';
          buffer += '; cm\tcm\t' + feature.unit + '\n';
        } else {
          buffer += ';\n; depth\t' + feature.type + '\n';
          buffer += '; cm\t' + feature.unit + '\n';
        }

        return buffer;
      }

      function values (feature) {
        // e. g.
        // 145	142	95
        // 142	139	92
        // 139	136	107
        // ...
        // 7	4	348
        // 4	0	342

        var buffer = '', i;

        if (!feature.info.pointprofile) {
          for (var i = feature.layers.length - 1; i >= 0; --i) {
            var l = feature.layers[i];
            buffer += round(l.top, 2) + '\t\t' + round(l.bottom, 2) + '\t\t' + l.value + '\n'
          }
        } else if (feature.profile.top || feature.profile.top === 0) {
          for (var i = feature.layers.length - 1; i >= 0; --i) {
            var l = feature.layers[i];
            buffer += round(feature.profile.top - l.top, 2) + '\t\t' + l.value + '\n'
          }
        }

        return buffer;
      }

      function writefile (station, profile, feature) {
        var buffer = header(station, profile, feature);
        buffer += values(feature);

        var name = profile.date && profile.date.format('YYYYMMDDHHmm')
          + '.' + feature.type + '.csv';

        save(name, 'csv', buffer);
      }

      return {
        writefile: writefile
      };
    }
  ]).

  factory('readcsv', [
    function () {

      function loadfile (file, callback) {
        var reader = new FileReader();

        reader.addEventListener('load', function () {

          var lines = reader.result.split('\n'), tmp, validlines, result, ii,
              pointprofile = false, max = 0, floatvals, comment = '',
              fraction = '', impurity = '', method = '';

          validlines = lines.filter(function(line) {
            tmp = line.trim();

            if (tmp.indexOf(' Fraction type: ') === 1) fraction = tmp.substring(17);
            else if (tmp.indexOf(' Impurity type: ') === 1) impurity = tmp.substring(17);
            else if (tmp.indexOf(' Comment: ') === 1) comment = tmp.substring(11);
            else if (tmp.indexOf(' Method: ') === 1) method = tmp.substring(10);

            return tmp.length && tmp[0] !== '#' && tmp[0] !== ';';
          });

          result = validlines.map(function (array) {
            array = array.trim();
            tmp = array.split(/[,;\s]+/);

            if (tmp.length === 3) {
              ii = tmp[2];
              tmp[2] = tmp[1];
              tmp[1] = ii;
            } else if (tmp.length === 2) {
              pointprofile = true;
            } else {
              throw Error('CSV file has invalid format');
            }

            floatvals = tmp.map(function (e) { return round(parseFloat(e), 6); });
            if (floatvals[0] > max) max = floatvals[0];
            return floatvals;
          });

          var obj = {
            pointprofile: pointprofile,
            max: max,
            result: result
          };

          if (comment) obj.comment = comment;
          if (method) obj.method = method;
          if (impurity) obj.impuritytype = impurity;
          if (fraction) obj.fractiontype = fraction;

          callback(obj);
        });

        reader.readAsText(file, 'utf-8');
      }

      return {
        loadfile: loadfile
      };
    }
  ]).

  factory('save', [
    'save-as-modal',

    function (sam) {
      return function save(name, type, data) {
        sam(name).then(function (filename) {
          if (!filename) return;

          var blob = new Blob([data], { type: type });

          if (navigator.msSaveBlob) {
            navigator.msSaveBlob(blob, filename);

          } else {
            var a = document.createElement('a');

            a.download = filename;
            a.type = type;
            a.href = (URL || window.webkitURL).createObjectURL(blob);

            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
          }
        });
      };
    }
  ]).

  factory('geolocation', [
    '$http',
    '$timeout',

    function ($http, $timeout) {
      var pos = {}, elevator;

      $timeout(function () {
        elevator = new google.maps.ElevationService;
      }, 1000);

      function getLocationElevation(location) {
        elevator.getElevationForLocations({
          'locations': [location]
        }, function(results, status) {
          if (status === 'OK' && results[0]) {
            pos.altitude = Math.round(results[0].elevation);
          } else {
            console.log('Error while retrieving elevation');
            pos.altitude = 1000; // default to an altitude of 1000
          }
        });
      }

      function geo_success (position) {
        if (position.coords.latitude)
          pos.latitude = position.coords.latitude;

        if (position.coords.longitude)
          pos.longitude = position.coords.longitude;

        if (position.coords.altitude)
          pos.altitude = position.coords.altitude;
        else {
          getLocationElevation({lat: pos.latitude, lng: pos.longitude});
        }
      }

      function geo_error (e) {
        console.log(e.message);
      }

      var geo_options = {
        enableHighAccuracy: true,
        maximumAge        : 30000,
        timeout           : 27000
      };

      var getPosition = function (position, ms) {
        pos = position;

        ms = ms || 2000;

        navigator.geolocation.getCurrentPosition(geo_success);

        var wid = navigator.geolocation.watchPosition(geo_success, geo_error, geo_options);

        return $timeout(function() {
          navigator.geolocation.clearWatch(wid);
          return pos;
        }, ms);
      };

      return {
        getPosition: getPosition
      };
    }
  ]).

  service('registry', function () {
    var graphs = {};

    this.add = function (hash, graph) {
      graphs[hash] = graph;
    };

    this.remove = function (hash) {
      this.desynchronize(hash);
      delete graphs[hash];
    };

    this.desynchronize = function (hash) {
      forEach(graphs, function (g, key) {
        if (key !== hash) g.desynchronize(graphs[hash]);
      });
    };

    this.desynchronizeAll = function () {
      forEach(graphs, function (graph, key) {
        forEach(graphs, function (g, k) {
          if (k !== key) {
            g.desynchronize(graph);
          }
        });
      });
    };

    this.synchronize = function () {
      forEach(graphs, function (graph, key) {
        forEach(graphs, function (g, k) {
          if (k !== key) {
            //g.desynchronize(graph);
            g.synchronize(graph);
          }
        });
      });
    };
  }).

  factory('print', [
    function () {
      return function (element, title) {
        var body = $('body').detach(), doctitle = document.title;
        document.body = document.createElement('body');
        if (title) document.title = title;

        var clone = element.clone();
        clone.appendTo($('body'));
        clone.show(); // In case the content was hidden

        window.print();

        document.title = doctitle;
        $('html body').remove();
        body.appendTo($('html'));
      };
    }
  ]).

  directive('help', function() {
    return {
      restrict: 'C',
      link: function(scope, elem, attrs) {
        var modal = document.getElementById('imagemodal');
        var modalImg = document.getElementById('img01');
        var captionText = document.getElementById('caption');

        scope.imgpop = function (imgsrc, imgalt) {
          imgalt = imgalt || '';

          modal.style.display = 'block';
          modalImg.src = imgsrc;
          captionText.innerHTML = imgalt;
        };

        scope.closepopup = function () {
          modal.style.display = 'none';
        };
      }
    };
  }).

  directive('biggerThan', function() {
    return {
      require: 'ngModel',
      scope: {
        otherModelValue: '=biggerThan'
      },
      link: function(scope, elem, attrs, ngModel) {
        ngModel.$validators.biggerThan = function(modelValue) {
          if (modelValue === undefined || modelValue === null || modelValue === '')
            return true;
          return modelValue >= scope.otherModelValue;
        };

        scope.$watch('otherModelValue', function() {
          ngModel.$validate();
        });
      }
    };
  }).

  directive('leqThan', function() {
    return {
      require: 'ngModel',
      scope: {
        otherModelValue: '=leqThan'
      },
      link: function(scope, elem, attrs, ngModel) {
        ngModel.$validators.leqThan = function(modelValue) {
          if (modelValue === undefined || modelValue === null) return true;
          return modelValue <= scope.otherModelValue;
        };

        scope.$watch('otherModelValue', function() {
          ngModel.$validate();
        });
      }
    };
  }).

  // Translation filter, uses niviz.Translate.gettext
  filter('t', [
    function () {
      return function (name, lang) {
        return t(name, 'ui');
      };
    }
  ]);

}(angular, jQuery, niviz));
