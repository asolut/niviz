/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//= require es5-shim
//= require jquery
//= require moment
//= require angular
//= require angular-ui-router.min
//= require angular-animate
//= require snap.svg-min.js
//= require canvg.bundle
//= require spin
//= require pako

//= require ui-bootstrap-tpls
//= require bootstrap-colorpicker-module
//= require select
//= require angularjs-dropdown-multiselect.min
//= require angular-drag-and-drop-lists

//= require niviz

//= require lib
//= require console
//= require menu
//= require controller
//= require info
//= require settings
//= require create
//= require visualizer
//= require profiler
//= require profile-editor
//= require timeliner
//= require meteographer
//= require spinner

(function (angular) {
  'use strict';

  function valToString (val) {
    return val != null ? val.toString() : val;
  }

  function valFromString (val) {
    return val != null ? val.toString() : val;
  }

  function regexpMatches (val) {
    return this.pattern.test(val);
  }

  angular
    .module('niviz', [
      'ngAnimate',
      'ui.router',
      'ui.bootstrap',
      'colorpicker.module',
      'angularjs-dropdown-multiselect',
      'dndLists',

      'niviz.util',
      'niviz.lib',
      'niviz.console',
      'niviz.menu',
      'niviz.controller',
      'niviz.info',
      'niviz.settings',
      'niviz.create',
      'niviz.visualizer',
      'niviz.profiler',
      'niviz.profileeditor',
      'niviz.timeliner',
      'niviz.meteographer',
      'niviz.spinner'
    ])

    .config([
      '$locationProvider',
      '$stateProvider',
      '$urlRouterProvider',
      '$urlMatcherFactoryProvider',
      '$uibTooltipProvider',

      function ($locationProvider, $stateProvider, $urlRouterProvider,
                $urlMatcherFactory, $uibTooltipProvider) {
        var protocol = window.location.protocol;

        // Globally disable tooltip texts
        if (!niviz.Common.defaults.show_tooltips)
          $uibTooltipProvider.options({
            trigger: 'dontTrigger'
          });

        $locationProvider.html5Mode({
          enabled: (protocol !== 'file:'), requireBase: false
        });

        $urlMatcherFactory.type("MyType", {
          encode: valToString,
          decode: valFromString
          // is: regexpMatches
          // pattern: /[^/]+\/[^/]+/
        });

        $urlRouterProvider.otherwise('/');

        var mainState = {
          name: 'main',
          url: '/?{file:MyType}',
          templateUrl: 'main.html',
          reloadOnSearch : true
        }

        var mainState2 = {
          name: 'main2',
          url: '/index.html?{file:MyType}',
          templateUrl: 'main.html',
          reloadOnSearch : true
        }

        var mainState3 = {
          name: 'full',
          url: '/full.html?{file:MyType}',
          templateUrl: 'main.html',
          reloadOnSearch : true
        }

        var simpleState = {
          name: 'simple',
          url: '/simple.html?{file:MyType}',
          templateUrl: 'simple.html',
          reloadOnSearch : true
        }

        var helpState = {
          name: 'help',
          url: '/help',
          templateUrl: 'help.html'
        }

        var contentHelpState = {
          name: 'help.contents',
          url: '/contents',
          templateUrl: 'contents.html'
        }

        var programmerHelpState = {
          name: 'help.programmer',
          url: '/programmer',
          templateUrl: 'help-programmer.html'
        }

        var userHelpState = {
          name: 'help.user',
          url: '/user',
          templateUrl: 'help-user.html'
        }

        $stateProvider.state('main', mainState);
        $stateProvider.state('main2', mainState2);
        $stateProvider.state('full', mainState3);
        $stateProvider.state('simple', simpleState);
        $stateProvider.state('help', helpState);
        $stateProvider.state('help.contents', contentHelpState);
        $stateProvider.state('help.user', userHelpState);
        $stateProvider.state('help.programmer', programmerHelpState);
      }
    ]);

}(angular));
