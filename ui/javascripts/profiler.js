/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (angular, $) {
  'use strict';

  angular
    .module('niviz.profiler', [
      'niviz.lib',
      'niviz.util'
    ])

    .directive('profiler', [
      'niviz',
      'print',

      function (niviz, print) {
        return {
          restrict: 'A',
          templateUrl: 'profiler.html',
          transclude: true,
          scope: {
            profile: '=',
            close: '&onClose',
            miniature: '=',
            width: '=',
            lang: '='
          },
          link: function (scope, element, attrs) {
            window.console.dir('Setting up profiler controller');
            var W = $(window);
            var height = W.innerHeight() - $('.navbar').height() - 80;
            var width  = W.width();
            var maxwidth = scope.width || 900;
            var canvas = $('#canvas', element);
            element = $(element);
            var gtype = niviz.Common.defaults.preferred_profile_graph;
            console.dir('Profile max-width: ' + scope.width);

            scope.view = function (pgraph, props) {
              var properties = $.extend({
                miniature: scope.miniature || false
              }, props);

              if (scope.graph) scope.graph.destroy();
              canvas.empty();
              gtype = pgraph;

              canvas.height(height);
              canvas.width(Math.min(Math.max(width, maxwidth), maxwidth));
              scope.graph = niviz.draw(pgraph, scope.station, canvas, properties);

              element.width(canvas.width() + 20);
              element.height(canvas.height() + 50);
            };

            scope.print = function () {
              print(canvas, scope.profile && scope.profile.filename || null);
            };

            scope.save = function (type) {
              var name = scope.graph.profile.date.format('YYYYMMDDHHmm.') + gtype;
              scope.$parent.save(type, scope.graph, name);
            };

            function resize (mini) {
              if (!scope.station) return;
              var newwidth = Math.min(Math.max(width, maxwidth), maxwidth);
              console.dir('resizing profiler h: ' + height + ' w: ' + newwidth);
              element.height(height);
              element.width(newwidth);

              canvas.height(height);
              canvas.width(newwidth);

              if (scope.graph) scope.graph.destroy();

              if (scope.$parent.tmax) { // only draw when can be seen

                scope.graph = null;
                return;
              }

              //setTimeout(function () { // HACK: otherwise wrong dimensions are passed to graph
                //if (!scope.graph) {
                scope.graph = niviz.draw(gtype, scope.station, canvas, {
                  miniature: scope.miniature || mini || false
                });
                // } else {
                //   var properties = {
                //     miniature: scope.miniature || false
                //   };
                //   scope.graph.setProperties(properties);
                //   //scope.graph.draw();
                // }

                element.width(canvas.width() + 20);
                element.height(canvas.height() + 50);
              //}, 10);

              if (scope.$parent.resize) scope.$parent.resize();
            }

            //W.on('resize', resize);

            scope.$on('settings', function (e, c) {
              resize();
            });

            scope.$on('$destroy', function () {
              console.dir('destruction');
              element.remove();
              //W.off('resize', resize);
            });

            scope.$watch('width', function (w) {
              maxwidth = w || 900;

              // if (scope.pmax && !scope.miniature) gtype = 'SLFProfile';
              // else gtype = 'SimpleProfile';

              if (scope.miniature) gtype = 'SimpleProfile';

              resize(scope.$parent.pmax ? 'true' : 'false');
            });

            scope.$watch('$parent.height', function (h) {
              if (h && h > 60) {
                height = h - 60;
                resize();
              }
            });

            scope.$watch('profile', function (station) {
              if (station) {
                scope.station = station;
                resize();
              }
            });
          }
        };
      }
    ]);

}(angular, jQuery));
