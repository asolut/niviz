/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (angular, $) {
  'use strict';

  function storeLocally (station) {
    if (!localStorage || !station || !station.creationdate) return;
    station.lastedited = moment();

    var fileStore = JSON.parse(localStorage.getItem('fileStore')) || {};
    var uniqueID = station.creationdate.format('YYYYMMDDHHmmssSSS');

    fileStore[uniqueID] = station;
    localStorage.setItem('fileStore', JSON.stringify(fileStore));
  }

  function correct (to, parameter, i, l) {

    if (parameter === 'ct') {
      to[i].value = { value: l.value };
      to[i].value.nofailure = l.nofailure;
      to[i].value.comment = l.comment;
      to[i].value.character = l.character;

    } else if (parameter === 'ect') {
      to[i].value = { value: l.value && l.numeric };
      to[i].value.nofailure = l.nofailure;
      to[i].value.comment = l.comment;
      to[i].value.character = l.character;
      to[i].value.swiss = l.swisscode;

    } else if (parameter === 'rb') {
      to[i].value = { value: l.value };
      to[i].value.character = l.character;
      to[i].value.releasetype = l.releasetype;
      to[i].value.nofailure = l.nofailure;
      to[i].value.comment = l.comment;

    } else if (parameter === 'saw') {
      to[i].value = { value: l.value };
      to[i].value.cutlength = l.cutlength;
      to[i].value.columnlength = l.columnlength;
      to[i].value.comment = l.comment;

    } else if (parameter === 'sf') {
      to[i].value = { value: l.value };
      to[i].value.character = l.character;
      to[i].value.nofailure = l.nofailure;
      to[i].value.comment = l.comment;

    } else if (parameter === 'ramm') {
      to[i].value = { value: l.value };
      to[i].value.weightHammer = l.weightHammer;
      to[i].value.weightTube = l.weightTube;
      to[i].value.nDrops = l.nDrops;
      to[i].value.dropHeight = l.dropHeight;
      to[i].value.depth = l.depth;
      to[i].value.height = l.height;
      to[i].simple = l.simple;

    } else if (parameter === 'smp') {
      to[i].value = { value: l.value };
      to[i].value.depth = l.depth;

    }
    //else if (parameter === 'wetness') {
    //  to[i].value = l.code;
    //}

  }

  function clone (parameter, from, to, profile) {
    if (!from || !from.layers) return;

    from.layers.forEach(function (l, i) {
      to.push({
        top: l.top,
        topcopy: l.top,
        bottom: l.bottom,
        index: i,
        value: l.value,
        comment: l.comment || ''
      });

      // if (l.top !== undefined)
      //   to[i].depthtop = round(scope.profile.hs - l.top, 5);

      // if (l.bottom !== undefined)
      //   to[i].thickness = l.top - l.bottom;

      correct(to, parameter, i, l);
    });
  }

  function cloneextral (profile, to, type, i, extrai) {
    var l;

    l = profile[type].elements[extrai].layers[i];

    to[i].top = l.top;
    to[i].topcopy = l.top;
    to[i].bottom = l.bottom;
    to[i].value = l.value;

    correct(to, type, i, l);
  }

  function clonel (type, i, profile, clones) {
    var l = profile[type].layers[i];

    clones[i].top = l.top;
    clones[i].topcopy = l.top;
    clones[i].bottom = l.bottom;
    clones[i].value = l.value;

    correct(clones, type, i, l);
  }

  function addlinkfunction ($timeout) {
    return function (scope, element, attrs) {
      scope.clear = function (i, obj) {
        scope.$parent.clear(i, obj);

        $timeout(function () {
          if (obj)
            element[0].getElementsByClassName('bottom-value')[0].focus();
          else
            element[0].getElementsByClassName('top-value')[0].focus();
        });
      };

      // Initially focus on top-value
      $timeout(function () {
        if (scope.layer && scope.layer[scope.$index].top)
          element[0].getElementsByClassName('bottom-value')[0].focus();
        else
          element[0].getElementsByClassName('top-value')[0].focus();
      });
    }
  }

  // Check whether layers within one profile p overlap
  function check_overlap (p) {
    var i, ii = p.length, j, top = 0, bottom = 0;

    for (i = 0; i < ii; ++i) {
      p[i].overlapping = false;

      for (j = i + 1; j < ii; ++j) {

        if ((p[i].top || p[i].top === 0) && p[i].top === p[i].bottom) { // bottom === top
          if (p[i].top === p[j].top && p[i].bottom === p[j].bottom)
            p[i].overlapping = true;
        } else if ((p[i].top || p[i].top === 0) && // there is a top and bottom value
                   (p[i].bottom ||  p[i].bottom === 0)) {
          if (p[i].top > p[j].bottom || p[i].top > p[j].top) {
            p[i].overlapping = true;
          }
        } else if (p[i].top || p[i].top === 0) { // no bottom value
          if (p[i].top === p[j].top) p[i].overlapping = true;
        }
      }
    }
  }

  angular
    .module('niviz.profileeditor', [
      'niviz.lib',
      'niviz.util',
      'niviz.console'
    ]).

  directive('profileEditor', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'A',
        templateUrl: 'profile-editor.html',
        transclude: true,
        scope: {
          profile: '=',
          close: '&onClose',
          width: '=',
          lang: '='
        },
        link: function (scope, element, attrs) {
          var stbtests = ['ct', 'ect', 'rb', 'saw', 'sf'];

          window.console.log('Setting up profile-editor controller');

          scope.niviz = niviz;
          scope.views = [
            {id: 1, name: '_pe_metadata'},
            {id: 2, name: '_pe_stratprofile'},
            {id: 3, name: '_pe_tempprofile', display: 'temperature'},
            {id: 4, name: '_pe_densityprofile', display: 'density'},
            {id: 5, name: '_pe_stabilityprofile'},
            {id: 6, name: '_pe_hardnessprofile', display: 'ramm'},
            {id: 7, name: '_pe_lwcprofile', display: 'wetness'},
            {id: 8, name: '_pe_ssaprofile', display: 'ssa'},
            {id: 9, name: '_pe_impurityprofile', display: 'impurity'}
          ];

          scope.view = scope.views[0];

          niviz.parse(JSON.stringify(scope.profile), 'json', function (error, station) {
            var index = scope.$parent.$parent.$index;

            scope.station = station;
            scope.current = scope.$parent.$parent.profiles[index].profiles[0];

            $timeout(scope.clone_all);
          });

          scope.setView = function (i) {
            scope.view = scope.views[i];
          };

          scope.repaint = function () {
            $timeout(function () {
              scope.profile.emitter.emit('profile', scope.current, true);
              //scope.profile.emitter.emit('parameter', scope.view.display);
            });
          };

          scope.$watch('$parent.$parent.gotlayer', function () {
            if (scope.$parent.view)
              scope.view = scope.views[scope.$parent.view - 1];
          });

          scope.$watch('view', function (view) {
            if (!view || !view.display) return;
            scope.profile.emitter.emit('parameter', view.display || 'temperature');
          });

        }
      };
    }
  ]).

  directive('editMetadata', [
    'niviz',
    'geolocation',
    '$timeout',

    function (niviz, geolocation, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'edit-metadata.html',
        scope: {
          profile: '=',
          station: '=',
          callback: '=',
          control: '='
        },
        link: function (scope, element, attrs) {
          var zoomfactor = 12, round = niviz.util.round, t = niviz.Translate.gettext;

          scope.niviz = niviz;
          scope.meta = {};
          scope.pm = {};
          scope.blockgeo = false;
          scope.status = {
            open: [true, false, false, false, false, false]
          };

          scope.togglePane = function (e, index) {
            e.preventDefault();
            e.stopPropagation();
            scope.status.open[index] = !scope.status.open[index];
          };

          scope.getPosition = function (form) {
            scope.blockgeo = true;
            if (!navigator.geolocation) {
              scope.blockgeo = false;
              alert(niviz.Translate.gettext('_pe_alert_geolocation', 'ui'));
              return;

            } else {
              navigator.geolocation.getCurrentPosition(function (pos) {
                var position = {};
                zoomfactor = 12;

                geolocation.getPosition(position).then(function (result) {
                  scope.meta.position.latitude = result.latitude;
                  scope.meta.position.longitude = result.longitude;
                  scope.meta.position.altitude = result.altitude;

                  scope.blockgeo = false;
                  scope.zoom(0);

                  form.$setDirty();
                });
              }, function (e) {
                scope.blockgeo = false;
                alert(niviz.Translate.gettext('_pe_alert_geolocation', 'ui') + ': '
                      + e.message);
              });
            }
          };

          scope.zoom = function (inc) {
            if ((inc < 0 && zoomfactor > 4) || (inc > 0 && zoomfactor < 16) || !inc)
              zoomfactor = zoomfactor + inc;

            scope.imagesrc = scope.niviz.Position.imagesrc(scope.meta.position, zoomfactor);
          };

          scope.clonemeta = function () {
            scope.meta = scope.station.meta;
            scope.pm = scope.profile.meta;

            scope.status.timezone = scope.pm.date.utcOffset();
            var year = scope.pm.date.year(), month = scope.pm.date.month(),
              day = scope.pm.date.date(), hour = scope.pm.date.hour(),
              min = scope.pm.date.minute();

            scope.status.datepicked = new Date(year, month, day, hour, min);
            scope.status.timepicked = new Date(year, month, day, hour, min);

            // Using km/h instead of the internal m/s
            if (scope.pm.wind && (scope.pm.wind.speed || scope.pm.wind.speed === 0)) {
              scope.status.windspeed = round(scope.pm.wind.speed * 3.6, 2);
              scope.computeWindSpeed(scope.status.windspeed);
            }

            // Setting wind direction angle/dir, if present
            if (scope.pm.wind && scope.pm.wind.dir
                && (!scope.pm.wind.angle || scope.pm.wind.angle === 0)) {
              scope.dir_to_angle();
            } else if (scope.pm.wind && !scope.pm.wind.dir
                       && (scope.pm.wind.angle || scope.pm.wind.angle === 0)) {
              scope.angle_to_dir();
            }
          };

          /*
           * Save the edited meta data and set the form to pristine state
           * @param {FormController} mf
           */
          scope.savemeta = function (mf) {
            delete scope.meta.position.x;
            delete scope.meta.position.y;

            var tmpdate = moment(scope.status.timepicked);
            scope.pm.date = moment(scope.status.datepicked);
            scope.pm.date.utcOffset(scope.status.timezone);
            scope.pm.date.add(-scope.status.timezone, 'minutes');
            scope.pm.date.hours(tmpdate.hour());
            scope.pm.date.minutes(tmpdate.minute());

            scope.station.meta = scope.meta;
            scope.profile.meta = scope.pm;

            scope.profile.adjust();

            scope.clonemeta();
            if (scope.callback) scope.callback();
            scope.repaint();

            if (mf) mf.$setPristine();
            storeLocally(scope.station);

            checkSnowHeight();
          };

          function checkSnowHeight () {
            console.dir(scope.profile);
            if (scope.pm.hs || scope.pm.hs === 0) {
              if (scope.profile.height > scope.pm.hs)
                alert(t('_pe_meta_hs_alert', 'ui'));
            }
          }

          if (scope.control) scope.control.savemeta = scope.savemeta;

          // -- begin -- datepicker options
          scope.picker = {opened: false};
          scope.today = function() {
            scope.pm.date = new Date();
          };

          scope.clear = function () {
            scope.pm.date = null;
          };

          scope.open = function($event) {
            scope.picker.opened = true;
          };

          scope.dateOptions = {
            formatYear: 'yyyy',
            maxDate: new Date(2030, 11, 8),
            minDate: new Date(0, 12, 24)
          };

          scope.formats = ['yyyy-MM-dd'];
          scope.format = scope.formats[0];
          // -- end -- datepicker options

          scope.dir_to_angle = function () {
            scope.pm.wind.angle = niviz.Position.dir_to_angle(scope.pm.wind.dir);
          };

          scope.angle_to_dir = function () {
            if (!scope.pm.wind.angle && scope.pm.wind.angle !== 0) {
              scope.pm.wind.dir = 'n/a';
            } else {
              scope.pm.wind.dir = niviz.Position.angle_to_dir(scope.pm.wind.angle);
            }
          };

          scope.computeWindSpeed = function (value) {
            if (value === null || value === undefined) {
              scope.pm.wind.estimation = null;
              scope.pm.wind.speed = null;

            } else if (typeof value === 'number') {
              scope.pm.wind.speed = value / 3.6;
              scope.pm.wind.estimation = scope.niviz.Profile.convert_windspeed(value / 3.6);

            } else {
              if (scope.pm.wind.estimation.symbol === 'X')
                scope.pm.wind.speed = 20;
              else
                scope.pm.wind.speed = round((scope.pm.wind.estimation.min
                                             + scope.pm.wind.estimation.max) / 2, 3);

              scope.status.windspeed = round(3.6 * scope.pm.wind.speed, 2);
            }
          };

          scope.repaint = function () {
            $timeout(function () {
              scope.station.emitter.emit('profile', scope.profile, true);
            });
          };

          scope.$watchCollection('meta.position', function (profile) {
            var pos = scope.meta.position;

            if (!pos || pos.latitude > 90 || pos.latitude < -90
                || pos.longitude > 180 || pos.longitude < -180) {
              scope.imagesrc = null;
              return;
            }

            scope.imagesrc = scope.niviz.Position.imagesrc(scope.meta.position, zoomfactor);
          });

          scope.$watch('profile', function (profile) {
            profile && scope.clonemeta();
          });
        }
      };
    }
  ]).

  directive('addStratigraphy', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-stratigraphy.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editStratprofile', [
    'niviz',
    '$timeout',
    'insertLayerModal',

    function (niviz, $timeout, insertLayerModal) {
      return {
        restrict: 'E',
        templateUrl: 'edit-stratprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '='
        },
        link: function (scope, element, attrs) {
          scope.niviz = niviz;
          scope.cloneSP = [];
          scope.newlayer = {};

          var type = '_stratigraphic';

          function clonelayer (layer, i) {
            var l = scope.profile.wetness.layers[i];

            layer.wetness = l.code;
            layer.hardness = scope.profile.hardness.layers[i].caamlcode;
            layer.top = l.top;
            layer.topcopy = l.top;
            layer.bottom = l.bottom;
            layer.grainshape = scope.profile.grainshape.layers[i].code;

            layer.grainform = {
              primary: { id: scope.profile.grainshape.layers[i].value.primary || ''},
              secondary: {id: scope.profile.grainshape.layers[i].value.secondary || ''}
            };

            if (layer.grainform.primary.id) layer.grainform.primary.key =
              niviz.Value.GrainShape.type[layer.grainform.primary.id].key;
            if (layer.grainform.secondary.id) layer.grainform.secondary.key =
              niviz.Value.GrainShape.type[layer.grainform.secondary.id].key;

            var tmp = scope.profile.grainsize.layers[i];
            layer.grainsize = {
              avg: tmp.value && tmp.value.avg || '',
              max: tmp.value && tmp.value.max || ''
            };

            layer.index = i;

            layer.comments = scope.profile.comments.layers[i].value;
          }

          scope.clear = function (layer) {
            scope.newlayer = {
              wetness: null, hardness: null, top: layer && layer.bottom || null,
              bottom: null, grainform: { primary: null, secondary: null },
              grainsize: { avg: null, max: null }
            };

            $timeout(function () {
              if (element[0].getElementsByClassName('bottom-value').length
                  && scope.newlayer.top) {
                element[0].getElementsByClassName('bottom-value')[0].focus();
              }
            });

          };
          scope.clear();
          scope.isopened = [];

          scope.clone_stratigraphic_profile = function () {
            scope.cloneSP = [];

            if (!scope.profile.wetness || !scope.profile.wetness.elements[0]
                || !scope.profile.wetness.layers) return;

            while (scope.isopened.length < scope.profile.wetness.layers.length)
              scope.isopened.push(false);

            scope.isopened.length = scope.profile.wetness.layers.length;

            scope.profile.wetness.layers.forEach(function (l, i) {
              var tmp = {};
              clonelayer(tmp, i);
              scope.cloneSP.push(tmp);
            });

            scope.assess(scope.cloneSP);
            showlater && showlayer();
          };

          scope.assess = function () {
            check_overlap(scope.cloneSP);
          };

          scope.addlayer = function (layer) {
            console.dir('Parameter adding: ' + type);
            layer.grainshape = {
              primary: layer.grainform.primary && layer.grainform.primary.id || null,
              secondary: (layer.grainform.primary && layer.grainform.primary.id
                          && layer.grainform.secondary
                          && layer.grainform.secondary.id) || null
            };

            if (!layer.grainshape.primary) layer.grainshape = null;

            scope.profile.addlayer(type, layer);
            scope.clone_stratigraphic_profile();

            console.dir(scope.profile);
            scope.clear(layer);

            scope.assess();
            storeLocally(scope.station);
            scope.callback && scope.callback();
          };

          scope.remove = function (index) {
            console.dir('Removing stratigraphic layer: ' + index);
            scope.profile.rmlayer(type, index);
            scope.isopened.splice(scope.isopened.length - index - 1, 1);

            if (!scope.profile.hardness && scope.profile.wetness
                && scope.profile.wetness.length >= 1) {
              scope.profile.wetness.elements.unshift(null);
              delete scope.profile.wetness.elements[0];
            }
            scope.clone_stratigraphic_profile();

            console.dir(scope.profile);
            scope.assess();
            storeLocally(scope.station);
            scope.callback && scope.callback();
          };

          scope.cancel = function (layer) {
            clonelayer(layer, layer.index);
          };

          scope.save = function (layer) {
            var reset = false, oldtop;

            oldtop = scope.profile.wetness.layers[layer.index].top;
            reset = (layer.top !== oldtop);

            scope.profile.editlayer('comments', layer.index, { value: layer.comments });
            scope.profile.editlayer('wetness', layer.index, { code: layer.wetness });
            scope.profile.editlayer('hardness', layer.index, { code: layer.hardness });

            scope.profile.editlayer('grainshape', layer.index, { value: {
              primary: layer.grainform.primary && layer.grainform.primary.id || null,
              secondary: (layer.grainform.primary && layer.grainform.primary.id
                          && layer.grainform.secondary
                          && layer.grainform.secondary.id) || null
            } });

            scope.profile.editlayer('grainsize', layer.index, { value: {
              avg: layer.grainsize.avg || null,
              max: (layer.grainsize.avg && layer.grainsize.max) || null
            } });

            scope.profile.editlayer(type, layer.index, {
              top: layer.top,
              bottom: layer.bottom
            });

            if (reset) scope.clone_stratigraphic_profile();
            else clonelayer(layer, layer.index);

            console.dir(scope.profile);
            scope.assess();
            storeLocally(scope.station);
            scope.callback && scope.callback();
          };

          var showlater = false, buffer;
          function showlayer (layernr) {
            if (showlater) layernr = buffer;
            showlater = false;

            var index = scope.cloneSP.length - layernr - 1;
            if (index < 0) {
              showlater = true;
              buffer = layernr;
              return;
            }

            scope.cloneSP.forEach(function (e, i) {
              scope.isopened[i] = (i === index);
            });

            $timeout(function () {
              document.getElementById('stratlayer' + index).scrollIntoView();
            }, 500); // give it some time
          }

          /* Insert layer logic */
          scope.insert = function (lindex) {
            var clayer = JSON.parse(JSON.stringify(scope.cloneSP[lindex])),
                layer = JSON.parse(JSON.stringify(scope.cloneSP[lindex])),
                top = clayer.top, bottom = clayer.bottom, open = lindex;

            insertLayerModal(clayer).then(function (d) {
              if (!d) return;

              if (layer.top > d.top && layer.bottom < d.bottom) {
                // Insert layer in between, split original layer in two
                layer.bottom = d.top;
                scope.save(layer);
                scope.addlayer(clayer);

                clayer.top = clayer.bottom;
                clayer.bottom = bottom;
                scope.addlayer(clayer);
                open++;
              } else if (layer.top === d.top && d.bottom > layer.bottom) {
                layer.top = d.bottom;
                scope.save(layer);
                scope.addlayer(clayer);
                open++;
              } else if (layer.bottom === d.bottom && d.top < layer.top) {
                layer.bottom = d.top;
                scope.save(layer);
                scope.addlayer(clayer);
              }

              scope.clone_stratigraphic_profile();
              showlayer(open);
            });
          };

          scope.$watch('$parent.$parent.$parent.gotlayer', function () {
            var layer = scope.$parent.$parent.$parent.layer;
            if (!layer || (!layer.index && layer.index !== 0)) return;
            showlayer(layer.index);
          });

          scope.$watch('profile', function (profile) {
            profile && scope.clone_stratigraphic_profile();
          });
        }
      };
    }
  ]).

  // The modal for inserting a layer into an existing one
  factory('insertLayerModal', [
    '$uibModal',
    '$q',

    function (modal, q) {
      return function (properties) {

        return modal
          .open({
            animation: true,
            templateUrl: 'edit-insert-layer.html',
            controller: 'InsertLayerController',
            size: 'md',
            windowClass: 'modal-insert-layer',
            resolve: {
              data: function () { return properties; }
            }
          })
          .result
          .catch(function (/* reason */) {
            // cancel btn pressed
          });
      };
    }
  ]).

  controller('InsertLayerController', [
    '$scope',
    '$uibModalInstance',
    'data',

    function (scope, modal, data) {
      scope.layer = { min: data.bottom, max: data.top };
      scope.data = data;

      scope.save = function () {
        modal.close(scope.data);
      };

      scope.cancel = function () {
        modal.dismiss('cancel');
      };
    }
  ]).

  factory('editMultipleProfile', [
    'niviz',
    'readcsv',
    'writecsv',
    '$console',
    '$timeout',
    'insertLayerModal',

    function (niviz, readcsv, writecsv, $console, $timeout, insertLayerModal) {
      function template (scope) {

        scope.niviz = niviz;

        scope.offset = 0;
        scope.layer = [];
        scope.startcollapsed = false;

        scope.status = {
          iscollapsed: [],
          ismetaopen: []
        };

        scope.meta = [];

        scope.profiletypes = [
          { name: '_pe_layer_profile', value: false },
          { name: '_pe_point_profile', value: true }
        ];

        scope.assess = function () {
          scope[scope.varname].forEach(function (p) {
            check_overlap(p);
          });
        };

        scope.transform = function () {
          // Override this function to transform cloned values
        };

        scope.cloneall = function () {
          scope[scope.varname].length = 0;
          scope.layer.length = 0;
          scope.status.iscollapsed.length = 0;
          scope.status.ismetaopen.length = 0;
          scope.meta.length = 0;

          scope.profile[scope.type]
            && scope.profile[scope.type].elements.forEach(function (t, i) {
            if (i < scope.offset) return;

            scope[scope.varname][i - scope.offset] = [];
            scope.layer.push(JSON.parse(JSON.stringify(scope.init)));
            scope.status.iscollapsed.push(scope.startcollapsed);
            scope.status.ismetaopen.push(false);
            scope.meta.push(t.meta);
            clone(scope.type, t, scope[scope.varname][i - scope.offset]);
            scope.assess();

            scope.transform(i - scope.offset);
          });
        }

        scope.addprofile = function () {
          if (scope[scope.varname].length &&
              !scope[scope.varname][scope[scope.varname].length - 1].length) return;

          scope[scope.varname].push([]);
          scope.layer.push(JSON.parse(JSON.stringify(scope.init)));

          //Collapse all profiles, except the newly added one
          scope.status.iscollapsed.forEach(function (c, i) {
            scope.status.iscollapsed[i] = true;
            scope.status.ismetaopen[i] = false;
          });
          scope.status.iscollapsed.push(false);
          scope.status.ismetaopen.push(false);

          scope.profile.addfeature(scope.type, scope[scope.varname].length - 1 + scope.offset);
          scope.meta[scope.meta.length] = scope.profile[scope.type]
            .elements[scope.profile[scope.type].elements.length - 1].meta;

          scope.station.updateFeatures();
          if (scope.callback) scope.callback();
        };

        var moved_to = null;
        scope.move = function (array, moved_from) {
          var index = moved_from;

          moved_to = moved_to + scope.offset || 0;
          moved_from = moved_from + scope.offset || 0;
          if (moved_to === null || moved_to === moved_from) return;
          //console.log('---Moving profile from ' + moved_from + ' to ' + moved_to);

          var element = null, meta = null, collapsed = null, metaopen = null;

          if (moved_from > moved_to) moved_from--;
          if (moved_from < moved_to) moved_to--;

          //console.log('   Moving profile from ' + moved_from + ' to ' + moved_to);

          element = scope.profile[scope.type].elements.splice(moved_from, 1);
          scope.profile[scope.type].elements.splice(moved_to, 0, element[0]);

          meta = scope.meta.splice(moved_from, 1);
          scope.meta.splice(moved_to, 0, meta[0]);

          collapsed = scope.status.iscollapsed.splice(moved_from, 1);
          scope.status.iscollapsed.splice(moved_to, 0, collapsed[0]);

          metaopen = scope.status.ismetaopen.splice(moved_from, 1);
          scope.status.ismetaopen.splice(moved_to, 0, metaopen[0]);

          array.splice(index, 1);
          // console.dir(scope.profile[scope.type].elements[0].layers[15].value)
          // console.dir(scope.profile[scope.type].elements[1].layers[15].value)
          // console.dir(scope.profile[scope.type].elements[2].layers[15].value)
          scope.station.updateFeatures();
          if (scope.callback) scope.callback();

          moved_to = null;
        };


        scope.moved = function (index) {
          moved_to = index;
        }

        scope.rmprofile = function (extrai) {
          var c = confirm(niviz.Translate.gettext('_pe_confirm_rm_profile', 'ui'));
          if (c === false) return;

          extrai = extrai || 0;

          if (scope.profile[scope.type] &&
              scope.profile[scope.type].elements.length > extrai + scope.offset)
            scope.profile.rmfeature(scope.type, extrai + scope.offset);

          scope[scope.varname].splice(extrai, 1);
          scope.layer.splice(extrai, 1);
          scope.status.iscollapsed.splice(extrai, 1);
          scope.status.ismetaopen.splice(extrai, 1);
          scope.meta.splice(extrai, 1);

          scope.station.updateFeatures();
          if (scope.callback) scope.callback();
        };

        scope.copystratigraphy = function (extrai, properties) {
          if (!scope.profile.grainshape || !scope.profile.grainshape.layers.length) return;
          if (scope[scope.varname][extrai].length) return;

          properties = properties || ['top', 'bottom']
          if (scope.profile[scope.type].elements[extrai + scope.offset].info.pointprofile)
            delete properties[properties.indexOf('bottom')];

          scope.profile.grainshape.layers.forEach(function (l, i) {
            var obj = { value: null, index:  i };

            properties.forEach(function (p) {
              obj[p] = l[p];
            });

            scope.profile.addlayer(scope.type, obj, extrai + scope.offset);
          });

          clone(scope.type, scope.profile[scope.type].elements[extrai + scope.offset],
                scope[scope.varname][extrai] = []);

          scope.transform(extrai - scope.offset);
          scope.assess();

          if (scope.callback) scope.callback();
        };

        scope.clear = function (i, obj) {
          scope.layer[i].top = obj && obj.bottom || null;
          scope.layer[i].bottom = null;
          scope.layer[i].value = null;
        };

        scope.addlayer = function (layer, extrai, layeroverride) {
          console.dir('Parameter adding: ' + scope.type + ' extrai: ' + extrai);
          var clayer;

          if (layeroverride) {
            clayer = layeroverride;
            console.dir(layeroverride);
          } else {
            clayer = { top: layer.top,
                       bottom: layer.bottom,
                       value: layer.value };
          }

          scope.profile.addlayer(scope.type, clayer, extrai + scope.offset);
          console.dir(scope.profile);

          clone(scope.type, scope.profile[scope.type].elements[extrai + scope.offset],
                scope[scope.varname][extrai] = []);

          scope.transform(extrai - scope.offset);
          scope.assess();

          if (scope.callback) scope.callback();

          scope.clear(extrai, clayer);
          storeLocally(scope.station);
        };

        scope.save = function (layer, extrai) {
          console.dir('Changing element nr: '  + extrai + ' layer: ' + layer.index);

          var reset = false, oldtop;
          var clayer = { top: layer.top,
                         bottom: layer.bottom,
                         value: layer.value};

          oldtop = scope.profile[scope.type]
            .elements[extrai + scope.offset].layers[layer.index].top;
          reset = (layer.top !== oldtop);

          scope.profile.editlayer(scope.type, layer.index, clayer, extrai + scope.offset);

          if (reset)
            clone(scope.type, scope.profile[scope.type].elements[extrai + scope.offset],
                  scope[scope.varname][extrai] = []);
          else
            cloneextral(scope.profile, scope[scope.varname][extrai],
                        scope.type, layer.index, extrai + scope.offset);

          console.dir(scope.profile);

          scope.transform(extrai - scope.offset);
          scope.assess();

          if (scope.callback) scope.callback();
          storeLocally(scope.station);
        };

        scope.cancel = function (layer, extrai) {
          cloneextral(scope.profile, scope[scope.varname][extrai],
                      scope.type, layer.index, extrai + scope.offset);

          scope.transform(extrai - scope.offset);
          scope.assess();
        };

        scope.remove = function (layeri, extrai) {
          console.dir('Removing element nr: '  + extrai + ' layer: ' + layeri);
          scope.profile.rmlayer(scope.type, layeri, extrai + scope.offset);

          if (scope.profile[scope.type]
              && scope.profile[scope.type].elements[extrai + scope.offset]) {
            clone(scope.type, scope.profile[scope.type].elements[extrai + scope.offset],
                  scope[scope.varname][extrai] = []);
            scope.clear(extrai);
            scope.transform(extrai - scope.offset);
            scope.assess();
          } else {
            scope[scope.varname].splice(extrai, 1);
            scope.layer.splice(extrai, 1);
            scope.status.iscollapsed.splice(extrai, 1);
            scope.status.ismetaopen.splice(extrai, 1);
            scope.meta.splice(extrai, 1);
          }

          console.dir(scope.profile);
          storeLocally(scope.station);
          if (scope.callback) scope.callback();
        };

        scope.togglemeta = function (e, i) {
          e.preventDefault();
          e.stopPropagation();
          scope.status.ismetaopen[i] = !scope.status.ismetaopen[i];
          if (scope.status.ismetaopen[i] && scope.status.iscollapsed[i])
            scope.status.iscollapsed[i] = false;
        };

        scope.cancelmeta = function (i) {
          scope.meta[i] = scope.profile[scope.type].elements[i + scope.offset].meta;
        };

        scope.savemeta = function (i) {
          scope.profile[scope.type].elements[i + scope.offset].meta = scope.meta[i];
          storeLocally(scope.station);
        };

        /* The csv import routines */
        var input = $('<input type="file"/>'), extrai;

        function convert_depth_to_top (result, max) {
          var i, ii = result.length, max = scope.profile.hs || max;
          for (i = 0; i < ii; ++i) {
            result[i][0] = max - Math.abs(result[i][0]);
            result[i].push(result[i][0]);
          }
        }

        function plausibilityCheck (result) {
          if (scope.type === 'temperature') {
            var max = result.result.reduce(function (acc, cur) {
              if (!acc) return cur[1];
              return Math.max(acc, cur[1]);
            }, null);

            if (max > 0) {
              $console.error('Temperatures may not be positive, found +' + max + ' °C');
              return false;
            }
          }

          return true;
        }

        input.on('change', function () {
          var self = this;

          if (this.files.length) {
            readcsv.loadfile(this.files[0], function (result) {
              self.value = null;
              var eindex = extrai + scope.offset;

              if (!plausibilityCheck(result)) return;

              if (result.pointprofile && scope.type !== 'smp') {
                scope.profile[scope.type].elements[eindex].info.pointprofile = true;
                scope.cancelmeta(extrai);
                convert_depth_to_top(result.result, result.max);
                scope.profile.add(scope.type, result.result, eindex);
              } else {
                scope.profile.add(scope.type, result.result, eindex);
              }

              ['comment', 'method', 'impuritytype', 'fractiontype'].forEach(function (t) {
                if (result[t])
                  scope.profile[scope.type].elements[eindex].info[t] = result[t];
              });

              scope.meta[eindex] = scope.profile[scope.type].elements[eindex].info;

              clone(scope.type, scope.profile[scope.type].elements[eindex],
                    scope[scope.varname][extrai] = []);

              console.dir(scope.profile);

              scope.transform(extrai);
              scope.assess();

              if (scope.callback) scope.callback();
              storeLocally(scope.station);
            });
          }
        });

        scope.importcsv = function (i) {
          extrai = i;
          input[0].click();
        };

        scope.exportcsv = function (i) {
          var feature = scope.profile[scope.type].elements[i + scope.offset];
          writecsv.writefile(scope.station, scope.profile, feature);
        }

        /* Insert layer logic */
        scope.insert = function (lindex, extrai) {
          var clayer = JSON.parse(JSON.stringify(scope[scope.varname][extrai][lindex])),
            layer = JSON.parse(JSON.stringify(scope[scope.varname][extrai][lindex])),
            top = clayer.top, bottom = clayer.bottom;

          ['topcopy', '$$hashKey', 'overlapping', 'index', 'overlapping']
            .forEach(function (key) {
              delete clayer[key];
              delete layer[key];
            });

          insertLayerModal(clayer).then(function (d) {
            if (!d) return;

            if (layer.top > d.top && layer.bottom < d.bottom) {
              // Insert layer in between, split original layer in two
              layer.bottom = d.top;
              scope.profile.editlayer(scope.type, lindex, layer, extrai + scope.offset);
              scope.profile.addlayer(scope.type, clayer, extrai + scope.offset);

              clayer.top = clayer.bottom;
              clayer.bottom = bottom;
              scope.profile.addlayer(scope.type, clayer, extrai + scope.offset);
            } else if (layer.top === d.top && d.bottom > layer.bottom) {
              layer.top = d.bottom;
              scope.profile.editlayer(scope.type, lindex, layer, extrai + scope.offset);
              scope.profile.addlayer(scope.type, clayer, extrai + scope.offset);
            } else if (layer.bottom === d.bottom && d.top < layer.top) {
              layer.bottom = d.top;
              scope.profile.editlayer(scope.type, lindex, layer, extrai + scope.offset);
              scope.profile.addlayer(scope.type, clayer, extrai + scope.offset);
            }

            clone(scope.type, scope.profile[scope.type].elements[extrai + scope.offset],
                    scope[scope.varname][extrai] = []);

            console.dir(scope.profile);

            scope.transform(extrai);
            scope.assess();
            storeLocally(scope.station);
            if (scope.callback) scope.callback();
          });
        };

        return {
          cloneall: scope.cloneall,
          remove: scope.remove,
          cancel: scope.cancel,
          save: scope.save,
          addlayer: scope.addlayer,
          clear: scope.clear,
          rmprofile: scope.rmprofile,
          addprofile: scope.addprofile,
          showmeta: scope.showmeta,
          importcsv: scope.importcsv,
          insert: scope.insert
        };
      }

      return template;
    }

  ]).

  directive('addTemp', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-temp.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editTempprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-tprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope);
          angular.extend(scope, f);

          scope[scope.varname] = [];
          scope.init = { top: null, value: null };
          scope.type = 'temperature';

          // Linking to methods of editMultipleProfile
          var addlayer = scope.addlayer, savelayer = scope.save;

          scope.addlayer = function (layer, extrai) {
            var clayer = {top: layer.top, bottom: layer.top,
                          value: -layer.value, index: layer.index};
            addlayer(clayer, extrai);
          };

          scope.save = function (layer, extrai) {
            var clayer = {top: layer.top, bottom: layer.top,
                          value: -layer.value, index: layer.index};
            savelayer(clayer, extrai);
          };

          scope.transform = function transform (index) {
            if (!scope[scope.varname][index]) return;

            scope[scope.varname][index].forEach(function (layer) {
              layer.value = Math.abs(layer.value);
            });
          };

          scope.clear = function (i, obj) {
            scope.layer[i].top = null;
            scope.layer[i].bottom = null;
            scope.layer[i].value = null;
          };

          scope.cloneall();
        }
      };
    }
  ]).

  directive('addLwc', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-lwc.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editLwcprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-lwcprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope);
          angular.extend(scope, f);

          scope.init = { top: null, bottom: null, value: null };
          scope.type = 'wetness';

          scope[scope.varname] = [];
          scope.offset = 1;

          scope.methods = [ 'Denoth Probe', 'Snow Fork', 'other' ];

          scope.cloneall();
        }
      };
    }
  ]).

  directive('addSsa', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-ssa.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editSsaprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-ssaprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope);
          angular.extend(scope, f);

          scope.init = { top: null, bottom: null, value: null };
          scope.type = 'ssa';

          scope.methods = [ 'Ice Cube', 'other' ];

          scope[scope.varname] = [];
          scope.offset = 1;

          scope.cloneall();
        }
      };
    }
  ]).

  directive('addDensity', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-density.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editDensityprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-densityprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope), round = niviz.util.round;
          angular.extend(scope, f);

          scope.init = { top: null, bottom: null, value: null };
          scope.type = 'density';

          scope[scope.varname] = [];
          scope.offset = 0;

          scope.methods = [ 'Snow Tube', 'Snow Cylinder', 'Snow Cutter',
                            'Denoth Probe', 'other' ];

          scope.clear = function (i, obj) {
            scope.layer[i].top = obj && obj.bottom || null;
            scope.layer[i].bottom = null;
            scope.layer[i].value = scope.layer[i].swe = null;
          };

          scope.transform = function transform (index) {
            if (!scope[scope.varname][index]) return;

            scope[scope.varname][index].forEach(function (layer) {
              layer.swe = round(((layer.top - layer.bottom) / 100) * layer.value, 8);
            });
          };

          scope.compute = function (layer, source) {
            var top = layer.top, bottom = layer.bottom;

            if ((top !== 0 && !top) || (bottom !== 0 && !bottom) || (top - bottom === 0)) {
              if (source === 'swe') layer.value = null;
              if (source === 'density') layer.swe = null;
              return;
            }

            // SWE [mm] = Snow Depth [m] * Density [kg / m³]
            if (source === 'swe') {
              if (layer.swe !== 0 && !layer.swe) layer.value = null;
              else layer.value = round(layer.swe / ((top - bottom) / 100), 8);
            } else if (source === 'density') {
              if (layer.value !== 0 && !layer.value) layer.swe = null;
              else layer.swe = round(((top - bottom) / 100) * layer.value, 8);
            } else {
              if (layer.value)
                layer.swe = round(((top - bottom) / 100) * layer.value, 8);
              else if (layer.swe)
                layer.value = round(layer.swe / ((top - bottom) / 100), 8);
            }
          };

          scope.cloneall();
        }
      };
    }
  ]).

  directive('addImpurity', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-impurity.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editImpurityprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-impurityprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope);
          angular.extend(scope, f);

          scope.init = { top: null, bottom: null, value: null };
          scope.type = 'impurity';

          scope.methods = [ 'other' ];
          scope.impuritytypes = [ 'Black Carbon', 'Dust', 'Isotopes' ];
          scope.fractiontypes = [ 'massFraction', 'volumeFraction' ];

          scope[scope.varname] = [];
          scope.offset = 0;

          scope.cloneall();
        }
      };
    }
  ]).

  directive('editStbprofiles', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'edit-stbprofiles.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '='
        }
      };
    }
  ]).

  directive('addCt', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-ct.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editCtprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-ctprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope), round = niviz.util.round;
          angular.extend(scope, f);

          scope.init = { top: null, bottom: null, value: null };
          scope.type = 'ct';

          scope[scope.varname] = [];
          scope.startcollapsed = true;
          scope.offset = 0;

          // Linking to methods of editMultipleProfile
          var addlayer = scope.addlayer;

          scope.addlayer = function (layer, extrai, nofailure) {
            if (nofailure) {
              addlayer(layer, extrai, { value: {
                nofailure: true,
                comment: layer.value && layer.value.comment || '',
                value: null }});

            } else {
              addlayer(layer, extrai);
            }
          };

          scope.cloneall();
        }
      };
    }
  ]).

  directive('addEct', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-ect.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editEctprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-ectprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope), round = niviz.util.round;
          angular.extend(scope, f);
          scope.swissregexp = /(\d{1,2}\/(np|\d{1,2})|np)/;

          scope.init = { top: null, bottom: null,
                         value: { character: 'P', value: null }};
          scope.type = 'ect';

          scope[scope.varname] = [];
          scope.startcollapsed = true;
          scope.offset = 0;

          scope.transform = function (index) {
            if (!scope[scope.varname][index]) return;

            if (niviz.Common.defaults.ECT_format !== 'Swiss') {
              scope[scope.varname][index].forEach(function (layer) {
                delete layer.value.swiss;
              });
            }
          };

          scope.clear = function (i, obj) {
            scope.layer[i].top = obj && obj.bottom || null;
            scope.layer[i].bottom = null;
            scope.layer[i].value = {
              character: 'P',
              value: null
            };
          };

          scope.setECTCharacter = function (layer, val, form) {
            if (val === 'P') {
              layer.value.character = 'P';
            } else if (val === 'PV') {
              layer.value.value = null;
              layer.value.character = 'PV';
            } else {
              layer.value.character = 'N';
            }
          };

          // Linking to methods of editMultipleProfile
          var addlayer = scope.addlayer;

          scope.addlayer = function (layer, extrai, nofailure) {
            if (nofailure) {
              addlayer(layer, extrai, { value: {
                nofailure: true,
                comment: layer.value && layer.value.comment || '',
                value: null }});

            } else {
              addlayer(layer, extrai);
            }
          };

          scope.cloneall();
        }
      };
    }
  ]).

  directive('addRb', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-rb.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editRbprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-rbprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope), round = niviz.util.round;
          angular.extend(scope, f);

          scope.init = { top: null, bottom: null, value: null };
          scope.type = 'rb';

          scope[scope.varname] = [];
          scope.startcollapsed = true;
          scope.offset = 0;

          // Linking to methods of editMultipleProfile
          var addlayer = scope.addlayer;

          scope.addlayer = function (layer, extrai, nofailure) {
            if (nofailure) {
              addlayer(layer, extrai, { value: {
                nofailure: true,
                comment: layer.value && layer.value.comment || '',
                value: null }});

            } else {
              addlayer(layer, extrai);
            }
          };

          scope.cloneall();
        }
      };
    }
  ]).

  directive('addSaw', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-saw.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editSawprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-sawprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope), round = niviz.util.round;
          angular.extend(scope, f);

          scope.init = { top: null, bottom: null, value: null };
          scope.type = 'saw';

          scope[scope.varname] = [];
          scope.startcollapsed = true;
          scope.offset = 0;

          // Linking to methods of editMultipleProfile
          var addlayer = scope.addlayer;

          scope.addlayer = function (layer, extrai, nofailure) {
            if (nofailure) {
              addlayer(layer, extrai, { value: {
                nofailure: true,
                comment: layer.value && layer.value.comment || '',
                value: null }});

            } else {
              addlayer(layer, extrai);
            }
          };

          scope.cloneall();
        }
      };
    }
  ]).

  directive('addSf', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-sf.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editSfprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-sfprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope), round = niviz.util.round;
          angular.extend(scope, f);

          scope.init = { top: null, bottom: null, value: null };
          scope.type = 'sf';

          scope[scope.varname] = [];
          scope.startcollapsed = true;
          scope.offset = 0;

          // Linking to methods of editMultipleProfile
          var addlayer = scope.addlayer;

          scope.addlayer = function (layer, extrai, nofailure) {
            if (nofailure) {
              addlayer(layer, extrai, { value: {
                nofailure: true,
                comment: layer.value && layer.value.comment || '',
                value: null }});

            } else {
              addlayer(layer, extrai);
            }
          };

          scope.cloneall();
        }
      };
    }
  ]).

  directive('editHardnessprofiles', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'edit-hardnessprofiles.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '='
        }
      };
    }
  ]).

  directive('addRam', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-ram.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editRamprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-ramprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope), round = niviz.util.round;
          angular.extend(scope, f);

          scope.init = { top: null, bottom: null, value: null };
          scope.type = 'ramm';

          scope[scope.varname] = [];
          scope.startcollapsed = true;
          scope.offset = 0;

          // Linking to methods of editMultipleProfile
          var addlayer = scope.addlayer;

          scope.clear = function (i, obj) {
            scope.layer[i].top = obj && obj.bottom || null;
            scope.layer[i].bottom = null;
            scope.layer[i].value = {
              weightHammer: obj && obj.value && obj.value.weightHammer || null,
              weightTube: obj && obj.value && obj.value.weightTube || null
            };
          };

          scope.adjust = function (layer, extrai) {
            var p = scope.profile.ramm.elements[extrai],
                length = p.layers && p.layers.length || 0;

            if (!layer.index && layer.index !== 0) {
              // Add layer
              if (scope[scope.varname][extrai].length) {
                layer.value.height =
                  layer.depthBottom - scope[scope.varname][extrai][0].depthBottom;
              } else {
                layer.value.height = layer.depthBottom;
              }
            } else if (length - 1 === layer.index) {
              // the top most layer
              layer.value.height = layer.depthBottom;
            } else {
              layer.value.height =
                layer.depthBottom - scope[scope.varname][extrai][layer.index + 1].depthBottom;
            }

            layer.value.depth = layer.depthBottom - layer.value.height;
          };

          scope.compute = function (layer, extrai) {
            scope.adjust(layer, extrai);
            layer.value.value = niviz.Value.Ramm.compute(layer.value);
          };

          // Linking to methods of editMultipleProfile
          var addlayer = scope.addlayer, savelayer = scope.save;

          scope.addlayer = function (layer, extrai, simple) {
            var clayer = JSON.parse(JSON.stringify(layer));

            if (simple) {
              clayer.value.height = layer.top - layer.bottom;
              clayer.value.depth = (scope.profile.top || 0) - layer.top;
              delete clayer.top;
              delete clayer.bottom;
              delete clayer.value.weightHammer;
              delete clayer.value.weightTube;
            } else {
              clayer.value.depth = layer.depthBottom - layer.value.height;
            }

            addlayer(clayer, extrai);
          };

          scope.save = function (layer, extrai) {
            var clayer = JSON.parse(JSON.stringify(layer));

            if (layer.simple) {
              clayer.value.height = layer.top - layer.bottom;
              clayer.value.depth = (scope.profile.top || 0) - layer.top;
            } else {
              clayer.value.depth = layer.depthBottom - layer.value.height;
            }

            savelayer(clayer, extrai);
          };

          scope.transform = function transform (index) {
            if (!scope[scope.varname][index]) return;

            scope[scope.varname][index].forEach(function (layer) {
              layer.depthBottom = Math.abs(layer.value.depth + layer.value.height);
            });
          };

          scope.cloneall();
        }
      };
    }
  ]).

  directive('addSmp', [
    'niviz',
    '$timeout',

    function (niviz, $timeout) {
      return {
        restrict: 'E',
        templateUrl: 'add-smp.html',
        link: addlinkfunction($timeout)
      }
    }
  ]).

  directive('editSmpprofile', [
    'niviz',
    '$timeout',
    'editMultipleProfile',

    function (niviz, $timeout, editMultipleProfile) {

      return {
        restrict: 'E',
        templateUrl: 'edit-smpprofile.html',
        scope: {
          station: '=',
          profile: '=',
          collapsible: '=',
          callback: '=',
          varname: '='
        },
        link: function (scope, element, attrs) {
          var f = editMultipleProfile(scope), round = niviz.util.round;
          angular.extend(scope, f);

          scope.init = { top: null, bottom: null, value: null };
          scope.type = 'smp';

          scope[scope.varname] = [];
          scope.startcollapsed = true;
          scope.offset = 0;

          scope.cloneall();
        }
      };
    }
  ]).

  directive('dndNodragMouseover', function(){
    return {
      restrict: 'A',
      require: 'dndNodragMouseover',
      controller: ['$element', function ( $element ) {

        this.ancestors = [];

        this.findDraggableAncestorsUntilDndDraggable = function ( h ) {
          var a = [];
          while ( h !== null ) {
            if ( h.attr('draggable') !== undefined ) {
              a.push({
                element : h,
                draggable : h.attr('draggable')
              });
            }
            if ( h.attr('dnd-draggable') !== undefined ) {
              break;
            }
            h = h.parent();
          }
          return a;
        };

        this.cleanup = function () {
          this.ancestors = [];
        };

        this.removeDraggable = function () {
          this.ancestors = this.findDraggableAncestorsUntilDndDraggable( $element );
          this.ancestors.forEach(function(o){
            o.element.attr('draggable', 'false');
          });
        };

        this.restoreDraggable = function () {
          this.ancestors.forEach(function(o){
            o.element.attr('draggable', o.draggable);
          });
        };

      }],
      link: function (scope, iElement, iAttrs, controller) {
        iElement.on('mouseover', function(event){
          controller.removeDraggable();
        });
        iElement.on('mouseout', function(event){
          controller.restoreDraggable();
        });
        scope.$on('$destroy', function(){
          iElement.off('mouseover');
          iElement.off('mouseout');
          controller.cleanup();
        });
      }
    };
  });

}(angular, jQuery));
