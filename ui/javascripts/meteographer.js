/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (angular, $, moment, niviz) {
  'use strict';

  var t = niviz.Translate.gettext;

  angular
    .module('niviz.meteographer', [
      'niviz.lib',
      'niviz.util'
    ])

    .controller('MeteoGrapherController', [
      '$scope',

      function($scope) {
        $scope.dropdownSettings = {
          scrollableHeight: '250px',
          scrollable: true,
          showCheckAll: false
        };

        $scope.example5customTexts = {
          buttonDefaultText: t('_dr_buttonDefaultText', 'ui'),
          dynamicButtonTextSuffix: t('_dr_dynamicButtonTextSuffix', 'ui'),
          checkAll: t('_dr_checkAll', 'ui'),
          uncheckAll: t('_dr_uncheckAll', 'ui')
        };

        $scope.dropdownEvents = {
          onSelectionChanged: function(item) {
            var value = $scope.set();
          }
        };
      }
    ])

    .directive('meteographer', [
      'niviz',
      'print',
      '$timeout',
      '$location',
      'registry',

      function (niviz, print, timeout, location, registry) {
        return {
          restrict: 'A',
          templateUrl: 'meteographer.html',
          scope: { meteo: '=', close: '&onClose', width: '=', cart: '=', lang: '=', hscale: '=' },
          link: function (scope, element, attrs) {

            var W = $(window),
                height = element.parent().height() || 420,
                width  = W.width(), maxwidth = scope.width || 900,
                canvas = $('#canvas', element), elem = $(element),
                date = moment(location.search().date, 'YYYYMMDDTHHmm'),
                hashcode = new Date().getTime();

            element.height(Math.min(height, 420));

            scope.parameters = [];
            scope.selected = [];
            scope.autoscale = false;
            scope.use_settings = true;

            scope.print = function () {
              print(canvas, scope.meteo && scope.meteo.filename || null);
            };

            scope.save = function (type) {
              var name = scope.graph.data.name + '.meteo';
              scope.$parent.save(type, scope.graph, name);
            };

            function update () {
              if (!scope.meteo) return;

              if (scope.cart) {

                height = element.parent().height() - 10;
                elem.width(scope.width);
                elem.height(height / (scope.hscale || 1) - (scope.hscale || 1 - 1) * 5);
                canvas.width(elem.width() - 94);

                if (scope.selected && scope.selected.length > 1) {
                  canvas.width(elem.width() - 80);
                }

                canvas.css('margin-left', '52px');

              } else {
                //console.dir(element.parent().height());
                // height = Math.min(element.parent().height() - 60, 420);
                // elem.width(Math.min(Math.max(width, maxwidth - 20), maxwidth - 20));
                // elem.height(height + 50);
                height = Math.max(element.height(), 100);
                width = Math.max(element.width(), maxwidth)
                element.height(height)
                element.width(width)
                canvas.width(elem.width() - 20);
              }

              canvas.height(Math.max(elem.height() - 50, 100));

              if (!scope.graph) {
                scope.graph = niviz.draw('Meteograph', scope.meteo, canvas, {});
                if (date.isValid()) scope.graph.current(date);
                registry.add(hashcode, scope.graph);
              } else {
                scope.graph.draw();
              }

              scope.initParameters();

              var select = $('.dropdownSelect', elem);
              select.width(300);
              select.offset({ left: scope.graph.properties.br.x - 288 + elem.offset().left});
              //scope.set();
            }

            scope.initParameters = function () {
              scope.parameters = scope.graph.all.map(function (p, i) {
                return { id: i, label: p };
              });

              scope.selected = scope.parameters.filter(function (p) {
                if (scope.graph.parameters.indexOf(p.label) > -1) return true;
                return false;
              });
            };

            scope.set = function (value) {
              if (scope.graph) {
                var array = scope.selected.map(function (e) {
                  var i = 0;
                  for (i = 0; i < scope.parameters.length; ++i) {
                    if (scope.parameters[i].id === e.id) break;
                  }
                  return scope.parameters[i] && scope.parameters[i].label;
                });

                scope.graph.set(array);
                update();
              }
            };

            scope.toggleAutoscale = function () {
              scope.autoscale = !scope.autoscale;
              scope.graph.setProperties({ autoscale: scope.autoscale });
              scope.set();
            };

            scope.setPriority = function (priority) {
              scope.use_settings = (priority === 'settings');
              scope.graph.setProperties({ use_settings: scope.use_settings });
              scope.set();
            };


            var oldwidth = 0, oldheight = 0, tListener = null;
            var observer = new MutationObserver(function(mutations) {
              var h = element.height(), w = element.width();

              if (h !== oldheight || w !== oldwidth) {
                clearTimeout(tListener)
                tListener = setTimeout(function () {
                  update();
                }, 500);
              }

            });

            observer.observe(element[0], { attributes: true });

            scope.$on('$destroy', function () {
              observer.disconnect();
              clearTimeout(tListener)
              registry.remove(hashcode);
            });

            scope.$watch('width', function (w) {
              // console.dir(w)
              maxwidth = scope.width || 900;
              update();
            });

            scope.$watch('$parent.height + hscale', function (h) {
              // console.log(h)
              update();
            });

            scope.$on('settings', function (e, c) {
              scope.set();
            });

            scope.$watch('meteo', function (meteo) {
              if (meteo) {
                scope.meteo = meteo;
                update();
              }
            });
          }
        };
      }
    ]);

}(angular, jQuery, moment, niviz));
