/*
 * niViz -- snow profiles visualization
 * Copyright (C) 2015 WSL/SLF - Fluelastrasse 11 - 7260 Davos Dorf - Switzerland.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

(function (angular, $) {
  'use strict';

  angular.module('niviz.visualizer', [
    'niviz.settings'
  ]).

  directive('pvisualizer', [
    '$location',
    '$timeout',
    'editprofile',

    function (location, $timeout, editprofile) {
      return {
        restrict: 'A',
        transclude: true,
        templateUrl: 'pvisualizer.html',
        link: function (scope, element, attrs) {
          var node = angular.element(element[0].getElementsByClassName('pvisualizer')[0]);

          scope.resize = function () {
            var editor = angular.element(element[0].getElementsByClassName('editor')[0]);
            var edit = angular.element(element[0].getElementsByClassName('profile-editor')[0]);
            var prof = angular.element(element[0].getElementsByClassName('prof')[0]);

            var h = prof[0].offsetHeight;
            edit.css('height', h + 'px');
            editor.css('min-height', (h - 37) + 'px');
            editor.css('max-height', (h - 37) + 'px');
          };

          scope.editor = false;
          scope.showeditor = function (val) {
            scope.editor = val;
            node.css('display', val ? 'flex' : 'block');
            $timeout(scope.resize);
          };

          scope.showmodal = function (val) {
            editprofile({
              station: scope.profile,
              profile: scope.profile.current || scope.profile.profiles[0]
            });
          };

          function showlayer (layer) {
            $timeout(function () {
              scope.view = 2;
              scope.layer = layer;
              scope.gotlayer = new Date();
              scope.showeditor(true); //needs to be called last
            });
          }

          scope.profile.emitter.on('layer', showlayer);

          // Check whether it's a new profile
          if (scope.profile && scope.profile.profiles.length === 1
              && scope.profile.profiles[0].features.length === 0)
            scope.editor = true;

          scope.$on('$destroy', function () {
            scope.profile.emitter.off('layer');
          });
        }
      };
    }
  ]).

  directive('visualizer', [
    '$location',

    function (location) {
      return {
        restrict: 'A',
        transclude: true,
        templateUrl: 'visualizer.html',
        link: function (scope, element, attrs) {
          var W = $(window), offset = 80, minsize = 1000,
            width  = Math.max(W.width() - offset, minsize),
            view = location.search().view, count = 0, height = 0;

          var mode = 'overlap';
          scope.height = height;
          scope.extracted = { meteo: null };
          scope.hscale = 1;

          scope.maximize = function (target) {
            mode = 'maximize';

            width  = Math.max(W.width() - offset, minsize);
            element.width(width + offset);

            if (target === 'timeline') {
              var twidth = 0.95 * width;
              if (twidth < 500) twidth = 500;
              scope.twidth = twidth;
              //scope.pwidth = 0;
              scope.tmax = true;
            } else if (target === 'profile') {
              scope.miniature = false;
              scope.pwidth = 900;
              scope.pmax = true;
            }
          };

          scope.overlap = function (show) {
            mode = 'overlap';

            scope.tmax = false;
            scope.pmax = false;
            scope.hscale = 1;

            if (show === 'timeline' || show === 'profile') {
              scope.maximize(show);
            } else {
              scope.miniature = true;
              width  = Math.max(W.width() - offset, minsize);
              element.width(width + offset);
              scope.pwidth = 0.35 * width;
              scope.twidth = 0.65 * width;
            }

            setHscale();
            scope.$$phase || scope.$digest();
          };

          scope.overlap(view);

          function adjustheight (timelines) {
            if (count !== timelines.length) count = timelines.length;
            height = W.innerHeight() - $('.navbar').height() - 10;
            if (count === 2) height = height / 2;
            if (count >= 3) height = height / 3;

            height = Math.max(280, height);
            $(element).height(height);
            scope.height = height;

            setHscale();
          }

          function setHscale () {
            var hscale = 1;
            if (scope.timeline.meteo) ++hscale;
            if (scope.extracted && scope.extracted.meteo) ++hscale;

            scope.hscale = hscale;
          }

          scope.$on('rearrange', function () {
            adjustheight(scope.timelines);
            scope[mode]();
          });

          scope.$watch('timeline.meteo', function (meteo) {
            // if (meteo) {
            //   scope.$parent.$parent.timeline_meteo = true;
            // } else {
            //   scope.$parent.$parent.timeline_meteo = false;
            // }

            if (meteo || meteo === null) {
              setTimeout(scope.overlap);
            }
          });

          scope.$watchCollection('timelines', function (timelines) {
            adjustheight(timelines);
          });
        }
      };
    }
  ]);
}(angular, jQuery));
